#include "io.h"

#include <fstream>

namespace IO {

std::vector<uint8> loadFile(const fs::path& path) noexcept {
	std::vector<uint8> contents;
	if (std::ifstream in{path, std::ios::in | std::ios::binary | std::ios::ate}) {
		contents.resize(in.tellg());
		in.seekg(0, std::ios::beg);
		in.read(reinterpret_cast<char*>(&contents[0]), contents.size());
		in.close();
	}
	return contents;
}

std::string loadFileSigned(const fs::path& path) noexcept {
	std::string contents;
	if (std::ifstream in{path, std::ios::in | std::ios::binary | std::ios::ate}) {
		contents.resize(in.tellg());
		in.seekg(0, std::ios::beg);
		in.read(&contents[0], contents.size());
		in.close();
	}
	return contents;
}

void saveFile(const fs::path& path, const std::vector<uint8>& data) noexcept {
	if (std::ofstream out{path, std::ios::out | std::ios::binary}) {
		out.write(reinterpret_cast<const char*>(&data[0]), data.size());
	}
}

void saveFile(const fs::path& path, std::string_view data) noexcept {
	if (std::ofstream out{path, std::ios::out | std::ios::binary}) {
		out.write(data.data(), data.size());
	}
}

bool createDir(const fs::path& path) noexcept {
	IO::fs::create_directory(path);

	if (!IO::fs::exists(path)) {
		return false;
	}

	return true;
}

} // namespace IO
