#pragma once

#include <GL/glew.h>
#ifndef __APPLE__
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#define DEBUG_GL

#ifdef DEBUG_GL
#define CHECK_FOR_GL_ERROR OGL::checkForGLError(__FILE__, __LINE__, __func__)
#else
#define CHECK_FOR_GL_ERROR do {} while (0)
#endif

namespace OGL {

int checkForGLError(const char* file, int line, const char* func);

};

