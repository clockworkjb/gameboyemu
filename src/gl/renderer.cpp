#include "renderer.h"

#include "gl.h"
#include "shader.h"

#include <memory>
#include <iostream>

namespace Renderer {

static SDL_GLContext context;
static GLuint shaderProgram;
static GLuint vao;

static const std::string vertShader(R"(#version 410
layout(location = 0) out vec2 fragTexCoord;
void main() {
	float x = -1.0 + float((gl_VertexID & 1) << 2);
	float y = -1.0 + float((gl_VertexID & 2) << 1);
	fragTexCoord.x = (x + 1.0) * 0.5;
	fragTexCoord.y = (y + 1.0) * 0.5;
	gl_Position = vec4(x, -y, 0, 1);
}
)");

static const std::string fragShader(R"(#version 410
uniform sampler2D texSampler;
layout(location = 0) in vec2 fragTexCoord;
layout(location = 0) out vec4 outColor;
void main() {
	outColor = vec4(texture(texSampler, fragTexCoord).rgb, 1.0);
}
)");

bool init(const SDL::Window& window) {
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4); 
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	context = SDL_GL_CreateContext(window.window);

	glewExperimental = GL_TRUE;
	auto err = glewInit();
	if (err != GLEW_OK) {
		std::cerr << "Error initialising glew: " << glewGetErrorString(err) << "\n";
		return false;
	}
	CHECK_FOR_GL_ERROR;

	shaderProgram = OGL::loadShaderProgram(vertShader, fragShader);

	glGenVertexArrays(1, &vao);
	CHECK_FOR_GL_ERROR;

	glUseProgram(shaderProgram);
	glUniform1i(glGetUniformLocation(shaderProgram, "texSampler"), 0);
	CHECK_FOR_GL_ERROR;

	return true;
}

void generateTexture(Texture& texture, int width, int height) {
	GLuint id;
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, width, height);
	CHECK_FOR_GL_ERROR;
	texture.content = reinterpret_cast<void*>(id);
}

void cleanupTexture(Texture& texture) {
	GLuint id = (intptr_t)texture.content;
	glDeleteTextures(1, &(id));
}

void cleanup() {
	glDeleteVertexArrays(1, &vao);
	glDeleteProgram(shaderProgram);
	SDL_GL_DeleteContext(context);
}

void updateTexture(Texture& texture, Image::PixelView<vmdx::rgba8> content) {
	glBindTexture(GL_TEXTURE_2D, (intptr_t)texture.content);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, content.width, content.height, GL_RGBA, GL_UNSIGNED_BYTE, content.data);
	CHECK_FOR_GL_ERROR;
}

void renderTexture(Texture& texture) {
	glUseProgram(shaderProgram);
	glBindTexture(GL_TEXTURE_2D, (intptr_t)texture.content);
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glBindVertexArray(0);
	CHECK_FOR_GL_ERROR;
}

void clear() {
	glClear(GL_COLOR_BUFFER_BIT);
}

void viewport(const SDL::Window& window) {
	SDL_GL_MakeCurrent(window.window, context);
	glViewport(0, 0, window.width, window.height);
}

void swap(const SDL::Window& window) {
	SDL_GL_SwapWindow(window.window);
}

};
