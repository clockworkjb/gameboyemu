#pragma once

#include "gl.h"

#include <string>

namespace OGL {

GLuint loadShaderProgram(const std::string& vertFile, const std::string& fragFile);
GLuint loadShaderProgram(const std::string& vertFile, const std::string& geomFile, const std::string& fragFile);

};
