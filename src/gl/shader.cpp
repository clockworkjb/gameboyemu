#include "shader.h"

#include <vector>
#include <iostream>

namespace OGL {

static void checkShaderCompilation(GLuint shader) {
	GLint success = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

	if (success == GL_FALSE) {
		GLint maxLength = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> log(maxLength);
		glGetShaderInfoLog(shader, maxLength, &maxLength, &log[0]);

		std::string str(std::begin(log), std::end(log));

		std::cerr << "Error compiling shader: " << str << "\n";

		glDeleteShader(shader);
	}
}

static void checkProgramLink(GLuint program) {
	GLint success = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &success);

	if (success == GL_FALSE) {
		GLint maxLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> log(maxLength);
		glGetProgramInfoLog(program, maxLength, &maxLength, &log[0]);

		std::string str(std::begin(log), std::end(log));

		std::cerr << "Error linking program: " << str << "\n";

		glDeleteProgram(program);
	}
}

GLuint loadShaderProgram(const std::string& vertString, const std::string& fragString) {
	const GLchar* vertSource = vertString.c_str();
	const GLchar* fragSource = fragString.c_str();
	const GLint vertLength = vertString.size();
	const GLint fragLength = fragString.size();

	auto vertShader = glCreateShader(GL_VERTEX_SHADER);
	auto fragShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertShader, 1, &vertSource, &vertLength);
	glShaderSource(fragShader, 1, &fragSource, &fragLength);

	glCompileShader(vertShader);
	checkShaderCompilation(vertShader);

	glCompileShader(fragShader);
	checkShaderCompilation(fragShader);

	GLuint shaderProgram = glCreateProgram();

	glAttachShader(shaderProgram, vertShader);
	glAttachShader(shaderProgram, fragShader);

	glLinkProgram(shaderProgram);
	checkProgramLink(shaderProgram);

	glDeleteShader(vertShader);
	glDeleteShader(fragShader);

	CHECK_FOR_GL_ERROR;

	return shaderProgram;
}

GLuint loadShaderProgram(const std::string& vertString, const std::string& geomString, const std::string& fragString) {
	const GLchar* vertSource = vertString.c_str();
	const GLchar* geomSource = geomString.c_str();
	const GLchar* fragSource = fragString.c_str();
	const GLint vertLength = vertString.size();
	const GLint geomLength = geomString.size();
	const GLint fragLength = fragString.size();

	auto vertShader = glCreateShader(GL_VERTEX_SHADER);
	auto geomShader = glCreateShader(GL_GEOMETRY_SHADER);
	auto fragShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertShader, 1, &vertSource, &vertLength);
	glShaderSource(geomShader, 1, &geomSource, &geomLength);
	glShaderSource(fragShader, 1, &fragSource, &fragLength);

	glCompileShader(vertShader);
	checkShaderCompilation(vertShader);

	glCompileShader(geomShader);
	checkShaderCompilation(geomShader);

	glCompileShader(fragShader);
	checkShaderCompilation(fragShader);

	GLuint shaderProgram = glCreateProgram();

	glAttachShader(shaderProgram, vertShader);
	glAttachShader(shaderProgram, geomShader);
	glAttachShader(shaderProgram, fragShader);

	glLinkProgram(shaderProgram);
	checkProgramLink(shaderProgram);

	glDeleteShader(vertShader);
	glDeleteShader(geomShader);
	glDeleteShader(fragShader);

	CHECK_FOR_GL_ERROR;

	return shaderProgram;
}

};
