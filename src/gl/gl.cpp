#include "gl.h"

#include <iostream>

namespace OGL {

int checkForGLError(const char* file, int line, const char* func) {
	auto err = glGetError();

	if (err != GL_NO_ERROR) {
		std::cerr << "GL error on line " << line << " in  file " << file << " in function " << func << ": ";

		switch (err) {
		case GL_INVALID_ENUM:
			std::cerr << "invalid enum";
			break;
		case GL_INVALID_VALUE:
			std::cerr << "invalid value";
			break;
		case GL_INVALID_OPERATION:
			std::cerr << "invalid operation";
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			std::cerr << "invalid framebuffer operation";
			break;
		case GL_OUT_OF_MEMORY:
			std::cerr << "out of memory";
			break;
		case GL_STACK_UNDERFLOW:
			std::cerr << "stack underflow";
			break;
		case GL_STACK_OVERFLOW:
			std::cerr << "stack overflow";
			break;
		default:
			std::cerr << "unknown error";
			break;
		}

		std::cerr << "\n";
		return 1;
	}

	return 0;
}

};
