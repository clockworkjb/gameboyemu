#pragma once

#include "sdl/sdl.h"
#include "types.h"
#include "image.h"
#include "texture.h"
#include "vec.h"

namespace Renderer {

bool init(const SDL::Window& mainWindow);
void cleanup();
void generateTexture(Texture& texture, int width, int height);
void updateTexture(Texture& texture, Image::PixelView<vmdx::rgba8> content);
void cleanupTexture(Texture& texture);
void renderTexture(Texture& texture);

void clear();
void viewport(const SDL::Window& window);
void swap(const SDL::Window& window);

};
