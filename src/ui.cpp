#include "ui.h"

#include "debug.h"
#include "types.h"
#include "gameboy/system/system.h"
#include "gameboy/memory.h"
#include "gameboy/cpu/disassemble.h"

#include "sdl/sdl.h"
#include "renderer.h"
#include "io.h"
#include "image.h"
#include "texture.h"
#include "vec.h"
#include "parse_ini.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include "imgui/imgui.h"
#include "imgui/imgui_tabs.h"
#include "imgui/imgui_memory_editor.h"
#include "imgui/imgui_impl_sdl_gl3.h"

#include <chrono>
#include <unordered_map>
#include <memory>

#include <iostream>
#include <iomanip>

namespace UI {

using namespace std::literals::chrono_literals;

constexpr int systemHeight = 144, systemWidth = 160;

enum struct TimingMode { Run, Halt, Stop };

static struct {
	float scaleX = 4.0, scaleY = 4.0;
	float speedMultiplier = 1.0f;
	bool lockAspectRatio = false;
	bool requireFocus = false;

	std::unordered_map<SDL_Keycode, Gameboy::Joypad::Button> keys;
	std::unordered_map<SDL_GameControllerButton, Gameboy::Joypad::Button> buttons;
} config;

static struct {
	bool initialised = false;
	int height = systemHeight * config.scaleY, width = systemWidth * config.scaleX;

 	int frameCount = 0;
 	float frameRate = 0;

	std::unique_ptr<SDL::Window> mainWindow;
	std::unique_ptr<SDL::Window> debugWindow;

	Gameboy::System* gb = nullptr;

	bool wantRedisplay = true;
	TimingMode tMode = TimingMode::Run;

	int cyclesLeftover = 0;

	int MAX_CONTROLLERS = 1;
	SDL_GameController *controllers[1];
	int controllerIndex = 0;

	Texture textures[4];
} state;

static void takeScreenshot();

static void handleKeys(SDL_KeyboardEvent event) {
	if (!state.mainWindow->focus)
		return;

	auto key = event.keysym;
	bool setting = event.type == SDL_KEYDOWN;

	if (auto it = config.keys.find(key.sym); it != std::end(config.keys))
		state.gb->toggleButton(it->second, setting);

	switch (key.sym) {
	case SDLK_ESCAPE:
		state.tMode = TimingMode::Stop;
		break;

	case SDLK_q:
		if (setting) {
			if (state.tMode == TimingMode::Halt) {
				state.tMode = TimingMode::Run;
				state.gb->setPaused(false);
			}
			else {
				state.tMode = TimingMode::Halt;
				state.gb->setPaused(true);
			}
		}
		break;

	case SDLK_w:
		if (setting && state.tMode == TimingMode::Halt) {
			state.gb->execute();
		}
		break;

	case SDLK_r:
		if (setting) {
			state.gb->reset();
		}
		break;

	case SDLK_m:
		if(setting) {
			state.gb->toggleMapViewIndex();
		}
		break;

	case SDLK_t:
		if(setting) {
			state.gb->toggleCurrentViewPalette();
		}
		break;

	case SDLK_SPACE:
		if (setting) {
			config.speedMultiplier = 2.0f;
		}
		else {
			config.speedMultiplier = 1.0f;
		}
		break;

	case SDLK_p:
		if (setting) {
			takeScreenshot();
		}
		break;

	default:
		break;
	}
}

static void handleControllerButton(SDL_ControllerButtonEvent &event)
{
	bool setting = event.state == SDL_PRESSED;

	if (auto it = config.buttons.find(static_cast<SDL_GameControllerButton>(event.button)); it != std::end(config.buttons))
		state.gb->toggleButton(it->second, setting);
}

static void addController(int id)
{
	if (SDL_IsGameController(id)) {
		if (auto pad = SDL_GameControllerOpen(id); pad) {
			if (state.controllerIndex < state.MAX_CONTROLLERS) {
				state.controllers[state.controllerIndex++] = pad;
				
				const char *name = SDL_GameControllerName(pad);
				if (!name) name = "Unknown Controller";
				std::cout << "Plugged in controller: " << name << "\n";
			}
		}
	}
}

static void removeController(int id)
{
	--state.controllerIndex;
	auto pad = SDL_GameControllerFromInstanceID(id);
	const char *name = SDL_GameControllerName(pad);
	if (!name) name = "Unknown Controller";
	std::cout << "Removed controller: " << name << "\n";
	SDL_GameControllerClose(pad);
}

static void handleEvents() {
	SDL_Event event;
	auto& io = ImGui::GetIO();

	while (SDL_PollEvent(&event)) {
		if (io.WantCaptureMouse || io.WantCaptureKeyboard) {
			ImGui_ImplSdlGL3_ProcessEvent(&event);
		}

		switch (event.type) {
		case SDL_QUIT:
			state.tMode = TimingMode::Stop;
			break;

		case SDL_MOUSEMOTION:
			break;

		case SDL_MOUSEBUTTONDOWN:
			break;

		case SDL_MOUSEBUTTONUP:
			break;

		case SDL_KEYDOWN:
		case SDL_KEYUP:
			
			handleKeys(event.key);
			break;

		case SDL_CONTROLLERBUTTONDOWN:
		case SDL_CONTROLLERBUTTONUP:
			handleControllerButton(event.cbutton);
			break;

		case SDL_CONTROLLERDEVICEADDED:
			addController(event.cdevice.which);
			break;

		case SDL_CONTROLLERDEVICEREMOVED:
			removeController(event.cdevice.which);
			break;

		case SDL_WINDOWEVENT:
			state.wantRedisplay = true;
			switch (event.window.event) {
				case SDL_WINDOWEVENT_RESIZED:
					if (event.window.windowID == SDL_GetWindowID(state.mainWindow->window)) {
						state.mainWindow->handleResize(event);
					}
					else if (event.window.windowID == SDL_GetWindowID(state.debugWindow->window)) {
						state.debugWindow->handleResize(event);
					}
					break;

				case SDL_WINDOWEVENT_CLOSE:
					state.tMode = TimingMode::Stop;
					break;
				default:
					break;

				case SDL_WINDOWEVENT_FOCUS_GAINED:
					if (event.window.windowID == SDL_GetWindowID(state.mainWindow->window)) {
						state.mainWindow->gainFocus();
					}
					break;
				case SDL_WINDOWEVENT_FOCUS_LOST:
					if (event.window.windowID == SDL_GetWindowID(state.mainWindow->window)) {
						state.mainWindow->loseFocus();
					}
					break;
			}
			break;

		default:
			break;
		}
	}
}

static void takeScreenshot() {
	auto data = state.gb->display.getFramebuffer();
	
	// get the current local date/time for the image filename
	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);
	std::stringstream ss;
	ss << state.gb->getCartInfo().name << " - " << std::put_time(&tm, "%F %r") << ".png";

	IO::fs::path path = "screenshot";

	if (!IO::createDir(path)) {
		std::cerr << "Could not access screenshot directory " << path << "\n";
		return;
	}

	path /= ss.str();

	if (!stbi_write_png((const char*)path.c_str(), systemWidth, systemHeight, 4, data.data, systemWidth * sizeof(vmdx::rgba8))) {
		std::cerr << "error saving screeenshot\n";
		return;
	}

	std::cout << "saving screenshot: " << path << "\n";
}

static void updateGUI() {
	ImGui_ImplSdlGL3_NewFrame(state.mainWindow->window);
	//ImGui::SetNextWindowPos(ImVec2(0, 0));
	//ImGui::SetNextWindowSize(ImVec2(state.debugWindow->width, state.debugWindow->height));

	ImGui::Begin("Debugger", nullptr, 0);
	const auto dimensions = ImGui::GetContentRegionAvail();

	if (ImGui::CollapsingHeader("Tile Set")) {
		const auto [width, height] = Image::resizeMaintainAspect(128, 192, dimensions.x, dimensions.y);
		ImGui::Image((ImTextureID)state.textures[1].getContent(), ImVec2(width, height));
	}
	if (ImGui::CollapsingHeader("Tile Map")) {
		const auto [width, height] = Image::resizeMaintainAspect(256, 256, dimensions.x, dimensions.y);
		ImGui::Image((ImTextureID)state.textures[2].getContent(), ImVec2(width, height));
	}
	if (ImGui::CollapsingHeader("OAM")) {
		const auto [width, height] = Image::resizeMaintainAspect(32, 160, dimensions.x, dimensions.y);
		ImGui::Image((ImTextureID)state.textures[3].getContent(), ImVec2(width, height));
	}
	if (ImGui::CollapsingHeader("Memory")) {
		static MemoryEditor mem_edit_1;
		auto data = state.gb->peekAllMemory();
		mem_edit_1.DrawContents(data.data(), data.size(), 0x0000);
	}
	if (ImGui::CollapsingHeader("Rom")) {
		auto info = state.gb->getCartInfo();
		ImGui::Text("Name: %s", info.name.data());
		ImGui::Text("GBC support: %s", info.gbc ? "yes" : "no");
		ImGui::Text("SGB support: %s", info.sgb ? "yes" : "no");
		std::stringstream ss;
		ss << info.mapper;
		ImGui::Text("Mapper: %s", ss.str().data());
		ImGui::Text("Rom size: %zub %zu banks", info.romSize, info.numRomBanks);
		ImGui::Text("Ram size: %zub %zu banks", info.ramSize, info.numRamBanks);
		ImGui::Text("Save ram: %s", info.ram ? "yes" : "no");
		ImGui::Text("Battery: %s", info.battery ? "yes" : "no");
		ImGui::Text("Realtime clock: %s", info.rtc ? "yes" : "no");
		ImGui::Text("Rumble: %s", info.rumble ? "yes" : "no");
		ImGui::Text("Camera: %s", info.camera ? "yes" : "no");
	}
	if (ImGui::CollapsingHeader("Display")) {
		auto displayData = state.gb->display.getData();

		ImGui::Checkbox("LCD Enabled", &displayData.lcdEnabled);
		ImGui::Checkbox("Window Map Select:", &displayData.windowMapSelect);
		ImGui::SameLine();
		ImGui::Text("%s", displayData.windowMapSelect ? "9c00" : "98000");
		ImGui::Checkbox("Window Enabled", &displayData.windowEnabled);
		ImGui::Checkbox("Tile Data Select:", &displayData.tileDataSelect);
		ImGui::SameLine();
		ImGui::Text("%s", displayData.tileDataSelect ? "8000" : "8800");
		ImGui::Checkbox("BG Map Select", &displayData.bgMapSelect);
		ImGui::SameLine();
		ImGui::Text("%s", displayData.bgMapSelect ? "9c00" : "98000");
		ImGui::Checkbox("8x16 Sprites", &displayData.sprite8x16);
		ImGui::Checkbox("Sprites Enabled", &displayData.spritesEnabled);
		ImGui::Checkbox("BG Enabled", &displayData.bgEnabled);
		ImGui::Separator();
		ImGui::Checkbox("LYC Interrupt", &displayData.lycInterrupt);
		ImGui::Checkbox("OAM Interrupt", &displayData.oamInterrupt);
		ImGui::Checkbox("V-Blank Interrupt", &displayData.vBlankInterrupt);
		ImGui::Checkbox("H-Blank Interrupt", &displayData.hBlankInterrupt);
		ImGui::Checkbox("LYC Coincidence", &displayData.lycCoincidence);
		ImGui::Text("Mode: %d", displayData.mode);
		ImGui::Separator();
		ImGui::Text("Scroll X: %04x Scroll Y: %04x", displayData.scrollX, displayData.scrollY);
		ImGui::Text("Window X: %04x Window Y: %04x", displayData.windowX, displayData.windowY);
		ImGui::Text("LY: %04x", displayData.ly);
		ImGui::Text("LYC: %04x", displayData.lyc);
		ImGui::Separator();
		ImGui::Text("BGP");
		for (size_t i = 0; i < 4; ++i ) {
			ImGui::PushID(i);
			ImGui::PushStyleColor(ImGuiCol_Button, state.gb->display.palette[displayData.bgp[i]]);
			ImGui::Combo("", reinterpret_cast<int*>(displayData.bgp.data() + i), " 0\0 1\0 2\0 3\0");
			ImGui::PopStyleColor();
			ImGui::PopID();
		}
		ImGui::Spacing();
		ImGui::Text("OBP0");
		for (size_t i = 0; i < 4; ++i ) {
			ImGui::PushID(i + 4);
			ImGui::PushStyleColor(ImGuiCol_Button, state.gb->display.palette[displayData.obp[0][i]]);
			ImGui::Combo("", reinterpret_cast<int*>(displayData.obp[0].data() + i), " 0\0 1\0 2\0 3\0");
			ImGui::PopStyleColor();
			ImGui::PopID();
		}
		ImGui::Spacing();
		ImGui::Text("OBP1");
		for (size_t i = 0; i < 4; ++i ) {
			ImGui::PushID(i + 8);
			ImGui::PushStyleColor(ImGuiCol_Button, state.gb->display.palette[displayData.obp[1][i]]);
			ImGui::Combo("", reinterpret_cast<int*>(displayData.obp[1].data() + i), " 0\0 1\0 2\0 3\0");
			ImGui::PopStyleColor();
			ImGui::PopID();
		}
		ImGui::Spacing();
		static ImVec4 col[4] = {};
		for (size_t i = 0; i < 4; ++i ) {
			ImGui::PushID(i + 12);
			col[i] = ImGui::ColorConvertU32ToFloat4(state.gb->display.palette[i]);
			ImGui::ColorEdit4("", &col[i].x, ImGuiColorEditFlags_RGB | ImGuiColorEditFlags_NoAlpha);
			state.gb->display.palette[i] = ImGui::ColorConvertFloat4ToU32(col[i]);
			ImGui::PopID();
		}
	}
	if (ImGui::CollapsingHeader("CPU")) {
		auto& cpu = state.gb->cpu;
		std::stringstream ss;
		ss << Gameboy::disassembler(cpu.registers.pc, state.gb);
		ImGui::Text("PC: %04x - %s", cpu.registers.pc, ss.str().data());
		ImGui::Text("SP: %04x", cpu.registers.sp);
		ImGui::Text("AF: %04x A: %02x F: %02x", (unsigned)cpu.registers.af, cpu.registers.a, (unsigned)cpu.registers.f);
		ImGui::Text("BC: %04x B: %02x C: %02x", cpu.registers.bc, cpu.registers.b, cpu.registers.c);
		ImGui::Text("DE: %04x D: %02x E: %02x", cpu.registers.de, cpu.registers.d, cpu.registers.e);
		ImGui::Text("HL: %04x H: %02x L: %02x", cpu.registers.hl, cpu.registers.h, cpu.registers.l);
		ImGui::Text("Flags:");
		ImGui::Checkbox("Z", &cpu.registers.f.z);
		ImGui::SameLine();
		ImGui::Checkbox("N", &cpu.registers.f.n);
		ImGui::SameLine();
		ImGui::Checkbox("H", &cpu.registers.f.h);
		ImGui::SameLine();
		ImGui::Checkbox("C", &cpu.registers.f.c);
		ImGui::Checkbox("IME", &cpu.registers.ime);
		ImGui::SameLine();
		ImGui::Checkbox("EI", &cpu.registers.ei);
		ImGui::Separator();
		ImGui::Text("Cycles completed: %d", cpu.cycleCount);
		ImGui::Text("DIV: %02x", cpu.div);
		ImGui::Text("TIMA: %02x", cpu.tima);
		ImGui::Text("TAC: %02x", cpu.read(Gameboy::Memory::TAC));
		ImGui::SameLine();
		ImGui::Checkbox("Timer enabled", &cpu.timerEnabled);
		int clockFrequency = 0;
		switch (cpu.clockSelect) {
		case 0:
			clockFrequency = 4096;
			break;
		case 1:
			clockFrequency = 262144;
			break;
		case 2:
			clockFrequency = 65536;
			break;
		case 3:
			clockFrequency = 16384;
			break;
		}
		ImGui::Text("Timer frequency: %dhz", clockFrequency);
		ImGui::Text("IE: %02x", cpu.read(Gameboy::Memory::IE));
		ImGui::Text("IF: %02x", cpu.read(Gameboy::Memory::IF));
		ImGui::Text("Enabled: ");
		ImGui::SameLine();
		ImGui::Checkbox("V-Blank", &cpu.interruptEnable.vBlank);
		ImGui::SameLine();
		ImGui::Checkbox("STAT", &cpu.interruptEnable.STAT);
		ImGui::SameLine();
		ImGui::Checkbox("Serial", &cpu.interruptEnable.serial);
		ImGui::SameLine();
		ImGui::Checkbox("Joypad", &cpu.interruptEnable.joypad);
		ImGui::Text("Requested: ");
		ImGui::SameLine();
		ImGui::Checkbox("V-Blank", &cpu.interruptFlag.vBlank);
		ImGui::SameLine();
		ImGui::Checkbox("STAT", &cpu.interruptFlag.STAT);
		ImGui::SameLine();
		ImGui::Checkbox("Serial", &cpu.interruptFlag.serial);
		ImGui::SameLine();
		ImGui::Checkbox("Joypad", &cpu.interruptFlag.joypad);
	}
	ImGui::End();
}

using FramePeriod = std::chrono::duration<long long int, std::ratio<1, 60>>;
constexpr auto singleFrame = FramePeriod(1);

static void idle() {
	static auto startTime = std::chrono::steady_clock::now();
	static auto last = startTime;
	static auto accumulator = 0ns;
	
	auto now = std::chrono::steady_clock::now();
	auto frameTime = now - last;
	last = now;

	accumulator += frameTime;

	if (accumulator > 100ms)
		accumulator = 100ms;

	while (accumulator >= singleFrame) {
		if (state.tMode == TimingMode::Run) {
			state.cyclesLeftover = state.gb->execute((state.gb->cyclesPerFrame + state.cyclesLeftover) * config.speedMultiplier);
			state.wantRedisplay = state.gb->getWantRedisplay();
		}

		if (state.gb->isStopped())
			state.tMode = TimingMode::Stop;

		if (state.gb->isPaused())
			state.tMode = TimingMode::Halt;

		accumulator -= std::chrono::duration_cast<std::chrono::nanoseconds>(singleFrame);
		++state.frameCount;
	}
}

void mainLoop() {
	if (!state.initialised) {
		return;
	}

	while (state.tMode != TimingMode::Stop) {
		handleEvents();

		if (state.wantRedisplay) {
			state.gb->postRedisplay();

			Renderer::updateTexture(state.textures[0], state.gb->display.getFramebuffer());
			Renderer::updateTexture(state.textures[1], state.gb->display.getTileImage());
			Renderer::updateTexture(state.textures[2], state.gb->display.getTileMapImage());
			Renderer::updateTexture(state.textures[3], state.gb->display.getOAMImage8x16());

			if (state.debugWindow->visible) {
				Renderer::viewport(*state.debugWindow);
				Renderer::clear();
				Renderer::swap(*state.debugWindow);
			}

			Renderer::viewport(*state.mainWindow);
			Renderer::clear();
			Renderer::renderTexture(state.textures[0]);
			updateGUI();
			ImGui::Render();
			ImGui_ImplSdlGL3_RenderDrawData(ImGui::GetDrawData());
			Renderer::swap(*state.mainWindow);
		}

		idle();
	}

	ImGui_ImplSdlGL3_Shutdown();
	ImGui::DestroyContext();

	for (int i = 0; i < 4; ++i) {
		Renderer::cleanupTexture(state.textures[i]);
	}
	Renderer::cleanup();
	SDL_Quit();
}

static void setKey(std::string_view keyName, Gameboy::Joypad::Button button, SDL_Keycode defaultKey) {
	auto key = SDL_GetKeyFromName(keyName.data());
	key = key == SDLK_UNKNOWN ? defaultKey : key;
	config.keys[key] = button;
}

static void setButton(std::string_view buttonName, Gameboy::Joypad::Button button, SDL_GameControllerButton defaultButton) {
	auto key = SDL_GameControllerGetButtonFromString(buttonName.data());
	key = key == SDL_CONTROLLER_BUTTON_INVALID ? defaultButton : key;
	config.buttons[key] = button;
}

static const std::string defaultINI(R"([keyboard]
a=x
b=z
start=return
select=right shift
up=up
down=down
left=left
right=right

[gamepad]
a=b
b=a
start=start
select=back
up=dpup
down=dpdown
left=dpleft
right=dpright

[palette]
p0=232,232,232
p1=160,160,160
p2=88,88,88
p3=16,16,16)");

static void loadConfig() {
	INI::Parser parser(defaultINI, "config.ini");

	if (auto keyboard = parser.getSection("keyboard"); keyboard) {
		setKey(keyboard->getValue("a").value(), Gameboy::Joypad::Button::A, SDLK_z);
		setKey(keyboard->getValue("b").value(), Gameboy::Joypad::Button::B, SDLK_x);
		setKey(keyboard->getValue("start").value(), Gameboy::Joypad::Button::Start, SDLK_RETURN);
		setKey(keyboard->getValue("select").value(), Gameboy::Joypad::Button::Select, SDLK_RSHIFT);
		setKey(keyboard->getValue("up").value(), Gameboy::Joypad::Button::Up, SDLK_UP);
		setKey(keyboard->getValue("down").value(), Gameboy::Joypad::Button::Down, SDLK_DOWN);
		setKey(keyboard->getValue("left").value(), Gameboy::Joypad::Button::Left, SDLK_LEFT);
		setKey(keyboard->getValue("right").value(), Gameboy::Joypad::Button::Right, SDLK_RIGHT);
	}

	if (auto gamepad = parser.getSection("gamepad"); gamepad) {
		setButton(gamepad->getValue("a").value(), Gameboy::Joypad::Button::A, SDL_CONTROLLER_BUTTON_A);
		setButton(gamepad->getValue("b").value(), Gameboy::Joypad::Button::B, SDL_CONTROLLER_BUTTON_B);
		setButton(gamepad->getValue("start").value(), Gameboy::Joypad::Button::Start, SDL_CONTROLLER_BUTTON_START);
		setButton(gamepad->getValue("select").value(), Gameboy::Joypad::Button::Select, SDL_CONTROLLER_BUTTON_BACK);
		setButton(gamepad->getValue("up").value(), Gameboy::Joypad::Button::Up, SDL_CONTROLLER_BUTTON_DPAD_UP);
		setButton(gamepad->getValue("down").value(), Gameboy::Joypad::Button::Down, SDL_CONTROLLER_BUTTON_DPAD_DOWN);
		setButton(gamepad->getValue("left").value(), Gameboy::Joypad::Button::Left, SDL_CONTROLLER_BUTTON_DPAD_LEFT);
		setButton(gamepad->getValue("right").value(), Gameboy::Joypad::Button::Right, SDL_CONTROLLER_BUTTON_DPAD_RIGHT);
	}

	if (auto ui = parser.getSection("ui"); ui) {
		ui->setValueOr("scale_x", config.scaleX, 2.f);
		ui->setValueOr("scale_y", config.scaleY, 2.f);

		ui->setValueOr("speed", config.speedMultiplier, 1.f);
		ui->setValueOr("lock_aspect", config.lockAspectRatio, false);
		ui->setValueOr("require_focus", config.requireFocus, true);

		state.width = config.scaleX * systemWidth;
		state.height = config.scaleY * systemHeight;
		state.mainWindow->resize(state.width, state.height);
	}
}

static void audioCallback(void* userData, uint8* stream, int length) {
}

static bool initAudio() {
	SDL_AudioSpec want, have;

	want.freq = 48000;
	want.format = AUDIO_S16SYS;
	want.channels = 1;
	want.samples = 1024;
	want.callback = audioCallback;

	auto deviceID = SDL_OpenAudioDevice(nullptr, 0, &want, &have, 0);
	if (!deviceID) {
		SDL::sdlError("Failed to open audio device: ");
		return false;
	}
	
	if (have.format != want.format) {
		SDL::sdlError("Couldn't find an audio device with the right format: ");
		return false;
	}

	return true;
}

bool init(Gameboy::System* gb) {
	state.gb = gb;

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER | SDL_INIT_AUDIO) < 0) {
		SDL::sdlError("Unable to init SDL: ");
		return false;
	}

	state.mainWindow = std::make_unique<SDL::Window>("GB Emulator", state.width, state.height);

	if (!state.mainWindow->window) {
		SDL::sdlError("Failed to create a window: ");
		return false;
	}

	state.debugWindow = std::make_unique<SDL::Window>("Debug", 128 * 4, 192 * 4);

	if (!state.debugWindow->window) {
		SDL::sdlError("Failed to create a window: ");
		return false;
	}

	state.mainWindow->raise();

	std::cout << "using OpenGL renderer" << "\n";
	if (!Renderer::init(*state.mainWindow)) {
		std::cerr << "Failed to initialize OpenGL renderer!" << "\n";
		return false;
	}

	Renderer::generateTexture(state.textures[0], systemWidth, systemHeight);
	Renderer::generateTexture(state.textures[1], 128, 192);
	Renderer::generateTexture(state.textures[2], 256, 256);
	Renderer::generateTexture(state.textures[3], 32, 160);

	ImGui::CreateContext();

	auto& io = ImGui::GetIO();
	io.DisplaySize.x = state.debugWindow->width;
	io.DisplaySize.y = state.debugWindow->height;

	auto& style = ImGui::GetStyle();
	style.WindowRounding = 0;
	style.WindowBorderSize = 0;

	ImGui_ImplSdlGL3_Init(state.debugWindow->window);

	loadConfig();

	if (!initAudio()) {
		std::cerr << "Failed to initialize audio output!" << "\n";
		return false;
	}

	state.initialised = true;

	return true;
}

} // namespace UI

