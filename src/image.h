#pragma once

#include "types.h"
#include "vec.h"

#include <cstddef>
#include <vector>
#include <algorithm>

namespace Image {

/*
 * A non-owning view of an image
 */
template <typename T>
struct PixelView {
	const T* data;
	const size_t width, height;

	auto begin() const {
		return data;
	}

	auto end() const {
		return &data[width * height];
	}

	const T& operator[] (size_t index) const {
		return data[index];
	}
};

/*
 * A statically allocated image
 */
template <typename T, size_t w, size_t h>
struct PixelData {
	static constexpr size_t width = w;
	static constexpr size_t height = h;
	static constexpr size_t size = w * h;
	T data[size];

	PixelData() = default;
	PixelData(const PixelData<T, w, h>& other) = default;
	PixelData(PixelData<T, w, h>&& other) = default;
	PixelData& operator =(const PixelData<T, w, h>& other) = default;
	PixelData& operator =(PixelData<T, w, h>&& other) = default;

	operator PixelView<T>() const {
		return { data, w, h };
	}

	auto begin() {
		return data;
	}

	auto end() {
		return &data[size];
	}

	auto begin() const {
		return data;
	}

	auto end() const {
		return &data[size];
	}

	T& operator [](size_t index) {
		return data[index];
	}

	const T& operator [](size_t index) const {
		return data[index];
	}
};

/*
 * An incomplete dynamically allocated image
 */
template <typename T>
struct PixelDataV {
	std::vector<T> data;
	size_t width, height;

	PixelDataV(size_t w, size_t h) : width(w), height(h) {
		data.reserve(w * h);
	}

	operator PixelView<T>() const {
		return { data.data(), width, height };
	}

	auto begin() {
		return std::begin(data);
	}

	auto end() {
		return std::end(data);
	}

	auto begin() const {
		return std::begin(data);
	}

	auto end() const {
		return std::end(data);
	}

	T& operator [](size_t index) {
		return data[index];
	}

	const T& operator [](size_t index) const {
		return data[index];
	}
};

constexpr std::pair<size_t, size_t> resizeMaintainAspect(size_t w, size_t h, size_t desiredW, size_t desiredH) {
	float ratio = std::min(desiredW / static_cast<float>(w), desiredH / static_cast<float>(h));
	return { static_cast<size_t>(w * ratio), static_cast<size_t>(h * ratio) };
}

}

