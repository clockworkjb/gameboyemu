#include "display.h"

#include "gameboy/memory.h"
#include "gameboy/cpu/cpu.h"
#include "gameboy/system/system.h"

#include <algorithm>

namespace Gameboy {

/*
 * 456 cycles per line
 * 204 cycles for data.mode 0
 * 80 cycles for data.mode 2
 * 172 cycles for data.mode 3
 *
 * 70224 cycles per frame
 *
 * 160 pixels in width
 * 144 lines displayed on screen
 * 10 "virtual" lines
 * 154 lines total
 */

constexpr int MODE0_LENGTH = 204;
constexpr int MODE2_LENGTH = 80;
constexpr int MODE3_LENGTH = 172;
constexpr int LINE_LENGTH = MODE0_LENGTH + MODE2_LENGTH + MODE3_LENGTH;
constexpr int CYCLES_PER_FRAME = 70224;

constexpr int SCREEN_WIDTH = 160;
constexpr int SCREEN_LINES = 144;
constexpr int TOTAL_PIXELS = SCREEN_WIDTH * SCREEN_LINES;
constexpr int TOTAL_LINES = CYCLES_PER_FRAME / LINE_LENGTH;

constexpr uint16 BGMAP0_OFFSET = 0x1800;
constexpr uint16 BGMAP1_OFFSET = 0x1c00;

void Display::update(int cycles) {
	while (cycles--) {
		++data.gpuClock;

		switch (data.mode) {
		case 0: // h-blank
			data.vRamAccessible = true;
			data.oamAccessible = true;
			if (data.gpuClock >= MODE0_LENGTH) {
				data.gpuClock -= MODE0_LENGTH;
				++data.ly;

				if (data.ly >= SCREEN_LINES) {
					data.mode = 1;

					if (data.vBlankInterrupt)
						system->cpu.requestInterrupt(Cpu::Interrupt::STAT);
					if (data.lcdEnabled)
						system->cpu.requestInterrupt(Cpu::Interrupt::VBlank);

					updateTileImage();
					updateTileMapImage();
					updateOAMImage();
					updateOAMImage8x16();
					data.frontbuffer = data.backbuffer;
					std::fill(std::begin(data.backbuffer), std::end(data.backbuffer), palette[0]);
					wantRedisplay = true;
				}
				else {
					data.mode = 2;

					if (data.oamInterrupt)
						system->cpu.requestInterrupt(Cpu::Interrupt::STAT);
				}

				if (data.ly == data.lyc) {
					data.lycCoincidence = true;

					if (data.lycInterrupt)
						system->cpu.requestInterrupt(Cpu::Interrupt::STAT);
				}
				else {
					data.lycCoincidence = false;
				}
			}
			break;
		case 1: // v-blank
			data.vRamAccessible = true;
			data.oamAccessible = true;
			if (data.gpuClock >= LINE_LENGTH) {
				data.gpuClock -= LINE_LENGTH;
				++data.ly;

				if (data.ly >= TOTAL_LINES - 1) {
					data.ly = 0;
					data.mode = 2;
				}
				if (data.oamInterrupt)
					system->cpu.requestInterrupt(Cpu::Interrupt::STAT);
			}
			break;
		case 2: // data.oam read
			data.vRamAccessible = true;
			data.oamAccessible = false;
			if (data.gpuClock >= MODE2_LENGTH) {
				data.gpuClock -= MODE2_LENGTH;
				data.mode = 3;
			}
			break;
		case 3: // vram read
			data.vRamAccessible = true;
			data.oamAccessible = true;
			if (data.gpuClock >= MODE3_LENGTH) {
				data.gpuClock -= MODE3_LENGTH;
				data.mode = 0;

				if (data.hBlankInterrupt)
					system->cpu.requestInterrupt(Cpu::Interrupt::STAT);

				if (data.lcdEnabled) {
					updateTileSet();
					if (data.bgEnabled) {
						updateTileMap();
						drawTiles();
					}

					if (data.spritesEnabled) {
						updateOAM();
						drawSprites();
					}
				}
			}
			break;
		}
	}
}

void Display::clearScreen() {
	std::fill(std::begin(data.backbuffer), std::end(data.backbuffer), palette[0]);
	std::fill(std::begin(data.frontbuffer), std::end(data.frontbuffer), palette[0]);
	wantRedisplay = true;
}

void Display::drawTiles() {
	bool useWindow = false;

	if (data.windowEnabled) {
		if (data.windowY <= data.ly) {
			useWindow = true;
		}
	}

	bool mapBank = useWindow ? data.windowMapSelect : data.bgMapSelect;

	const uint8 yPos = useWindow ? data.ly - data.windowY : data.scrollY + data.ly;

	const uint16 tileRow = (static_cast<int8>(yPos / 8)) * 32;

	vmdx::rgba8 pal[4];
	for (uint8 i = 0; i < 4; ++i) {
		pal[i] = palette[data.bgp[i]];
	}

	for (size_t px = 0; px < SCREEN_WIDTH; ++px) {
		uint8 xPos;

		if (useWindow) {
			xPos = px + 7 - data.windowX;
		}
		else {
			xPos = px + data.scrollX;
		}

		const uint8 tileCol = xPos / 8;
		uint16 mapNo = tileCol + tileRow;
		const uint8 line = (yPos % 8) ;

		Tile tile = data.tiles[0][data.tileMap[mapBank][mapNo]];

		const int bufferIndex = data.ly * SCREEN_WIDTH + px;
		if (bufferIndex >= TOTAL_PIXELS) {
			// DEBUG("bad tile pixel index: " << +(lY * 160 + px) << " LY: " << +(data.ly));
			continue;
		}

		unsigned y = line, x = xPos % 8;
		data.backbuffer[bufferIndex] = pal[tile[y][x]];
	}
}

void Display::drawSprites() {
	const unsigned ySize = data.sprite8x16 ? 16 : 8;
	const auto bgp0 = palette[data.bgp[0]];

	Sprite sprites[40];
	int nSprites = 0;

	for (const auto& sprite : data.oam) {
		if ((data.ly - static_cast<unsigned>(sprite.yPos)) < ySize) {
			sprites[nSprites++] = sprite;
		}
	}

	std::sort(std::begin(sprites), std::begin(sprites) + nSprites, [](Sprite a, Sprite b) {
		return a.xPos < b.xPos;
	});

	nSprites = nSprites > 10 ? 10 : nSprites;
	
	for (int i = nSprites - 1; i >= 0; --i) {
		const auto& sprite = sprites[i];

		unsigned line = data.ly - sprite.yPos;

		auto tileLocation = sprite.tileLocation;

		if (sprite.yFlip) {
			line -= ySize - 1;
			line *= -1;
		}

		if (data.sprite8x16) {
			if (line > 7) {
				tileLocation = sprite.tileLocation | 0x01;
			}
			else {
				tileLocation = sprite.tileLocation & 0xfe;
			}
		}


		for (int tilePixel = 7; tilePixel >= 0; --tilePixel) {
			int colorBit = tilePixel;

			if (!sprite.xFlip) {
				colorBit -= 7;
				colorBit *= -1;
			}

			unsigned colorID = data.tiles[0][tileLocation][line % 8][colorBit];
			if (colorID == 0)
				continue;

			const vmdx::rgba8 color = palette[data.obp[sprite.palette][colorID]];

			const int xPix = 0 - tilePixel + 7;
			const int px = sprite.xPos + xPix;
			if (px < 0) {
				continue;
			}

			const int bufferIndex = data.ly * SCREEN_WIDTH + px;
			if (bufferIndex >= TOTAL_PIXELS) {
				//DEBUG("bad sprite pixel index: " << +(lY * 160 + px));
				continue;
			}

			if (sprite.bgPriority) {
				if (data.backbuffer[bufferIndex] != bgp0) {
					continue;
				}
			}

			data.backbuffer[bufferIndex] = color;
		}
	}
}

void Display::updateTileSet() {
	for (size_t i = 0; i < 384; ++i) {
		uint16 address = i * 16;
		for (size_t y = 0; y < 8; ++y) {
			uint8 lo = data.vRam[address++];
			uint8 hi = data.vRam[address++];
			for (size_t x = 0; x < 8; ++x) {
				int colorBit = x;
				colorBit -= 7;
				colorBit *= -1;

				uint8 colorID = dx_util::getBit(hi, colorBit) << 1;
				colorID |= dx_util::getBit(lo, colorBit);
				data.tiles[0][i][y][x] = colorID;
			}
		}
	}
}

void Display::updateTileImage() {
	Palette palIndex;
	switch (data.currentViewPalette) {
	case Data::BGP:
		palIndex = data.bgp;
		break;
	case Data::OBP0:
		palIndex = data.obp[0];
		break;
	case Data::OBP1:
		palIndex = data.obp[1];
		break;
	}

	vmdx::rgba8 pal[4];
	for (uint8 i = 0; i < 4; ++i) {
		pal[i] = palette[palIndex[i]];
	}

	for (size_t tileY = 0, px = 0; tileY < 24; ++tileY) {
		size_t baseOffset = tileY * 16;
		for (size_t pxY = 0; pxY < 8; ++pxY) {
			for (size_t tileX = 0; tileX < 16; ++tileX) {
				Tile tile = data.tiles[0][baseOffset + tileX];
				for (size_t pxX = 0; pxX < 8; ++pxX) {
					tileImage[px++] = pal[tile[pxY][pxX]];
				}
			}
		}
	}
}

void Display::updateTileMap() {
	for (size_t i = 0; i < 1024; ++i) {
		uint16 address0 = BGMAP0_OFFSET + i, address1 = BGMAP1_OFFSET + i;
		int16 tileLocation0 = 0, tileLocation1 = 0;

		if(data.tileDataSelect) {
			tileLocation0 += data.vRam[address0];
			tileLocation1 += data.vRam[address1];
		}
		else {
			tileLocation0 += static_cast<int8>(data.vRam[address0]) + 256;
			tileLocation1 += static_cast<int8>(data.vRam[address1]) + 256;
		}

		data.tileMap[0][i] = tileLocation0;
		data.tileMap[1][i] = tileLocation1;
	}
}

void Display::updateTileMapImage() {
	vmdx::rgba8 pal[4];
	for (uint8 i = 0; i < 4; ++i) {
		pal[i] = palette[data.bgp[i]];
	}

	for (size_t tileY = 0, px = 0; tileY < 32; ++tileY) {
		size_t baseOffset = tileY * 32;
		for (size_t pxY = 0; pxY < 8; ++pxY) {
			for (size_t tileX = 0; tileX < 32; ++tileX) {
				Tile tile = data.tiles[0][data.tileMap[data.mapViewIndex][baseOffset + tileX]];
				for (size_t pxX = 0; pxX < 8; ++pxX) {
					tileMapImage[px++] = pal[tile[pxY][pxX]];
				}
			}
		}
	}
}

void Display::updateOAM() {
	size_t index = 0;
	for (auto& sprite : data.oam) {
		sprite.yPos = data.oamData[index++] - 16;
		sprite.xPos = data.oamData[index++] - 8;
		sprite.tileLocation = data.oamData[index++];
		uint8 attribs = data.oamData[index++];
		sprite.bgPriority = dx_util::testBit(attribs, 7);
		sprite.yFlip = dx_util::testBit(attribs, 6);
		sprite.xFlip = dx_util::testBit(attribs, 5);
		sprite.palette = dx_util::testBit(attribs, 4);
	}
}

void Display::updateOAMImage() {
	vmdx::rgba8 obp0[4];
	vmdx::rgba8 obp1[4];
	for (uint8 i = 0; i < 4; ++i) {
		obp0[i] = palette[data.obp[0][i]];
		obp1[i] = palette[data.obp[1][i]];
	}

	for (size_t tileY = 0, px = 0; tileY < 10; ++tileY) {
		size_t baseOffset = tileY * 4;
		for (size_t pxY = 0; pxY < 8; ++pxY) {
			for (size_t tileX = 0; tileX < 4; ++tileX) {
				auto sprite = data.oam[baseOffset + tileX];
				auto pal = sprite.palette ? obp1 : obp0;
				auto tile = data.tiles[0][sprite.tileLocation];
				int y = pxY;
				if (sprite.yFlip) {
					y -= 7;
					y *= -1;
				}

				for (size_t pxX = 0; pxX < 8; ++pxX) {
					int x = pxX;
					if (sprite.xFlip) {
						x -= 7;
						x *= -1;
					}

					oamImage[px++] = pal[tile[y][x]];
				}
			}
		}
	}
}

void Display::updateOAMImage8x16() {
	vmdx::rgba8 obp0[4];
	vmdx::rgba8 obp1[4];
	for (uint8 i = 0; i < 4; ++i) {
		obp0[i] = palette[data.obp[0][i]];
		obp1[i] = palette[data.obp[1][i]];
	}

	for (size_t tileY = 0, px = 0; tileY < 10; ++tileY) {
		size_t baseOffset = tileY * 4;
		for (size_t pxY = 0; pxY < 8; ++pxY) {
			for (size_t tileX = 0; tileX < 4; ++tileX) {
				auto sprite = data.oam[baseOffset + tileX];
				auto pal = sprite.palette ? obp1 : obp0;
				auto tile = data.tiles[0][sprite.tileLocation & 0xfe];
				int y = pxY;
				if (sprite.yFlip) {
					y -= 7;
					y *= -1;
				}

				for (size_t pxX = 0; pxX < 8; ++pxX) {
					int x = pxX;
					if (sprite.xFlip) {
						x -= 7;
						x *= -1;
					}

					oamImage8x16[px++] = pal[tile[y][x]];
				}
			}
		}
		for (size_t pxY = 0; pxY < 8; ++pxY) {
			for (size_t tileX = 0; tileX < 4; ++tileX) {
				auto sprite = data.oam[baseOffset + tileX];
				auto pal = sprite.palette ? obp1 : obp0;
				auto tile = data.tiles[0][sprite.tileLocation | 0x01];
				int y = pxY;
				if (sprite.yFlip) {
					y -= 7;
					y *= -1;
				}

				for (size_t pxX = 0; pxX < 8; ++pxX) {
					int x = pxX;
					if (sprite.xFlip) {
						x -= 7;
						x *= -1;
					}

					oamImage8x16[px++] = pal[tile[y][x]];
				}
			}
		}
	}
}

Display::SpriteImage Display::getSprite(size_t index) const {
	vmdx::rgba8 obp0[4];
	vmdx::rgba8 obp1[4];
	for (uint8 i = 0; i < 4; ++i) {
		obp0[i] = palette[data.obp[0][i]];
		obp1[i] = palette[data.obp[1][i]];
	}

	SpriteImage image = {};
	auto sprite = data.oam[index];
	auto pal = sprite.palette ? obp1 : obp0;
	auto tileLocation = sprite.tileLocation;
	if (data.sprite8x16) tileLocation &= 0xfe;
	auto tile1 = data.tiles[0][tileLocation];

	size_t px = 0;

	for (size_t pxY = 0; pxY < 8; ++pxY) {
		int y = pxY;
		if (sprite.yFlip) {
			y -= 7;
			y *= -1;
			DEBUG("yflip");
		}

		for (size_t pxX = 0; pxX < 8; ++pxX) {
			int x = pxX;
			if (sprite.xFlip) {
				x -= 7;
				x *= -1;
			}

			image[px++] = pal[tile1[y][x]];
		}
	}

	if (data.sprite8x16) {
		auto tile2 = data.tiles[0][sprite.tileLocation | 0x01];

		for (size_t pxY = 0; pxY < 8; ++pxY) {
			int y = pxY;
			if (sprite.yFlip) {
				y -= 7;
				y *= -1;
			}

			for (size_t pxX = 0; pxX < 8; ++pxX) {
				int x = pxX;
				if (sprite.xFlip) {
					x -= 7;
					x *= -1;
				}

				image[px++] = pal[tile2[y][x]];
			}
		}
	}

	return image;
}

}
