#include "display.h"

#include "util.h"
#include "gameboy/memory.h"
#include "gameboy/system/system.h"

namespace Gameboy {

uint8 Display::read(uint16 address) const {
	if (address >= 0x8000 && address <= 0x9fff) {
		return data.vRam[address - Memory::VRAM_OFFSET + data.vRamBank * 0x2000];
	}
	else if (address >= 0xfe00 && address <= 0xfe9f) {
		if (data.dmaStatus != DmaStatus::Completed) return 0xff;
		return data.oamData[address - Memory::OAM_OFFSET];
	}
	else if (address == Memory::LCDC) {
		return (data.lcdEnabled << 7) | (data.windowMapSelect << 6) | (data.windowEnabled << 5) | (data.tileDataSelect << 4) | (data.bgMapSelect << 3) | (data.sprite8x16 << 2) | (data.spritesEnabled << 1) | data.bgEnabled;
	}
	else if (address == Memory::STAT) {
		return (data.lycInterrupt << 6) | (data.oamInterrupt << 5) | (data.vBlankInterrupt << 4) | (data.hBlankInterrupt << 3) | (data.lycCoincidence << 2) | data.mode;
	}
	else if (address == Memory::SCX) {
		return data.scrollX;
	}
	else if (address == Memory::SCY) {
		return data.scrollY;
	}
	else if (address == Memory::LY) {
		return data.ly;
	}
	else if (address == Memory::LYC) {
		return data.lyc;
	}
	else if (address == Memory::DMA) {
		// dma
	}
	else if (address == Memory::BGP) {
		return (data.bgp[3] << 6) | (data.bgp[2] << 4) | (data.bgp[1] << 2) | data.bgp[0];
	}
	else if (address == Memory::OBP0) {
		return (data.obp[0][3] << 6) | (data.obp[0][2] << 4) | (data.obp[0][1] << 2) | data.obp[0][0];
	}
	else if (address == Memory::OBP1) {
		return (data.obp[1][3] << 6) | (data.obp[1][2] << 4) | (data.obp[1][1] << 2) | data.obp[1][0];
	}
	else if (address == Memory::WX) {
		return data.windowX;
	}
	else if (address == Memory::WY) {
		return data.windowY;
	}
	else if (address == Memory::BGPI) {
		return (data.bgpi.increment << 7) | (data.bgpi.index & 0x3f);
	}
	else if (address == Memory::BGPD) {
		return data.bgpd[data.bgpi.index];
	}
	else if (address == Memory::OBPI) {
		return (data.obpi.increment << 7) | (data.obpi.index & 0x3f);
	}
	else if (address == Memory::OBPD) {
		return data.obpd[data.obpi.index];
	}
	else if (system->model == System::Model::CGB) {
		if (address == Memory::VBK) {
			return data.vRamBank;
		}
	}

	return 0xff;
}

void Display::write(uint16 address, uint8 val) {
	if (address >= 0x8000 && address <= 0x9fff) {
		data.vRam[address - Memory::VRAM_OFFSET + data.vRamBank * 0x2000] = val;
	}
	else if (address >= 0xfe00 && address <= 0xfe9f) {
		if (data.dmaStatus == DmaStatus::Completed)
			data.oamData[address - Memory::OAM_OFFSET] = val;
	}
	else if (address == Memory::LCDC) {
		data.lcdEnabled = dx_util::testBit(val, 7);
		data.windowMapSelect = dx_util::testBit(val, 6);
		data.windowEnabled = dx_util::testBit(val, 5);
		data.tileDataSelect = dx_util::testBit(val, 4);
		data.bgMapSelect = dx_util::testBit(val, 3);
		data.sprite8x16 = dx_util::testBit(val, 2);
		data.spritesEnabled = dx_util::testBit(val, 1);
		data.bgEnabled = dx_util::testBit(val, 0);
	}
	else if (address == Memory::STAT) {
		data.lycInterrupt = dx_util::testBit(val, 6);
		data.oamInterrupt = dx_util::testBit(val, 5);
		data.vBlankInterrupt = dx_util::testBit(val, 4);
		data.hBlankInterrupt = dx_util::testBit(val, 3);
	}
	else if (address == Memory::SCX) {
		data.scrollX = val;
	}
	else if (address == Memory::SCY) {
		data.scrollY = val;
	}
	else if (address == Memory::LY) {
		data.ly = val;
	}
	else if (address == Memory::LYC) {
		data.lyc = val;
	}
	else if (address == Memory::DMA) {
		data.dmaDest = 0;
		data.dmaSource = val << 8;
		data.dmaStatus = DmaStatus::Start;
	}
	else if (address == Memory::BGP) {
		data.bgp[3] = (val & 0xc0) >> 6;
		data.bgp[2] = (val & 0x30) >> 4;
		data.bgp[1] = (val & 0x0c) >> 2;
		data.bgp[0] = val & 0x03;
	}
	else if (address == Memory::OBP0) {
		data.obp[0][3] = (val & 0xc0) >> 6;
		data.obp[0][2] = (val & 0x30) >> 4;
		data.obp[0][1] = (val & 0x0c) >> 2;
		data.obp[0][0] = val & 0x03;
	}
	else if (address == Memory::OBP1) {
		data.obp[1][3] = (val & 0xc0) >> 6;
		data.obp[1][2] = (val & 0x30) >> 4;
		data.obp[1][1] = (val & 0x0c) >> 2;
		data.obp[1][0] = val & 0x03;
	}
	else if (address == Memory::WX) {
		data.windowX = val;
	}
	else if (address == Memory::WY) {
		data.windowY = val;
	}
	else if (address == Memory::BGPI) {
		data.bgpi.increment = dx_util::testBit(val, 7);
		data.bgpi.index  = val & 0x3f;
	}
	else if (address == Memory::BGPD) {
		data.bgpd[data.bgpi.index] = val;
		if (data.bgpi.increment)
			++data.bgpi.index;
	}
	else if (address == Memory::OBPI) {
		data.obpi.increment = dx_util::testBit(val, 7);
		data.obpi.index = val & 0x3f;
	}
	else if (address == Memory::OBPD) {
		data.obpd[data.obpi.index] = val;
		if (data.obpi.increment)
			++data.obpi.index;
	}
	else if (system->model == System::Model::CGB) {
		if (address == Memory::VBK) {
			data.vRamBank = dx_util::testBit(val, 0);
		}
	}
}

// DMA supposedly takes 162 cycles to transfer 160 bytes, so add an extra cycle at the beginning and end
void Display::updateDMA() {
	switch (data.dmaStatus) {
	case DmaStatus::During:
		if (data.dmaDest < 0xA0) { 
			data.oamData[data.dmaDest++] = system->read(data.dmaSource++);
		}
		else {
			data.dmaStatus = DmaStatus::Completed;
		}
		break;
	case DmaStatus::Start:
		data.dmaStatus = DmaStatus::During;
		break;
	case DmaStatus::Completed:
		break;
	}
}

}

