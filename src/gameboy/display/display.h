#pragma once

#include "types.h"
#include "image.h"
#include "vec.h"

#include <array>

namespace Gameboy {

class System;

class Display {
public:

	using Framebuffer = Image::PixelData<vmdx::rgba8, 160, 144>;
	using TileImage = Image::PixelData<vmdx::rgba8, 128, 192>;
	using TileMapImage = Image::PixelData<vmdx::rgba8, 256, 256>;
	using OAMImage = Image::PixelData<vmdx::rgba8, 32, 80>;
	using OAMImage8x16 = Image::PixelData<vmdx::rgba8, 32, 160>;

	enum struct DmaStatus { During, Start, Completed, };

	using ImageView = Image::PixelView<vmdx::rgba8>;

	struct Sprite {
		int xPos, yPos;
		uint8 tileLocation;
		bool bgPriority;
		bool xFlip, yFlip;
		bool palette;
	};

	struct BGAttributes {
		int paletteIndex;
		bool vramBank;
		bool xFlip, yFlip;
		bool bgPriority;
	};

	using Tile = std::array<std::array<uint8, 8>, 8>;
	using TileSet = std::array<Tile, 384>;
	using TileMap = std::array<size_t, 1024>;
	using OAM = std::array<Sprite, 40>;
	using BGAttributeMap = std::array<BGAttributes, 256 * 256>;

	using Palette = std::array<unsigned, 4>;

	// the state of the display itself, including the framebuffer and the memory mapped registers
	struct Data {
		Framebuffer backbuffer, frontbuffer;
		
		// all of the tiles currently loaded in VRAM
		TileSet tiles[2] = {};
		// 2 background tile maps
		TileMap tileMap[2] = {};
		// 40 sprites in OAM
		OAM oam = {};

		// which tile map does tileMapImage represent, 0 or 1
		enum { MAP0 = 0, MAP1, } mapViewIndex = MAP0;
		enum { BGP, OBP0, OBP1, } currentViewPalette = BGP;

		int gpuClock = 0;

		bool vRamAccessible = true, oamAccessible = true;

		uint16 dmaSource = 0;
		uint16 dmaDest = 0;
		DmaStatus dmaStatus = DmaStatus::Completed;

		std::array<uint8, 0x4000> vRam = {}; // 0x8000 - 0x9FFF, bank switchable in CGB mode
		std::array<uint8, 0xA0> oamData = {}; // 0xFE00 - 0xFE9F

		// 0xff40 - LCDC
		bool lcdEnabled = 0;
		bool windowMapSelect = 0; // 0 = 0x9800, 1 = 0x9c00
		bool windowEnabled = 0;
		bool tileDataSelect = 0; // 0 = 0x8800, 1 = 0x8000
		bool bgMapSelect = 0; // 0 = 0x9800, 1 = 0x9c00
		bool sprite8x16 = 0; // otherwise 8x8
		bool spritesEnabled = 0;
		bool bgEnabled = 0;

		// 0xff41 - STAT
		bool lycInterrupt = 0;
		bool oamInterrupt = 0;
		bool vBlankInterrupt = 0;
		bool hBlankInterrupt = 0;
		bool lycCoincidence = 0; // set if ly == lyc, read only 
		unsigned mode = 0; // read only

		// 0xff42 - 0xff43
		uint8 scrollX = 0, scrollY = 0;

		// 0xff44
		uint8 ly = 0; // read only

		//0xff45
		uint8 lyc = 0;

		// 0xff47
		Palette bgp = {};

		// 0xff48 - 0xff49
		Palette obp[2] = {};
		
		// 0xff4a - 0xff4b
		uint8 windowX = 0, windowY = 0;

		// CGB registers

		// 0xff4f - VBK
		bool vRamBank = 0;

		// 0xff68 BGPI
		struct {
			uint8 index = 0;
			bool increment = false;
		} bgpi;

		struct RGBData {
			uint8 redIntensity : 5;
			uint8 greenIntensity : 5;
			uint8 blueIntensity : 5;
		};

		// 0xff69 BGPD
		std::array<uint8, 64> bgpd;

		// 0xff6a OBPI
		struct {
			uint8 index = 0;
			bool increment = false;
		} obpi;

		// 0xff6b OBPD
		std::array<uint8, 64> obpd;
	};

	Display(System* system) : system(system) {
		clearScreen();
	}

	void update(int cycles);

	uint8 read(uint16 address) const;
	void write(uint16 address, uint8 val);
	void updateDMA();

	void toggleMapViewIndex() {
		data.mapViewIndex = data.mapViewIndex == Data::MAP1 ? Data::MAP0 : Data::MAP1;
	}

	void toggleCurrentViewPalette() {
		switch (data.currentViewPalette) {
		case Data::BGP:
			data.currentViewPalette = Data::OBP0;
			break;
		case Data::OBP0:
			data.currentViewPalette = Data::OBP1;
			break;
		default:
			data.currentViewPalette = Data::BGP;
			break;
		}
	}

	ImageView getFramebuffer() const { return data.frontbuffer; }
	ImageView getTileImage() const { return tileImage; }
	ImageView getTileMapImage() const { return tileMapImage; }
	ImageView getOAMImage() const { return oamImage; }
	ImageView getOAMImage8x16() const { return oamImage8x16; }

	void postRedisplay() { wantRedisplay = false; }
	bool getWantRedisplay() const { return wantRedisplay; }

	DmaStatus getDmaStatus() const { return data.dmaStatus; }

	using SpriteImage = Image::PixelData<vmdx::rgba8, 8, 16>;
	SpriteImage getSprite(size_t index) const;

	const Data& getData() const { return data; };

	vmdx::rgba8 palette[4] {
		{ 232, 232, 232, 255 },
		{ 160, 160, 160, 255 },
		{ 88, 88, 88, 255 },
		{ 16, 16, 16, 255 },
	};

private:
	System* system;

	Data data;

	TileImage tileImage;
	TileMapImage tileMapImage;
	OAMImage oamImage;
	OAMImage8x16 oamImage8x16;

	bool wantRedisplay = true;

	void updateTileSet();
	void updateTileMap();
	void updateTileImage();
	void updateTileMapImage();
	void updateOAM();
	void updateOAMImage();
	void updateOAMImage8x16();
	void clearScreen();
	void drawTiles();
	void drawSprites();
};

}
