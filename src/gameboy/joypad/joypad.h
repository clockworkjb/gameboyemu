#pragma once

#include "types.h"

namespace Gameboy {

class Joypad {
public:
	enum struct Button {
		A = 0, B = 1, Start = 3, Select = 2, Up = 6, Down = 7, Left = 5, Right = 4,
	};

	void update(Button button, bool status);
	uint8 read() const;
	void write(uint8 val);

private:
	bool p15 = true;
	bool p14 = true;
	bool buttons[8] = {};
};

}
