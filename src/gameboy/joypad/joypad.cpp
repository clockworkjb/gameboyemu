#include "joypad.h"

#include "util.h"

namespace Gameboy {

void Joypad::update(Button button, bool status) {
	buttons[dx_util::toUnderlyingType(button)] = status;
}

uint8 Joypad::read() const {
	uint8 joyp = (p15 << 5) | (p14 << 4);
	joyp |= 0xCF;

	if (!p15) {
		for (size_t i = 0; i < 4; ++i) {
			if (buttons[i])
				joyp = dx_util::resetBit(joyp, i);
		}
	}

	if (!p14) {
		for (size_t i = 4; i < 8; ++i) {
			if (buttons[i])
				joyp = dx_util::resetBit(joyp, i - 4);
		}
	}

	return joyp;
}

void Joypad::write(uint8 val) {
	p14 = dx_util::testBit(val, 4);
	p15 = dx_util::testBit(val, 5);
}

}
