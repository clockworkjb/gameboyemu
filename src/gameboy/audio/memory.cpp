#include "audio.h"

#include "gameboy/memory.h"
#include "util.h"

namespace Gameboy {

uint8 Apu::read(uint16 address) const {
	// Channel 1
	if (address == Memory::NR10) {
		return 0x80 | (channel1.sweepTime << 4) | (channel1.sweepSubtraction << 3) | channel1.sweepShift;
	}
	else if (address == Memory::NR11) {
		return (channel1.duty << 6) | 0x3f;
	}
	else if (address == Memory::NR12) {
		return (channel1.envelopeVolume << 4) | (channel1.envelopeDirection << 3) | channel1.envelopeSweep;
	}
	else if (address == Memory::NR13) {
		return 0xff;
	}
	else if (address == Memory::NR14) {
		return 0x80 | (channel1.useCounter << 6) | 0x3f;
	}

	// Channel 2
	else if (address == Memory::NR21) {
		return (channel2.duty << 6) | 0x3f;
	}
	else if (address == Memory::NR22) {
		return (channel2.envelopeVolume << 4) | (channel2.envelopeDirection << 3) | channel2.envelopeSweep;
	}
	else if (address == Memory::NR23) {
		return 0xff;
	}
	else if (address == Memory::NR24) {
		return 0x80 | (channel2.useCounter << 6) | 0x3f;
	}

	// Channel 3
	else if (address == Memory::NR30) {
		return (channel3.enabled << 7) | 0x7f;
	}
	else if (address == Memory::NR31) {
		return 0xff;
	}
	else if (address == Memory::NR32) {
		return 0x80 | (channel3.volume << 5) | 0x1f;
	}
	else if (address == Memory::NR33) {
		return 0xff;
	}
	else if (address == Memory::NR34) {
		return 0x80 | (channel3.useCounter << 6) | 0x3f;
	}
	else if (address >= 0xff30 && address <= 0xff3f) { // wave pattern ram
		return channel3.wavePatternRam[address - 0xff30];
	}

	// Channel 4
	else if (address == Memory::NR41) {
		return 0xff;
	}
	else if (address == Memory::NR42) {
		return (channel4.envelopeVolume << 4) | (channel4.envelopeDirection << 3) | channel4.envelopeSweep;
	}
	else if (address == Memory::NR43) {
		return (channel4.frequency << 4) | (channel4.counterStep << 3) | channel4.dividingRatio;
	}
	else if (address == Memory::NR44) {
		return 0x80 | (channel4.useCounter << 6) | 0x3f;
	}

	else if (address == Memory::NR50) {
		return (leftEnabled << 7) | (leftVolume << 4) | (rightEnabled << 3) | rightVolume;
	}
	else if (address == Memory::NR51) {
		return (channel4.leftEnabled << 7) | (channel3.leftEnabled << 6) | (channel2.leftEnabled << 5) | (channel1.leftEnabled << 4) | (channel4.rightEnabled << 3) | (channel3.rightEnabled << 2) | (channel2.rightEnabled << 1) | channel1.rightEnabled;
	}
	else if (address == Memory::NR52) {
		return (globalEnable << 7) | 0x70 | (channel4.enabled << 3) | (channel3.enabled << 2) | (channel2.enabled << 1) | channel1.enabled;
	}

	return 0xff;
}

void Apu::write(uint16 address, uint8 val) {
	// Channel 1
	if (address == Memory::NR10) {
		channel1.sweepShift = (val & 0x70) >> 4;
		channel1.sweepSubtraction = dx_util::testBit(val, 3);
		channel1.sweepTime = val & 0x7;
	}
	else if (address == Memory::NR11) {
		channel1.duty = (val & 0xc0) >> 6;
		channel1.length = 64 - (val & 0x3f);
	}
	else if (address == Memory::NR12) {
		channel1.envelopeVolume = (val & 0xf0) >> 4;
		channel1.envelopeDirection = dx_util::testBit(val, 3);
		channel1.envelopeSweep = val & 0x7;
	}
	else if (address == Memory::NR13) {
		channel1.frequency = (channel1.frequency & 0xff00) | val;
	}
	else if (address == Memory::NR14) {
		channel1.useCounter = dx_util::testBit(val, 6);
		channel1.frequency = (channel1.frequency & 0xff) | ((val & 0x7) << 8);

		if (dx_util::testBit(val, 7)) {
			// do restart

			channel1.period = (2048 - channel1.frequency) * 2;
		}
	}

	// Channel 2
	else if (address == Memory::NR21) {
		channel2.duty = (val & 0xc0) >> 6;
		channel2.length = 64 - (val & 0x3f);
	}
	else if (address == Memory::NR22) {
		channel2.envelopeVolume = (val & 0xf0) >> 4;
		channel2.envelopeDirection = dx_util::testBit(val, 3);
		channel2.envelopeSweep = val & 0x7;
	}
	else if (address == Memory::NR23) {
		channel2.frequency = (channel2.frequency & 0xff00) | val;
	}
	else if (address == Memory::NR24) {
		channel2.useCounter = dx_util::testBit(val, 6);
		channel2.frequency = (channel2.frequency & 0xff) | ((val & 0x7) << 8);

		if (dx_util::testBit(val, 7)) {
			// do restart
		}
	}

	// Channel 3
	else if (address == Memory::NR30) {
		channel3.enabled = dx_util::testBit(val, 7);
	}
	else if (address == Memory::NR31) {
		channel3.length = 256 - val;
	}
	else if (address == Memory::NR32) {
		channel3.volume = (val & 0x60) >> 5;
	}
	else if (address == Memory::NR33) {
		channel3.frequency = (channel3.frequency & 0xff00) | val;
	}
	else if (address == Memory::NR34) {
		channel3.useCounter = dx_util::testBit(val, 6);
		channel3.frequency = (channel3.frequency & 0xff) | ((val & 0x7) << 8);

		if (dx_util::testBit(val, 7)) {
			// do restart
		}
	}
	else if (address >= 0xff30 && address <= 0xff3f) { // wave pattern ram
		channel3.wavePatternRam[address - 0xff30] = val;
	}
	
	// Channel 4
	else if (address == Memory::NR41) {
		channel4.length = 64 - (val & 0x3f);
	}
	else if (address == Memory::NR42) {
		channel4.envelopeVolume = (val & 0xf0) >> 4;
		channel4.envelopeDirection = dx_util::testBit(val, 3);
		channel4.envelopeSweep = val & 0x7;
	}
	else if (address == Memory::NR43) {
		channel4.frequency = (val & 0xf0) >> 4;
		channel4.counterStep = dx_util::testBit(val, 3);
		channel4.dividingRatio = val & 0x7;
	}
	else if (address == Memory::NR44) {
		channel4.useCounter = dx_util::testBit(val, 6);

		if (dx_util::testBit(val, 7)) {
			// do restart
		}
	}

	else if (address == Memory::NR50) {
		leftEnabled = dx_util::testBit(val, 7);
		leftVolume = (val & 0x70) >> 4;
		rightEnabled = dx_util::testBit(val, 3);
		rightVolume = val & 0x7;
	}
	else if (address == Memory::NR51) {
		channel4.leftEnabled = dx_util::testBit(val, 7);
		channel3.leftEnabled = dx_util::testBit(val, 6);
		channel2.leftEnabled = dx_util::testBit(val, 5);
		channel2.leftEnabled = dx_util::testBit(val, 4);
		channel4.rightEnabled = dx_util::testBit(val, 3);
		channel3.rightEnabled = dx_util::testBit(val, 2);
		channel2.rightEnabled = dx_util::testBit(val, 1);
		channel1.rightEnabled = dx_util::testBit(val, 0);
	}
	else if (address == Memory::NR52) {
		bool enableFlag = dx_util::testBit(val, 7);

		if(globalEnable) {
			if (!enableFlag) {
				leftEnabled = false;
				rightEnabled = false;
				leftVolume = 0;
				rightVolume = 0;

				channel1 = {};
				channel2 = {};
				channel3 = {};
				channel4 = {};
			}
		}

		globalEnable = enableFlag;
	}
}

}
