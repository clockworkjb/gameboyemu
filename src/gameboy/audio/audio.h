#pragma once

#include "types.h"

#include <array>

using WavePatternRam = std::array<uint8, 16>;

namespace Gameboy {

struct Apu {
	struct Channel {
		uint16 output = 0;
		bool enabled = false;
		bool leftEnabled = false, rightEnabled = false;

		unsigned length = 0;
		unsigned volume = 0;
		unsigned period = 0;
		unsigned phase = 0;

		unsigned frequency = 0;
		bool useCounter = false;

		virtual void run() = 0;
	};

	struct SquareChannel : public Channel {
		unsigned duty = 0;

		unsigned envelopeVolume = 0;
		bool envelopeDirection = false; // 0 = decrease, 1 = increase
		unsigned envelopeSweep = 0;
	};

	// NR10 (0xff10) - NR14 (0xff14)
	struct Channel1 : public SquareChannel {
		unsigned sweepShift = 0; // shift by n / 128hz
		bool sweepSubtraction = false; // 0 = addition, 1 = subtraction
		unsigned sweepTime = 0;

		void run() override;
	} channel1;

	// NR21 (0xff16) - NR24 (0xff19)
	struct Channel2 : public SquareChannel {
		void run() override;
	} channel2;

	// NR30 (0xff1a) - NR34 (0xff1e)
	struct Channel3 : public Channel {
		WavePatternRam wavePatternRam = {};
		
		void run() override;
	} channel3;

	// NR41 (0xff20) - NR44 (0xff23)
	struct Channel4 : public Channel {
		unsigned envelopeVolume = 0;
		bool envelopeDirection = false; // 0 = decrease, 1 = increase
		unsigned envelopeSweep = 0;

		bool counterStep = false; // 0 = 15 bit, 1 = 7 bits
		unsigned dividingRatio = 0;

		void run() override;
	} channel4;

	bool globalEnable = false;

	bool leftEnabled = false;
	unsigned leftVolume = 0;
	bool rightEnabled = false;
	unsigned rightVolume = 0;

	void update(int cycles = 1);

	uint8 read(uint16 address) const;
	void write(uint16 address, uint8 val);

	int phase = 0, cycle = 0;
};

}
