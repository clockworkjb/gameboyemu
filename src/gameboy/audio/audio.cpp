#include "audio.h"

namespace Gameboy {

// this is set up to be sync to CPU, we should change this so that the CPU syncs to audio
void Apu::update(int cycles) {
	while (cycles--) {
		if (!globalEnable)
			continue;

		channel1.run();
		channel2.run();
		channel3.run();
		channel4.run();

		if (++cycle == 4096) { // 512hz
			cycle = 0;
			if (++phase == 8)
				phase = 0;

			if (phase % 2 == 0) { // 256hz
				// clock all
			}
			if (phase == 2 || phase == 6) { // 128hz
				// square 1 sweep
				if (channel1.useCounter)
					if (--channel1.length == 0)
						channel1.enabled = false;
			}
			if (phase == 7) { // 64hz
				// envelope all
			}
		}
	}
}

void Apu::Channel1::run() {
	if (--period == 0) {
		period = (2048 - frequency) * 2;
		phase++;

		output = 0;

		if (!enabled)
			return;

		switch (duty) {
			case 0:
				if (phase != 6)
					return;
				break;
			case 1:
				if (phase <= 5)
					return;
				break;
			case 2:
				if (phase <= 3)
					return;
				break;
			case 3:
				if (phase >= 6)
					return;
				break;
			default:
				break;
		}

		output = volume;
	}
}

void Apu::Channel2::run() {
}

void Apu::Channel3::run() {
}

void Apu::Channel4::run() {
}

}
