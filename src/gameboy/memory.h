#pragma once

#include "types.h"

namespace Gameboy::Memory {
	// memory area offsets
	constexpr uint16 CART_ROM_OFFSET = 0x0000;
	constexpr uint16 VRAM_OFFSET = 0x8000;
	constexpr uint16 CART_RAM_OFFSET = 0xa000;
	constexpr uint16 WRAM_OFFSET = 0xc000;
	constexpr uint16 WRAM_ECHO_OFFSET = 0xe000;
	constexpr uint16 OAM_OFFSET = 0xfe00;
	constexpr uint16 UNUSABLE_OFFSET = 0xfea0;
	constexpr uint16 IO_OFFSET = 0xff00;
	constexpr uint16 HIGH_RAM_OFFSET = 0xff80;

	enum { // io register addresses
		JOYP = 0xFF00, // joypad

		// serial
		SB = 0xFF01, // serial transfer data
		SC = 0xFF02, // serial transfer control

		// timing
		DIV = 0xFF04, // divider
		TIMA = 0xFF05, // timer counter
		TMA = 0xFF06, // timer modulo
		TAC = 0xFF07, // timer control

		// interrupt
		IF = 0xFF0F, // interrupt flag
		IE = 0xFFFF, // interrupt enable

		// video
		LCDC = 0xFF40, // lcd control
		STAT = 0xFF41, // lcd status
		SCY = 0xFF42, // scroll y
		SCX = 0xFF43, // scroll x
		LY = 0xFF44, // lcdc y coordinate
		LYC = 0xFF45, // ly compare
		DMA = 0xFF46, // direct memory access
		BGP = 0xFF47, // bg pallete data
		OBP0 = 0xFF48, // object pallete 0
		OBP1 = 0xFF49, // object pallete 1
		WY = 0xFF4A, // window y position
		WX = 0xFF4B, // window x position

		// CGB video
		VBK = 0xff4f, // video ram bank
		BGPI = 0xff68, // background palette index
		BGPD = 0xff69, // background palette data
		OBPI = 0xff6a, // sprite palette index
		OBPD = 0xff6b, // sprite palette data

		// CGB HDMA
		HDMA1 = 0xff51, // DMA source high
		HDMA2 = 0xff52, // DMA source low
		HDMA3 = 0xff53, // DMA destination high
		HDMA4 = 0xff54, // DMA destination low
		HDMA5 = 0xff55, // DMA length/mode/start

		// audio
		NR10 = 0xFF10, // channel 1 sweep register
		NR11 = 0xFF11, // channel 1 sound length / wave duty pattern
		NR12 = 0xFF12, // channel 1 volume envelope
		NR13 = 0xFF13, // channel 1 frequency low
		NR14 = 0xFF14, // channel 1 frequency high
		NR21 = 0xFF16, // channel 2 sound length / wave pattern duty
		NR22 = 0xFF17, // channel 2 volume envelope
		NR23 = 0xFF18, // channel 2 frequency low data
		NR24 = 0xFF19, // channel 2 frequency high data
		NR30 = 0xFF1A, // channel 3 sound on/off
		NR31 = 0xFF1B, // channel 3 soud length
		NR32 = 0xFF1C, // channel 3 select output
		NR33 = 0xFF1D, // channel 3 frequency low data
		NR34 = 0xFF1E, // channel 3 frequency high data
		NR41 = 0xFF20, // channel 4 sound length
		NR42 = 0xFF21, // channel 4 volume envelope
		NR43 = 0xFF22, // channel 4 polynomial counter
		NR44 = 0xFF23, // channel 4 counter consecutive/initial
		NR50 = 0xFF24, // channel control / on/off / volume
		NR51 = 0xFF25, // selection of sound output terminal
		NR52 = 0xFF26, // sound on/off

		// CGB special registers
		KEY1 = 0xff4d, // CGB speed switch
		RP = 0xff56, // infrared communication port
		SVBK = 0xff70, // WRAM bank
	};
} // namespace Gameboy::Memory
