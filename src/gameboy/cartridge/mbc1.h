#pragma once

#include "cartridge.h"

namespace Gameboy {

class Mbc1 : public Cartridge {
public:
	Mbc1(CartInfo info, const std::vector<uint8>& buffer) : Cartridge(info, buffer) {}

	~Mbc1() {}

	uint8 read(uint16 address) const override;
	void write(uint16 address, uint8 val) override;

private:
	uint8 romBankSelect = 1;
	uint8 ramBankSelect = 0;
	bool bankingMode = 0;
	bool enableRamWrite = false;
};

} // namespace Gameboy
