#include "mbc5.h"

namespace Gameboy {

uint8 Mbc5::read(uint16 address) const {
	if (address <= 0x3fff) {
		return rom[address];
	}
	else if (address >= 0x4000 && address <= 0x7fff) {
		return rom[romBankSelect * 0x4000 + (address & 0x3fff)];
	}
	else if (address >= 0xa000 && address <= 0xbfff) {
		if (info.ram && enableRamWrite)
			return ram[ramBankSelect * 0x2000 + (address & 0x1fff)];
	}

	return 0xff;
}

void Mbc5::write(uint16 address, uint8 val) {
	if (address <= 0x1fff) {
		enableRamWrite = (val & 0x0F) == 0x0A;
	}
	else if (address >= 0x2000 && address <= 0x2fff) {
		romBankSelect &= ~(0x1ff);
		romBankSelect = val & 0x1ff;
	}
	else if (address >= 0x3000 && address <= 0x3fff) {
		romBankSelect &= ~(0x200);
		romBankSelect = val & 0x200;
	}
	else if (address >= 0x4000 && address <= 0x5fff) {
		ramBankSelect = val & 0x0f;
	}
	else if (address >= 0xa000 && address <= 0xbfff) {
		if (info.ram && enableRamWrite)
			ram[ramBankSelect * 0x2000 + (address & 0x1fff)] = val;
	}
}

} // namespace Gameboy
