#pragma once

#include "cartridge.h"

namespace Gameboy {

class MbcNone : public Cartridge {
public:
	MbcNone(CartInfo info, const std::vector<uint8>& buffer) : Cartridge(info, buffer) {}

	uint8 read(uint16 address) const override;
	void write(uint16 address, uint8 val) override;
};

} // namespace Gameboy
