#pragma once

#include "cartridge.h"

namespace Gameboy {

class Mbc2 : public Cartridge {
public:
	Mbc2(CartInfo info, const std::vector<uint8>& buffer) : Cartridge(info, buffer) {}

	~Mbc2() {}

	uint8 read(uint16 address) const override;
	void write(uint16 address, uint8 val) override;

private:
	uint8 romBankSelect = 1;
	bool enableRamWrite = false;
};

} // namespace Gameboy
