#include "cartridge.h"

#include "mbc_none.h"
#include "mbc1.h"
#include "mbc2.h"
#include "mbc3.h"
#include "mbc5.h"
#include "debug.h"
#include "hex.h"

#include "io.h"

namespace Gameboy {

Cartridge::Cartridge(CartInfo info, const std::vector<uint8>& buffer) : info(info) {
	rtc.latched = 0;

	rtc.halt = true;
	rtc.second = 0;
	rtc.minute = 0;
	rtc.hour = 0;
	rtc.day = 0;
	rtc.dayCarry = false;
	rtc.latch.second = 0;
	rtc.latch.minute = 0;
	rtc.latch.hour = 0;
	rtc.latch.day = 0;
	rtc.latch.dayCarry = false;

	rom = buffer;
	ram.resize(info.ramSize);

	if (info.battery && info.numRamBanks > 0) {
		loadSRAM();
	}

	DEBUG(*this);
}

Cartridge::~Cartridge() {
	if (info.battery) {
		saveSRAM();
	}

	if (info.rtc) {
		saveRTC();
	}
}

constexpr size_t calcRomSize(size_t n) { return 0x8000 << n; }

std::unique_ptr<Cartridge> Cartridge::loadRom(const std::vector<uint8>& buffer) {
	CartInfo info;

	info.gbc = buffer[CartInfo::gbcOffset] == CartInfo::gbcTest;
	info.sgb = buffer[CartInfo::sgbOffset] == CartInfo::sgbTest;

	size_t titleSize = info.gbc ? 15 : 16;

	auto nameBegin = std::begin(buffer) + CartInfo::titleOffset;
	auto nameEnd = nameBegin + titleSize;
	for (auto it = nameBegin; *it != 0 && it != nameEnd; ++it) {
		info.name += *it;
	}

	switch (buffer[CartInfo::mapperOffset]) {
	case 0x00:
		info.mapper = Mapper::None;
		break;
	case 0x01:
		info.mapper = Mapper::MBC1;
		break;
	case 0x02:
		info.mapper = Mapper::MBC1;
		info.ram = true;
		break;
	case 0x03:
		info.mapper = Mapper::MBC1;
		info.ram = true;
		info.battery = true;
		break;
	case 0x05:
		info.mapper = Mapper::MBC2;
		break;
	case 0x06:
		info.mapper = Mapper::MBC2;
		info.battery = true;
		break;
	case 0x08:
		info.mapper = Mapper::None;
		info.ram = true;
		break;
	case 0x09:
		info.mapper = Mapper::None;
		info.ram = true;
		info.battery = true;
		break;
	case 0x0F:
		info.mapper = Mapper::MBC3;
		info.rtc = true;
		info.battery = true;
		break;
	case 0x10:
		info.mapper = Mapper::MBC3;
		info.rtc = true;
		info.ram = true;
		info.battery = true;
		break;
	case 0x11:
		info.mapper = Mapper::MBC3;
		break;
	case 0x12:
		info.mapper = Mapper::MBC3;
		info.ram = true;
		break;
	case 0x13:
		info.mapper = Mapper::MBC3;
		info.ram = true;
		info.battery = true;
		break;
	case 0x19:
		info.mapper = Mapper::MBC5;
		break;
	case 0x1A:
		info.mapper = Mapper::MBC5;
		info.ram = true;
		break;
	case 0x1B:
		info.mapper = Mapper::MBC5;
		info.ram = true;
		info.battery = true;
		break;
	case 0x1C:
		info.mapper = Mapper::MBC5;
		info.rumble = true;
		break;
	case 0x1D:
		info.mapper = Mapper::MBC5;
		info.ram = true;
		info.rumble = true;
		break;
	case 0x1E:
		info.mapper = Mapper::MBC5;
		info.ram = true;
		info.battery = true;
		info.rumble = true;
		break;
	default:
		info.mapper = Mapper::Unimplemented;
		break;
	}

	auto romSizeVal = buffer[CartInfo::romSizeOffset];
	switch (romSizeVal) {
	case 0x00:
		info.romSize = calcRomSize(romSizeVal);
		info.numRomBanks = 2;
		break;
	case 0x01:
	case 0x02:
	case 0x03:
	case 0x04:
	case 0x05:
	case 0x06:
		info.romSize = calcRomSize(romSizeVal);
		info.numRomBanks = 2 << romSizeVal;
		break;
	case 0x52:
	case 0x53:
	case 0x54:
	default:
		info.romSize = 0;
		info.numRomBanks = 0;
		break;
	}

	if (info.romSize != buffer.size()) {
		DEBUG("Invalid rom size in header!");
		return nullptr;
	}

	DEBUG("ram size info: " << +(buffer[CartInfo::ramSizeOffset]));
	switch (buffer[CartInfo::ramSizeOffset]) {
	case 0x00:
		if (info.mapper == Mapper::MBC2) {
			info.numRamBanks = 1;
			info.ramSize = 512;
		}
		else {
			info.ram = false;
			info.ramSize = 0;
			info.numRamBanks = 0;
		}
		break;
	case 0x01:
		info.ramSize = 2048;
		info.numRamBanks = 1;
		break;
	case 0x02:
		info.ramSize = 8192;
		info.numRamBanks = 1;
		break;
	case 0x03:
		info.ramSize = 32768;
		info.numRamBanks = 4;
		break;
	case 0x04:
		info.ramSize = 131072;
		info.numRamBanks = 16;
		break;
	default:
		info.ram = false;
		info.ramSize = 0;
		info.numRamBanks = 0;
		break;
	}

	switch (info.mapper) {
	case Mapper::None:
		return std::make_unique<MbcNone>(info, buffer);
	case Mapper::MBC1:
		return std::make_unique<Mbc1>(info, buffer);
	case Mapper::MBC2:
		return std::make_unique<Mbc2>(info, buffer);
	case Mapper::MBC3:
		return std::make_unique<Mbc3>(info, buffer);
	case Mapper::MBC5:
		return std::make_unique<Mbc5>(info, buffer);
	default:
		DEBUG("Unimplemented mapper");
		return nullptr;
	}
}

void Cartridge::saveSRAM() {
	IO::fs::path savPath = "sav";

	if (!IO::createDir(savPath)) {
		std::cerr << "Could not access save file firectory " << savPath << "\n";
		return;
	}

	const std::string filename = info.name + ".sram";
	savPath /= filename;

	IO::saveFile(savPath, ram);
}

void Cartridge::loadSRAM() {
	IO::fs::path savPath = "sav";
	const std::string filename = info.name + ".sram";
	savPath /= filename;

	if (!IO::fs::exists(savPath)) {
		std::cerr << "Could not access save file " << savPath << "\n";
		return;
	}

	auto buffer = IO::loadFile(savPath);

	if (buffer.empty()) {
		std::cerr << "SRAM file not present\n";
		return;
	}

	if (buffer.size() != info.ramSize) {
		std::cerr << "SRAM file size incorrect\n";
		return;
	}

	ram = buffer;

	if (info.rtc) {
		//std::string filename = info.name + ".rtc";
		//loadRTC();
	}
}

std::ostream& operator << (std::ostream& o, Cartridge::Mapper mapper) {
	return o << Cartridge::mapperNames[dx_util::toUnderlyingType(mapper)];
}

std::ostream& operator << (std::ostream& o, const Cartridge& cart) {
	o << "title: " << cart.info.name << "\n";

	o << "GBC: " << (cart.info.gbc ? "yes" : "no") << " | ";
	o << "SGB: " << (cart.info.sgb ? "yes" : "no") << "\n";

	o << "Rom size: " << cart.info.romSize << "b " << cart.info.numRomBanks << " banks" << "\n";
	o << "Ram size: " << cart.info.ramSize << "b " << cart.info.numRamBanks << " banks" << "\n";

	o << "Mapper: " << hex(cart.rom[Cartridge::CartInfo::mapperOffset], 2) << " ";
	o << cart.info.mapper << "\n";

	return o;
}

} // namespace Gameboy
