#pragma once

#include "cartridge.h"

namespace Gameboy {

class Mbc3 : public Cartridge {
public:
	Mbc3(CartInfo info, const std::vector<uint8>& buffer) : Cartridge(info, buffer) {}

	~Mbc3() {}

	uint8 read(uint16 address) const override;
	void write(uint16 address, uint8 val) override;

private:
	uint8 romBankSelect = 1;
	uint8 ramBankSelect = 0;
	bool enableRamWrite = false;
};

} // namespace Gameboy
