#include "mbc1.h"

#include "debug.h"
#include "hex.h"

namespace Gameboy {

uint8 Mbc1::read(uint16 address) const {
	if (address <= 0x3fff) {
		return rom[address];
	}
	else if (address >= 0x4000 && address <= 0x7fff) {
		return rom[romBankSelect * 0x4000 + (address & 0x3fff)];
	}
	else if (address >= 0xa000 && address <= 0xbfff) {
		if (info.ram && enableRamWrite) {
			if (bankingMode == 0) {
				return ram[address & 0x1fff];
			}
			else {
				return ram[ramBankSelect * 0x2000 + (address & 0x1fff)];
			}
		}
	}

	return 0xff;
}

void Mbc1::write(uint16 address, uint8 val) {
		if (address <= 0x1fff) {
			enableRamWrite = (val & 0x0f) == 0x0a;
		}
		else if (address >= 0x2000 && address <= 0x3fff) {
			romBankSelect = (val & 0x1f) % info.numRomBanks;
			if (romBankSelect == 0)
				romBankSelect = 1;
		}
		else if (address >= 0x4000 && address <= 0x5fff) {
			ramBankSelect = (val & 0x03) % info.numRamBanks;
		}
		else if (address >= 0x6000 && address <= 0x7fff) {
			bankingMode = val & 0x01;
		}
		else if (address >= 0xa000 && address <= 0xbfff) {
			if (info.ram && enableRamWrite) {
				if (bankingMode == 0) {
					ram[address & 0x1fff] = val;
				}
				else {
					ram[ramBankSelect * 0x2000 + (address & 0x1fff)] = val;
				}
			}
		}
}

} // namespace Gameboy
