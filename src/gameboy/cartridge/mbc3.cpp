#include "mbc3.h"

#include "debug.h"
#include "io.h"

#include <fstream>
#include <ctime>

namespace Gameboy {

uint8 Mbc3::read(uint16 address) const {
	if (address <= 0x3fff) {
		return rom[address];
	}
	else if (address >= 0x4000 && address <= 0x7fff) {
		return rom[romBankSelect * 0x4000 + (address & 0x3fff)];
	}
	else if (address >= 0xa000 && address <= 0xbfff) {
		if (enableRamWrite) {
			switch (ramBankSelect) {
			case 0x00:
			case 0x01:
			case 0x02:
			case 0x04:
				if (info.ram)
					return ram[ramBankSelect * 0x2000 + (address & 0x1fff)];
				break;
			case 0x08:
				return rtc.latch.second;
			case 0x09:
				return rtc.latch.minute;
			case 0x0a:
				return rtc.latch.hour;
			case 0x0b:
				return rtc.latch.day;
			case 0x0c:
				return (rtc.latch.dayCarry << 7) | (rtc.latch.day >> 8);
			}
		}
	}

	return 0xff;
}

void Mbc3::write(uint16 address, uint8 val) {
	if (address <= 0x1fff) {
		enableRamWrite = (val & 0x0F) == 0x0A;
	}
	else if (address >= 0x2000 && address <= 0x3fff) {
		romBankSelect = (val & 0x7F) % info.numRomBanks;
		if (romBankSelect == 0)
			romBankSelect = 0x01;
	}
	else if (address >= 0x4000 && address <= 0x5fff) {
		ramBankSelect = val;
	}
	else if (address >= 0x6000 && address <= 0x7fff) {
		if (info.rtc) {
			if (!rtc.latched && val == 0x01) {
				rtc.latch.second = rtc.second;
				rtc.latch.minute = rtc.minute;
				rtc.latch.hour = rtc.hour;
				rtc.latch.day = rtc.day;
				rtc.latch.dayCarry = rtc.dayCarry;
			}

			rtc.latched = val;
		}
	}
	else if (address >= 0xa000 && address <= 0xbfff) {
		if (enableRamWrite) {
			switch (ramBankSelect) {
			case 0x00:
			case 0x01:
			case 0x02:
			case 0x04:
				if(info.ram)
					ram[ramBankSelect * 0x2000 + (address & 0x1fff)] = val;
				return;
			case 0x08:
				if (info.rtc)
					rtc.second = val >= 60 ? 0 : val;
				return;
			case 0x09:
				if (info.rtc)
					rtc.minute = val >= 60 ? 0 : val;
				return;
			case 0x0a:
				if (info.rtc)
					rtc.hour = val >= 24 ? 0 : val;
				return;
			case 0x0b:
				if (info.rtc)
					rtc.day = (rtc.day & 0x0100) | val;
				return;
			case 0x0c:
				if (info.rtc) {
					rtc.day = ((val & 1) << 8) | (rtc.day & 0xff);
					rtc.halt = val & 0x40;
					rtc.dayCarry = val & 0x80;
				}
				return;
			}
		}
	}
}

void Cartridge::incrementRTC() {
	if (!rtc.halt) {
		if (++rtc.second >= 60) {
			rtc.second = 0;

			if (++rtc.minute >= 60) {
				rtc.minute = 0;

				if (++rtc.hour >= 24) {
					rtc.hour = 0;

					if (++rtc.day >= 512) {
						rtc.day = 0;
						rtc.dayCarry = true;
					}
				}
			}
		}
	}
}

void Cartridge::saveRTC() {
	IO::fs::path savPath = "sav";

	if (!IO::createDir(savPath)) {
		std::cerr << "Could not access save file firectory " << savPath << "\n";
		return;
	}

	const std::string filename = info.name + ".rtc";
	savPath /= filename;

	std::ofstream out(savPath, std::ios::out | std::ios::binary);

	out.write(reinterpret_cast<char*>(&rtc.second), 4);
	out.write(reinterpret_cast<char*>(&rtc.minute), 4);
	out.write(reinterpret_cast<char*>(&rtc.hour), 4);
	out.write(reinterpret_cast<char*>(&rtc.day), 8);

	out.write(reinterpret_cast<char*>(&rtc.latch.second), 4);
	out.write(reinterpret_cast<char*>(&rtc.latch.minute), 4);
	out.write(reinterpret_cast<char*>(&rtc.latch.hour), 4);
	out.write(reinterpret_cast<char*>(&rtc.latch.day), 8);

	auto currentTime = std::time(nullptr);
	out.write(reinterpret_cast<char*>(&currentTime), 8);
}

void Cartridge::loadRTC() {
	// std::vector<uint8> buffer;

	// try {
	// 	buffer = IO::loadFile(filename);
	// }
	// catch(std::ios_base::failure &e) {
	// 	std::cerr << "RTC file not present\n";
	// 	return;
	// }

	// auto savedTime = static_cast<const std::time_t>(buffer.data()[40]);
	// auto timeDiff = std::time(nullptr) - savedTime;
	// auto tmDiff = std::localtime(&timeDiff);
	// auto tmOld = std::localtime(&savedTime);
	auto timeDiff = std::time(nullptr);
	auto tmDiff = std::localtime(&timeDiff);

	rtc.second = tmDiff->tm_sec;
	rtc.minute = tmDiff->tm_min;
	rtc.hour = tmDiff->tm_hour;
	rtc.day = tmDiff->tm_yday & 0xFF;
	rtc.dayCarry = tmDiff->tm_yday >> 8;

	rtc.latch.second = rtc.second;
	rtc.latch.minute = rtc.minute;
	rtc.latch.hour = rtc.hour;
	rtc.latch.day = rtc.day;
	rtc.latch.dayCarry = rtc.dayCarry;
}

} // namespace Gameboy
