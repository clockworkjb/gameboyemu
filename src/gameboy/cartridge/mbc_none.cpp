#include "mbc_none.h"

namespace Gameboy {

uint8 MbcNone::read(uint16 address) const {
	if (address <= 0x7fff) {
		return rom[address];
	}
	else if (address >= 0xa000 && address <= 0xbfff) {
		if (info.ram)
			return ram[address & 0x1fff];
	}

	return 0xff;
}

void MbcNone::write(uint16 address, uint8 val) {
	if (!info.ram)
		return;
	
	if (address >= 0xa000 && address <= 0xbfff) {
		ram[address & 0x1fff] = val;
	}
}

} // namespace Gameboy
