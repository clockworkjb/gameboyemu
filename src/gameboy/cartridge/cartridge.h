#pragma once

#include "types.h"
#include "util.h"

#include <string>
#include <vector>
#include <memory>
#include <iostream> // std::ostream

namespace Gameboy {

class Cartridge {
protected:
	struct CartInfo;

public:
	enum struct Mapper {
		None, MBC1, MBC2, MBC3, MBC5, Unimplemented,
	};

	static constexpr const char* mapperNames[] = {
		"None", "MBC1", "MBC2", "MBC3", "MBC5", "Unimplemented",
	};

	Cartridge(CartInfo info, const std::vector<uint8>& buffer);
	~Cartridge();

	static std::unique_ptr<Cartridge> loadRom(const std::vector<uint8>& buffer);

	virtual uint8 read(uint16 address) const = 0;
	virtual void write(uint16 address, uint8 val) = 0;

	const CartInfo& getInfo() const { return info; }

	friend std::ostream& operator << (std::ostream& o, const Cartridge& cart);

	// cartridge_mbc3.cpp
	void incrementRTC();

protected:

	struct CartInfo {
		enum {
			titleOffset = 0x134,
			mapperOffset = 0x147,
			gbcOffset = 0x143,
			gbcTest = 0x80,
			sgbOffset = 0x146,
			sgbTest = 0x03,
			romSizeOffset = 0x148,
			ramSizeOffset = 0x149,
		};

		std::string name;

		bool gbc = false;
		bool sgb = false;

		Mapper mapper = Mapper::Unimplemented;
		bool ram = false;
		bool battery = false;
		bool rtc = false;
		bool rumble = false;
		bool camera = false;

		size_t romSize = 0;
		size_t numRomBanks = 0;

		size_t ramSize = 0;
		size_t numRamBanks = 0;
	} info;

	std::vector<uint8> rom;
	std::vector<uint8> ram;

	struct {
		bool halt;
		unsigned second;
		unsigned minute;
		unsigned hour;
		unsigned day;
		bool dayCarry;

		struct {
			unsigned second;
			unsigned minute;
			unsigned hour;
			unsigned day;
			bool dayCarry;
		} latch;

		bool latched;
	} rtc;

private:
	void saveSRAM();
	void loadSRAM();

	// cartridge_mbc3.cpp
	void saveRTC();
	void loadRTC();
};

std::ostream& operator << (std::ostream& o, Cartridge::Mapper mapper);

} // namespace Gameboy
