#include "mbc2.h"

#include "debug.h"
#include "hex.h"
#include "util.h"

namespace Gameboy {

uint8 Mbc2::read(uint16 address) const {
	if (address <= 0x3fff) {
		return rom[address];
	}
	else if (address >= 0x4000 && address <= 0x7fff) {
		return rom[romBankSelect * 0x4000 + (address & 0x3fff)];
	}
	else if (address >= 0xa000 && address <= 0xa1ff) {
		if (enableRamWrite)
			return ram[address & 0x1fff];
	}
	
	return 0xff;
}

void Mbc2::write(uint16 address, uint8 val) {
	if (address <= 0x1fff) {
		enableRamWrite = !(address & 0x8000);
	}
	else if (address >= 0x2000 && address <= 0x3fff) {
		romBankSelect = (val & 0x0F) % info.numRomBanks;
	}
	else if (address >= 0xa000 && address <= 0xa1ff) {
		if (enableRamWrite)
			ram[address & 0x1fff] = (val & 0xf0);
	}
}

} // namespace Gameboy
