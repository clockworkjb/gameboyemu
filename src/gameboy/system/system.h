#pragma once

#include "gameboy/cpu/cpu.h"
#include "gameboy/display/display.h"
#include "gameboy/audio/audio.h"
#include "gameboy/joypad/joypad.h"
#include "gameboy/cartridge/cartridge.h"

#include <vector>
#include <memory>

namespace Gameboy {

constexpr int NORMAL_SPEED_CYCLES_PER_FRAME = 70224;
constexpr int DOUBLE_SPEED_CYCLES_PER_FRAME = NORMAL_SPEED_CYCLES_PER_FRAME * 2;

class System {
public:
	enum struct Model {
		DMG, CGB, SGB,
	};

	System(std::unique_ptr<Cartridge> c) : cpu(this), display(this), cart(std::move(c)) {
		auto info = cart->getInfo();
		// prefer CGB over SGB, this should be use-configurable in future
		if (info.gbc)
			model = Model::CGB;
		else if (info.sgb)
			model = Model::SGB;
	}

	System(const std::vector<uint8>& buffer) : System(Cartridge::loadRom(buffer)) {}

	~System() {}

	void reset();

	int execute(int cyclesNeeded);
	int execute();

	uint8 peekMemory(uint16 address) { return read(address); }

	std::array<uint8, 0xffff> peekAllMemory() {
		std::array<uint8, 0xffff> data;
		for (size_t i = 0; i < 0xffff; ++i) {
			data[i] = read(i);
		}
		return data;
	}

	auto getSprite(size_t index) const { return display.getSprite(index); }

	inline bool getWantRedisplay() const { return display.getWantRedisplay(); }
	inline void postRedisplay() { display.postRedisplay(); }
	
	inline bool isStopped() const { return stopped; }

	inline bool isPaused() const { return cpu.paused; }
	inline void setPaused(bool setting) { cpu.paused = setting; }

	uint8 read(uint16 address) const;
	void write(uint16 address, uint8 value);

	void toggleButton(Joypad::Button button, bool status);

	auto getCartInfo() { return cart->getInfo(); }

	void toggleMapViewIndex() {
		display.toggleMapViewIndex();
	}

	void toggleCurrentViewPalette() {
		display.toggleCurrentViewPalette();
	}

	Cpu cpu;
	Display display;
	Apu apu;
	Joypad joypad;
	std::unique_ptr<Cartridge> cart;

	bool stopped = false;
	Model model = Model::DMG;

	int cyclesPerFrame = NORMAL_SPEED_CYCLES_PER_FRAME;
};

} // namespace Gameboy
