#include "system.h"

#include "debug.h"
#include "util.h"
#include "gameboy/memory.h"

#include <memory>

namespace Gameboy {

void System::reset() {
	cpu.reset();
	display = Display{ this };
	apu = Apu{};
	joypad = Joypad{};
	cart = nullptr;
}

int System::execute(int cyclesNeeded) {
	while (cyclesNeeded > 0) {
		if (cpu.paused) {
			return cyclesNeeded;
		}

		cyclesNeeded -= execute();
	}

	return cyclesNeeded;
}

int System::execute() {
	cpu.execute();
	if (display.getDmaStatus() != Display::DmaStatus::Completed)
		display.updateDMA();
	return cpu.getLastCycleCount();
}

void System::toggleButton(Joypad::Button button, bool status) {
	joypad.update(button, status);

	if ((joypad.read() & 0x0F) != 0x0F)
		cpu.requestInterrupt(Cpu::Interrupt::Joypad);
}

// thing have gotten a little complicated since we introduced thr CGB registers, maybe we should try mapping these addresses in a slightly easier to read way

uint8 System::read(uint16 address) const {
	if (address <= 0x7fff) {
		return cart->read(address);
	}
	else if (address >= 0x8000 && address <= 0x9fff) {
		return display.read(address);
	}
	else if (address >= 0xa000 && address <= 0xbfff) {
		return cart->read(address);
	}
	else if (address >= 0xc000 && address <= 0xfdff) {
		return cpu.read(address);
	}
	else if (address >= 0xfe00 && address <= 0xfe9f) {
		return display.read(address);
	}
	else if (address >= 0xff00 && address <= 0xff7f) {
		if(address == Memory::JOYP) {
			return joypad.read();
		}
		else if (address >= 0xff10 && address <= 0xff3f) {
			return apu.read(address);
		}
		else if (address >= 0xff40 && address <= 0xff4e || address == 0xff4f) {
			return display.read(address);
		}

		return cpu.read(address);
	}
	else if (address >= 0xff80) {
		return cpu.read(address);
	}

	return 0xff;
}

void System::write(uint16 address, uint8 val) {
	if (address <= 0x7fff) {
		cart->write(address, val);
	}
	else if (address >= 0x8000 && address <= 0x9fff) {
		display.write(address, val);
	}
	else if (address >= 0xa000 && address <= 0xbfff) {
		cart->write(address, val);
	}
	else if (address >= 0xc000 && address <= 0xfdff) {
		cpu.write(address, val);
	}
	else if (address >= 0xfe00 && address <= 0xfe9f) {
		display.write(address, val);
	}
	else if (address >= 0xff00 && address <= 0xff7f) {
		if (address == Memory::JOYP) {
			joypad.write(val);
		}
		else if (address >= 0xff10 && address <= 0xff3f) {
			apu.write(address, val);
		}
		else if (address >= 0xff40 && address <= 0xff4e || address == 0xff4f) {
			display.write(address, val);
		}

		cpu.write(address, val);
	}
	else if (address >= 0xff80) {
		cpu.write(address, val);
	}
}

} // namespace Gameboy
