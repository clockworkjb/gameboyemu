#include "cpu.h"

#include "util.h"
#include "hex.h"

#include "gameboy/system/system.h"

namespace Gameboy {

void Cpu::unknownOp() {
	DEBUG("Unknown opcode: " << hex(op, 4));
	DEBUG(*this);
	registers.stopped = true;
}

void Cpu::nop() {
	// do nothing
}

// 8-bit load
void Cpu::ld_n_m_8(uint8& dest, uint8 source) {
	dest = source;
}

void Cpu::ld_n_hl_8(uint8& dest) {
	dest = readFromHL();
}

void Cpu::ld_n_d_8(uint8& dest) {
	dest = readImmediate8();
}

void Cpu::ld_hl_n_8(uint8 source) {
	writeToHL(source);
}

void Cpu::ld_hl_d_8() {
	writeToHL(readImmediate8());
}

void Cpu::ld_n_mm_8(uint8& dest, uint16 source) {
	dest = readMem(source);
}

void Cpu::ld_n_d16_8(uint8& dest) {
	dest = readMem(readImmediate16());
}

void Cpu::ld_nn_m_8(uint16 dest, uint8 source) {
	writeMem(dest, source);
}

void Cpu::ld_d16_m_8(uint8 source) {
	writeMem(readImmediate16(), source);
}

void Cpu::ld_a_ff00plusc_8() {
	registers.a = readMem(0xFF00 + registers.c);
}

void Cpu::ld_ff00plusc_a_8() {
	writeMem(0xFF00 + registers.c, registers.a);
}

void Cpu::ld_a_hld_8() {
	registers.a = readFromHL();
	--registers.hl;
}

void Cpu::ld_hld_a_8() {
	writeToHL(registers.a);
	--registers.hl;
}

void Cpu::ld_a_hli_8() {
	registers.a = readFromHL();
	++registers.hl;
}

void Cpu::ld_hli_a_8() {
	writeToHL(registers.a);
	++registers.hl;
}

void Cpu::ldh_d8_a_8() {
	writeMem(0xFF00 + readImmediate8(), registers.a);
}

void Cpu::ldh_a_d8_8() {
	registers.a = readMem(0xFF00 + readImmediate8());
}

// 16-bit load
void Cpu::ld_n_d16_16(uint16& dest) {
	dest = readImmediate16();
}

void Cpu::ld_sp_hl_16() {
	addCycles(4);
	registers.sp = registers.hl;
}

void Cpu::ld_hl_spplusd8_16() {
	int d8 = (int8)readImmediate8();

	addCycles(4);
	registers.f.z = false;
	registers.f.n = false;
	registers.f.h = ((registers.sp & 0x0F) + (d8 & 0x0F)) > 0x0F;
	registers.f.c = ((registers.sp & 0xFF) + (d8 & 0xFF)) > 0xFF;

	registers.hl = registers.sp + d8;
}

void Cpu::ld_n_sp_16() {
	uint16 address = readImmediate16();
	writeMem(address, registers.sp);
	writeMem(address + 1, registers.sp >> 8);
}

void Cpu::push_n_16(uint16 n) {
	stackPush(n);
	addCycles(4);
}

void Cpu::pop_n_16(uint16& n) {
	n = stackPop();
}

void Cpu::pop_n_16(RegisterAF& n) {
	n = stackPop();
}

// 8-bit arithmetic
void Cpu::add_a_m_8(uint8 x) {
	uint16 rh = registers.a + x;
	uint16 rl = (registers.a & 0x0f) + (x & 0x0f);
	registers.a = rh;
	registers.f.z = (uint8)rh == 0;
	registers.f.n = 0;
	registers.f.h = rl > 0x0f;
	registers.f.c = rh > 0xff;
}

void Cpu::add_a_hl_8() {
	add_a_m_8(readFromHL());
}

void Cpu::add_a_d8() {
	add_a_m_8(readImmediate8());
}

void Cpu::adc_a_m_8(uint8 x) {
	uint16 rh = registers.a + x + registers.f.c;
	uint16 rl = (registers.a & 0x0f) + (x & 0x0f) + registers.f.c;
	registers.a = rh;
	registers.f.z = (uint8)rh == 0;
	registers.f.n = 0;
	registers.f.h = rl > 0x0f;
	registers.f.c = rh > 0xff;
}

void Cpu::adc_a_hl_8() {
	adc_a_m_8(readFromHL());
}

void Cpu::adc_a_d8() {
	adc_a_m_8(readImmediate8());
}

void Cpu::sub_a_m_8(uint8 x) {
	uint16 rh = registers.a - x;
	uint16 rl = (registers.a & 0x0f) - (x & 0x0f);
	registers.a = rh;
	registers.f.z = (uint8)rh == 0;
	registers.f.n = 1;
	registers.f.h = rl > 0x0f;
	registers.f.c = rh > 0xff;
}

void Cpu::sub_a_hl_8() {
	sub_a_m_8(readFromHL());
}

void Cpu::sub_a_d8() {
	sub_a_m_8(readImmediate8());
}

void Cpu::sbc_a_m_8(uint8 x) {
	uint16 rh = registers.a - x - registers.f.c;
	uint16 rl = (registers.a & 0x0f) - (x & 0x0f) - registers.f.c;
	registers.a = rh;
	registers.f.z = (uint8)rh == 0;
	registers.f.n = 1;
	registers.f.h = rl > 0x0f;
	registers.f.c = rh > 0xff;
}

void Cpu::sbc_a_hl_8() {
	sbc_a_m_8(readFromHL());
}

void Cpu::sbc_a_d8() {
	sbc_a_m_8(readImmediate8());
}

void Cpu::and_a_n_8(uint8 x) {
	registers.a &= x;
	registers.f.z = registers.a == 0;
	registers.f.n = 0;
	registers.f.h = 1;
	registers.f.c = 0;
}

void Cpu::and_a_hl_8() {
	and_a_n_8(readFromHL());
}

void Cpu::and_a_d8() {
	and_a_n_8(readImmediate8());
}

void Cpu::or_a_n_8(uint8 x) {
	registers.a |= x;
	registers.f.z = registers.a == 0;
	registers.f.n = 0;
	registers.f.h = 0;
	registers.f.c = 0;
}

void Cpu::or_a_hl_8() {
	or_a_n_8(readFromHL());
}

void Cpu::or_a_d8() {
	or_a_n_8(readImmediate8());
}

void Cpu::xor_a_n_8(uint8 x) {
	registers.a ^= x;
	registers.f.z = registers.a == 0;
	registers.f.n = 0;
	registers.f.h = 0;
	registers.f.c = 0;
}

void Cpu::xor_a_hl_8() {
	xor_a_n_8(readFromHL());
}

void Cpu::xor_a_d8() {
	xor_a_n_8(readImmediate8());
}

void Cpu::cp_a_n_8(uint8 x) {
	uint16 rh = registers.a - x;
	uint16 rl = (registers.a & 0x0f) - (x & 0x0f);
	registers.f.z = (uint8)rh == 0;
	registers.f.n = 1;
	registers.f.h = rl > 0x0f;
	registers.f.c = rh > 0xff;
}

void Cpu::cp_a_hl_8() {
	cp_a_n_8(readFromHL());
}

void Cpu::cp_a_d8() {
	cp_a_n_8(readImmediate8());
}

void Cpu::inc_n_8(uint8& n) {
	++n;
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = (n & 0x0F) == 0;
}

void Cpu::inc_hl_8() {
	uint8 result = readFromHL();
	writeToHL(++result);

	registers.f.z = result == 0;
	registers.f.n = 0;
	registers.f.h = (result & 0x0F) == 0;
}

void Cpu::dec_n_8(uint8& n) {
	--n;
	registers.f.z = n == 0;
	registers.f.n = 1;
	registers.f.h = (n & 0x0F) == 0x0F;
}

void Cpu::dec_hl_8() {
	uint8 result = readFromHL();
	writeToHL(--result);

	registers.f.z = result == 0;
	registers.f.n = 1;
	registers.f.h = (result & 0x0F) == 0x0F;
}

// 16-bit arithmetic
void Cpu::add_hl_n_16(uint16 x) {
	uint32 rb = (registers.hl + x);
	uint32 rn = (registers.hl & 0xfff) + (x & 0xfff);
	registers.hl = rb;
	registers.f.n = 0;
	registers.f.h = rn > 0x0fff;
	registers.f.c = rb > 0xffff;
	addCycles(4);
}

void Cpu::add_sp_d8_16() {
	int x = (int8)readImmediate8();
	addCycles(8);
 	registers.f.z = 0;
 	registers.f.n = 0;
 	registers.f.h = ((registers.sp & 0x0F) + (x & 0x0F)) > 0x0F;
 	registers.f.c = ((registers.sp & 0xFF) + (x & 0xFF)) > 0xFF;
 	registers.sp += x;
}

void Cpu::inc_n_16(uint16& n) {
	++n;
	addCycles(4);
}

void Cpu::dec_n_16(uint16& n) {
	--n;
	addCycles(4);
}

// misc

void Cpu::swap_n(uint8& n) {
	unsigned upper = (n & 0x0F) << 4;
	unsigned lower = (n & 0xF0) >> 4;
	n = upper | lower;

	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
	registers.f.c = 0;
}

void Cpu::swap_hl() {
	uint8 n = readFromHL();
	unsigned upper = (n & 0x0F) << 4;
	unsigned lower = (n & 0xF0) >> 4;
	n = upper | lower;
	writeToHL(n);
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
	registers.f.c = 0;
}

void Cpu::daa() {
	uint16 a = registers.a;
	if(registers.f.n == 0) {
		if(registers.f.h || (a & 0x0f) > 0x09)
			a += 0x06;
		if(registers.f.c || a > 0x9f)
			a += 0x60;
	}
	else {
		if(registers.f.h) {
			a -= 0x06;
			if(registers.f.c == 0)
				a &= 0xff;
		}
		if(registers.f.c)
			a -= 0x60;
	}
	registers.a = a;
	registers.f.z = registers.a == 0;
	registers.f.h = 0;
	registers.f.c |= a & 0x100;
}

void Cpu::cpl() {
	// registers.a = ~registers.a;
	registers.a ^= 0xFF;
	registers.f.n = 1;
	registers.f.h = 1;
}

void Cpu::ccf() {
	registers.f.n = 0;
	registers.f.h = 0;
	registers.f.c = !registers.f.c;
}

void Cpu::scf() {
	registers.f.n = 0;
	registers.f.h = 0;
	registers.f.c = 1;
}

void Cpu::halt() {
	registers.halted = true;
	//registers.ime = true;
}

void Cpu::stop() {
	if (system->model == System::Model::CGB && prepareSpeedSwitch) {
		prepareSpeedSwitch = false;
		if (speedMode == 0) {
			speedMode = 1;
			system->cyclesPerFrame = DOUBLE_SPEED_CYCLES_PER_FRAME;
		}
		else {
			speedMode = 0;
			system->cyclesPerFrame = NORMAL_SPEED_CYCLES_PER_FRAME;
		}
	}
	else {
		registers.stopped = true;
	}
}

void Cpu::di() {
	registers.ime = false;
}

void Cpu::ei() {
	registers.ei = true;
}

// rotate and shift
void Cpu::rlca() {
	registers.f.c = registers.a & 0x80;
	registers.a = (registers.a << 1) | (registers.a >> 7);
	registers.f.z = 0;
	registers.f.n = 0;
	registers.f.h = 0;
}

void Cpu::rla() {
	bool c = registers.a & 0x80;
	registers.a = (registers.a << 1) | registers.f.c;
	registers.f.z = 0;
	registers.f.n = 0;
	registers.f.h = 0;
	registers.f.c = c;
}

void Cpu::rrca() {
	registers.f.c = registers.a & 0x01;
	registers.a = (registers.a >> 1) | (registers.a << 7);
	registers.f.z = 0;
	registers.f.n = 0;
	registers.f.h = 0;
}

void Cpu::rra() {
	bool c = registers.a & 0x01;
	registers.a = (registers.a >> 1) | (registers.f.c << 7);
	registers.f.z = 0;
	registers.f.n = 0;
	registers.f.h = 0;
	registers.f.c = c;
}

void Cpu::rlc(uint8& n) {
	registers.f.c = n & 0x80;
	n = (n << 1) | (n >> 7);
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
}

void Cpu::rlc_hl() {
	uint8 n = readFromHL();
	registers.f.c = n & 0x80;
	n = (n << 1) | (n >> 7);
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
	writeToHL(n);
}

void Cpu::rl(uint8& n) {
	bool c = n & 0x80;
	n = (n << 1) | registers.f.c;
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
	registers.f.c = c;
}

void Cpu::rl_hl() {
	uint8 n = readFromHL();
	bool c = n & 0x80;
	n = (n << 1) | registers.f.c;
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
	registers.f.c = c;
	writeToHL(n);
}

void Cpu::rrc(uint8& n) {
	registers.f.c = n & 0x01;
	n = (n >> 1) | (n << 7);
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
}

void Cpu::rrc_hl() {
	uint8 n = readFromHL();
	registers.f.c = n & 0x01;
	n = (n >> 1) | (n << 7);
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
	writeToHL(n);
}

void Cpu::rr(uint8& n) {
	bool c = n & 0x01;
	n = (n >> 1) | (registers.f.c << 7);
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
	registers.f.c = c;
}

void Cpu::rr_hl() {
	uint8 n = readFromHL();
	bool c = n & 0x01;
	n = (n >> 1) | (registers.f.c << 7);
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
	registers.f.c = c;
	writeToHL(n);
}

void Cpu::sla(uint8 &n) {
	registers.f.c = n & 0x80;
	n <<= 1;
	n &= ~(0x01);
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
}

void Cpu::sla_hl() {
	uint8 n = readFromHL();
	registers.f.c = n & 0x80;
	n <<= 1;
	n &= ~(0x01);
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
	writeToHL(n);
}

void Cpu::sra(uint8 &n) {
	registers.f.c = n & 0x01;
	n = (int8)n >> 1;
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
}

void Cpu::sra_hl() {
	uint8 n = readFromHL();
	registers.f.c = n & 0x01;
	n = (int8)n >> 1;
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
	writeToHL(n);
}

void Cpu::srl(uint8 &n) {
	registers.f.c = n & 0x01;
	n >>= 1;
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
}

void Cpu::srl_hl() {
	uint8 n = readFromHL();
	registers.f.c = n & 0x01;
	n >>= 1;
	registers.f.z = n == 0;
	registers.f.n = 0;
	registers.f.h = 0;
	writeToHL(n);
}

// bit
void Cpu::bit(uint8 n, unsigned bit) {
	registers.f.z = (n & (1 << bit)) == 0;
	registers.f.n = 0;
	registers.f.h = 1;
}

void Cpu::set(uint8& n, unsigned bit) {
	n |= (1 << bit);
}

void Cpu::res(uint8& n, unsigned bit) {
	n &= ~(1 << bit);
}

void Cpu::bit_hl(unsigned bit) {
	uint8 n = readFromHL();
	registers.f.z = (n & (1 << bit)) == 0;
	registers.f.n = 0;
	registers.f.h = 1;
}

void Cpu::set_hl(unsigned bit) {
	uint8 n = readFromHL();
	n |= (1 << bit);
	writeToHL(n);
}

void Cpu::res_hl(unsigned bit) {
	uint8 n = readFromHL();
	n &= ~(1 << bit);
	writeToHL(n);
}

// control
void Cpu::jp_n(uint16 n) {
	registers.pc = n;
	addCycles(4);
}

void Cpu::jp_cc(bool cc) {
	uint16 n = readImmediate16();
	if (cc)
		jp_n(n);
}

void Cpu::jp_hl() {
	registers.pc = registers.hl;
}

void Cpu::jr_n(int8 n) {
	registers.pc += n;
	addCycles(4);
}

void Cpu::jr_cc(bool cc) {
	int8 n = static_cast<int8>(readImmediate8());
	if (cc)
		jr_n(n);
}

void Cpu::call_n(uint16 n) {
	addCycles(4);
	stackPush(registers.pc);
	registers.pc = n;
}

void Cpu::call_cc(bool cc) {
	uint16 n = readImmediate16();
	if (cc)
		call_n(n);
}

void Cpu::rst(unsigned n) {
	stackPush(registers.pc);
	registers.pc = n;
	addCycles(4);
}

void Cpu::ret() {
	registers.pc = stackPop();
	addCycles(4);
}

void Cpu::ret_cc(bool cc) {
	if (cc)
		ret();
	addCycles(4);
}

void Cpu::reti() {
	ret();
	registers.ime = 1;
}

} // namespace Gameboy
