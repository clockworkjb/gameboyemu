void unknownOp();

void nop();

// 8-bit load
void ld_n_m_8(uint8& dest, uint8 source);
void ld_n_d_8(uint8& dest);
void ld_n_hl_8(uint8& dest);
void ld_hl_n_8(uint8 source);
void ld_hl_d_8();
void ld_n_mm_8(uint8& dest, uint16 source);
void ld_n_d16_8(uint8& dest);
void ld_nn_m_8(uint16 dest, uint8 source);
void ld_d16_m_8(uint8 source);
void ld_a_ff00plusc_8();
void ld_ff00plusc_a_8();
void ld_a_hld_8();
void ld_hld_a_8();
void ld_a_hli_8();
void ld_hli_a_8();
void ldh_d8_a_8();
void ldh_a_d8_8();

// 16-bit load
void ld_n_d16_16(uint16& dest);
void ld_sp_hl_16();
void ld_hl_spplusd8_16();
void ld_n_sp_16();
void push_n_16(uint16 n);
void pop_n_16(uint16& n);
void pop_n_16(RegisterAF& n);

// 8-bit arithmetic
void add_a_m_8(uint8 x);
void add_a_hl_8();
void add_a_d8();

void adc_a_m_8(uint8 x);
void adc_a_hl_8();
void adc_a_d8();

void sub_a_m_8(uint8 x);
void sub_a_hl_8();
void sub_a_d8();

void sbc_a_m_8(uint8 x);
void sbc_a_hl_8();
void sbc_a_d8();

void and_a_n_8(uint8 x);
void and_a_hl_8();
void and_a_d8();

void or_a_n_8(uint8 x);
void or_a_hl_8();
void or_a_d8();

void xor_a_n_8(uint8 x);
void xor_a_hl_8();
void xor_a_d8();

void cp_a_n_8(uint8 x);
void cp_a_hl_8();
void cp_a_d8();

void inc_n_8(uint8& n);
void inc_hl_8();

void dec_n_8(uint8& n);
void dec_hl_8();

// 16-bit arithmetic
void add_hl_n_16(uint16 x);
void add_sp_d8_16();

void inc_n_16(uint16& n);
void dec_n_16(uint16& n);

// misc
void swap_n(uint8& n);
void swap_hl();

void daa();

void cpl();

void ccf();
void scf();

void halt();
void stop();

void di();
void ei();

// rotate and shift
void rlca();
void rla();

void rrca();
void rra();

void rlc(uint8& n);
void rlc_hl();
void rl(uint8& n);
void rl_hl();

void rrc(uint8& n);
void rrc_hl();
void rr(uint8& n);
void rr_hl();

void sla(uint8 &n);
void sla_hl();
void sra(uint8 &n);
void sra_hl();
void srl(uint8 &n);
void srl_hl();

// bit
void bit(uint8 n, unsigned bit);
void set(uint8& n, unsigned bit);
void res(uint8& n, unsigned bit);

void bit_hl(unsigned bit);
void set_hl(unsigned bit);
void res_hl(unsigned bit);

// control
void jp_n(uint16 n);
void jp_cc(bool cc);
void jp_hl();
void jr_n(int8 n);
void jr_cc(bool cc);

void call_n(uint16 n);
void call_cc(bool cc);

void rst(unsigned n);

void ret();
void ret_cc(bool cc);
void reti();
