#include "cpu.h"

#include "gameboy/memory.h"
#include "gameboy/system/system.h"

namespace Gameboy {

uint16 Cpu::getWramAddress(uint16 address) const {
	return (address & 0xfff) + 0x1000 * (wramBankSelect == 0 ? 1 : wramBankSelect);
}

uint8 Cpu::read(uint16 address) const {
	if (address >= 0xc000 && address <= 0xcfff) { // WRAM bank 0
		return wRam[address & 0xfff];
	}
	else if (address >= 0xd000 && address <= 0xdfff) { // WRAM bank 1, 1 - 7 in CGB mode
		return wRam[getWramAddress(address)];
	}
	else if (address >= 0xe000 && address <= 0xefff) { // WRAM echo bank 0
		return wRam[address & 0xfff];
	}
	else if (address >= 0xf000 && address <= 0xfdff) { // WRAM echo bank 1, 1 - 7 in CGB mode
		return wRam[getWramAddress(address)];
	}
	else if (address >= 0xff00 && address <= 0xff7f) {
		if (address == Memory::SB) {
			return 0xff;
		}
		else if (address == Memory::SC) {
			return (serialTransferStartFlag << 7) | 0x7e | serialShiftClock;
		}
		else if (address == Memory::DIV) {
			return div;
		}
		else if (address == Memory::TIMA) {
			return tima;
		}
		else if (address == Memory::TMA) {
			return tma;
		}
		else if (address == Memory::TAC) {
			return 0xf8 | (timerEnabled << 2) | clockSelect;
		}
		else if (address == Memory::IF) {
			return 0xe0 | (interruptFlag.joypad << 4) | (interruptFlag.serial << 3) | (interruptFlag.timer << 2) | (interruptFlag.STAT << 1) | interruptFlag.vBlank;
		}
		else if (address == Memory::KEY1) {
			return (speedMode << 7) | 0x7e | prepareSpeedSwitch;
		}
		else if (address == Memory::SVBK) {
			return wramBankSelect;
		}

		return io[address - Memory::IO_OFFSET];
	}
	else if (address >= 0xff80 && address <= 0xfffe) {
		return hRam[address - Memory::HIGH_RAM_OFFSET];
	}
	else if (address == Memory::IE) {
		return 0xe0 | (interruptEnable.joypad << 4) | (interruptEnable.serial << 3) | (interruptEnable.timer << 2) | (interruptEnable.STAT << 1) | interruptEnable.vBlank;
	}
	
	return 0xff;
}

void Cpu::write(uint16 address, uint8 val) {
	if (address >= 0xc000 && address <= 0xcfff) { // WRAM bank 0
		wRam[address & 0xfff] = val;
	}
	else if (address >= 0xd000 && address <= 0xdfff) { // WRAM bank 1, 1 - 7 in CGB mode
		wRam[getWramAddress(address)] = val;
	}
	else if (address >= 0xe000 && address <= 0xefff) { // WRAM echo bank 0
		wRam[address & 0xfff] = val;
	}
	else if (address >= 0xf000 && address <= 0xfdff) { // WRAM echo bank 1, 1 - 7 in CGB mode
		wRam[getWramAddress(address)] = val;
	}
	else if (address >= 0xff00 && address <= 0xff7f) {
		if (address == Memory::SB) {
			serialTransferData = val;
		}
		else if (address == Memory::SC) {
			serialTransferStartFlag = dx_util::testBit(val, 7);
			serialShiftClock = dx_util::testBit(val, 0);
		}
		else if (address == Memory::DIV) {
			div = 0;
			divTimer = 0;
		}
		else if (address == Memory::TIMA) {
			tima = val;
		}
		else if (address == Memory::TMA) {
			tma = val;
		}
		else if (address == Memory::TAC) {
			timerEnabled = dx_util::testBit(val, 2);
			clockSelect = val & 0x3;
		}
		else if (address == Memory::IF) {
			interruptFlag.joypad = dx_util::testBit(val, 4);
			interruptFlag.serial = dx_util::testBit(val, 3);
			interruptFlag.timer = dx_util::testBit(val, 2);
			interruptFlag.STAT = dx_util::testBit(val, 1);
			interruptFlag.vBlank = dx_util::testBit(val, 0);
		}
		else if (address == Memory::KEY1) {
			prepareSpeedSwitch = dx_util::testBit(val, 0);
		}
		else if (address == Memory::SVBK) {
			wramBankSelect = val & 0x7;
		}
		else {
			io[address - Memory::IO_OFFSET] = val;
		}
	}
	else if (address >= 0xff80 && address <= 0xfffe) {
		hRam[address - Memory::HIGH_RAM_OFFSET] = val;
	}
	else if (address == Memory::IE) {
		interruptEnable.joypad = dx_util::testBit(val, 4);
		interruptEnable.serial = dx_util::testBit(val, 3);
		interruptEnable.timer = dx_util::testBit(val, 2);
		interruptEnable.STAT = dx_util::testBit(val, 1);
		interruptEnable.vBlank = dx_util::testBit(val, 0);
	}
}

}

