#pragma once

#include "debug.h"
#include "util.h"
#include "types.h"
#include "registers.h"

#include <array>

namespace Gameboy {

class System;

class Cpu {
public:
	enum struct Interrupt : unsigned {
		VBlank = 0,
		STAT,
		Timer,
		Serial,
		Joypad,
	};

	bool paused = false;

	Cpu(System* system) : system(system) {
		reset();
	}

	~Cpu() {}

	void execute();
	void reset();

	// interrupt.cpp
	void requestInterrupt(Interrupt interrupt);

	uint16 getWramAddress(uint16 address) const;
	uint8 read(uint16 address) const;
	void write(uint16 address, uint8 val);

	inline int getInstructionCount() const { return instructionCount; }
	inline int getCycleCount() const { return cycleCount; }
	inline int getLastCycleCount() const { return lastCycleCount; }

	inline bool isStopped() const { return registers.stopped; }

	friend std::ostream& operator << (std::ostream& o, Cpu& cpu);

//private:
	System* system;

	uint8 op;
	Registers registers;

	int instructionCount;
	int cycleCount;
	int lastCycleCount;

	bool halted;
	bool terminated;

	std::array<uint8, 0x8000> wRam; // 0xC000 - 0xDFFF, echo: 0xE000 - 0xFDFF
	std::array<uint8, 0x80> io; // 0xFF00 - 0xFF7F
	std::array<uint8, 0x7F> hRam; // 0xFF80 - 0xFFFE
	
	struct {
		bool vBlank;
		bool STAT;
		bool timer;
		bool serial;
		bool joypad;
	} interruptFlag, interruptEnable; // 0xff0f, 0xffff

	void executeCB();

	uint8 readImmediate8();
	uint16 readImmediate16();

	uint8 readMem(uint16 address);
	void writeMem(uint16 address, uint8 val);	

	uint8 readFromHL();
	void writeToHL(uint8 val);

	void stackPush(uint16 n);
	uint16 stackPop();

	uint8 serialTransferData; // 0xff01 SB

	// 0xff02 SC
	bool serialTransferStartFlag;
	bool serialShiftClock;

	// timing.cpp
	int divTimer = 0;
	uint8 div; // 0xff04
	int timaTimer = 0;
	uint8 tima; // 0xff05
	uint8 tma; // 0xff06
	int rtcTimer = 0;

	// 0xff07 TAC
	bool timerEnabled;
	unsigned clockSelect;
	void addCycles(int cycles);

	// interrupt.cpp
	enum struct InterruptVector : uint16 {
		VBlank = 0x40,
		STAT = 0x48,
		Timer = 0x50,
		Serial = 0x58,
		Joypad = 0x60,
	};

	void interrupt(InterruptVector v);
	void handleInterrupts();

	// 0xff4d KEY1
	bool speedMode = false;
	bool prepareSpeedSwitch = false;

	// 0xff70 SVBK
	uint8 wramBankSelect = 1;


#include "instructions.h"
};

} // namespace Gameboy
