#pragma once

#include "types.h" // uint 8-bit and 16-bit types
#include "util.h" // word
#include <iostream> // std::ostream

namespace Gameboy {

struct Register {
	virtual operator unsigned() const = 0;
	virtual unsigned operator =(unsigned b) = 0;

	Register& operator =(Register& b) {
		*this = (unsigned)b;
		return *this;
	}

	unsigned operator +=(unsigned b) {
		*this = *this + b;
		return *this;
	}

	unsigned operator -=(unsigned b) {
		*this = *this - b;
		return *this;
	}

	unsigned operator *=(unsigned b) {
		*this = *this * b;
		return *this;
	}

	unsigned operator /=(unsigned b) {
		*this = *this / b;
		return *this;
	}

	unsigned operator <<=(unsigned b) {
		*this = *this << b;
		return *this;
	}

	unsigned operator >>=(unsigned b) {
		*this = *this >> b;
		return *this;
	}

	unsigned operator |=(unsigned b) {
		*this = *this | b;
		return *this;
	}

	unsigned operator &=(unsigned b) {
		*this = *this & b;
		return *this;
	}

	unsigned operator ^=(unsigned b) {
		*this = *this ^ b;
		return *this;
	}

	unsigned operator ++() {
		*this = *this + 1;
		return *this;
	}

	unsigned operator --() {
		*this = *this - 1;
		return *this;
	}

	unsigned operator ++(int) {
		unsigned temp = *this;
		operator++();
		return temp;
	}

	unsigned operator --(int) {
		unsigned temp = *this;
		operator--();
		return temp;
	}
};

struct RegisterFlags : Register {
	bool z, n, h, c;

	RegisterFlags(bool z = 0, bool n = 0, bool h = 0, bool c = 0) : z(z), n(n), h(h), c(c) {}

	operator unsigned() const override { return (z << 7) | (n << 6) | (h << 5) | (c << 4); }
	unsigned operator =(unsigned b) override {
		z = b & 0x80;
		n = b & 0x40;
		h = b & 0x20;
		c = b & 0x10;
		return *this; 
	}
};

struct RegisterAF : Register {
	uint8& h;
	RegisterFlags& l;

	RegisterAF(uint8& h, RegisterFlags& l) : h(h), l(l) {}

	operator unsigned() const override { return dx_util::word(l, h); }
	unsigned operator =(unsigned b) override { h = b >> 8; l = b; return *this; }
};

struct Registers {
	union {
		struct { uint8 c, b; };
		uint16 bc;
	};

	union {
		struct { uint8 e, d; };
		uint16 de;
	};

	union {
		struct { uint8 l, h; };
		uint16 hl;
	};
	
	uint8 a;
	RegisterFlags f;
	RegisterAF af;
	uint16 pc, sp;

	bool ei;
	bool ime;
	bool halted;
	bool stopped;

	Registers() : af(a, f) {}
};

std::ostream& operator << (std::ostream& o, const RegisterFlags& r);
std::ostream& operator << (std::ostream& o, const RegisterAF& r);
std::ostream& operator << (std::ostream& o, const Registers& r);

} // namespace Gameboy
