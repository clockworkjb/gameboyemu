#pragma once

#include "types.h"

#include <iostream>

namespace Gameboy {

class System;

class Disassembler {
public:
	Disassembler() {}
	~Disassembler() {}

	const Disassembler& operator()(uint16 pc, System* mem) {
		this->pc = pc;
		this->mem = mem;
		return *this;
	}

	friend std::ostream& operator << (std::ostream& o, const Disassembler& d);
private:
	System* mem;
	uint16 pc;
};

extern Disassembler disassembler;

} // namespace Gameboy
