#include "registers.h"

#include "hex.h"

namespace Gameboy {

std::ostream& operator << (std::ostream& o, const RegisterFlags& r) {
	o << "z: " << +(r.z) << " | ";
	o << "n: " << +(r.n) << " | ";
	o << "h: " << +(r.h) << " | ";\
	o << "c: " << +(r.c) << "\n";
	
	return o;
}

std::ostream& operator << (std::ostream& o, const RegisterAF& r) {
	o << +((unsigned)r);
	return o;
}

std::ostream& operator << (std::ostream& o, const Registers& r) {
	o << "PC: " << hex(r.pc, 4) << " | ";
	o << "SP: " << hex(r.sp, 4) << "\n";

	o << "AF: " << hex(r.af, 4) << " | ";
	o << "BC: " << hex(r.bc, 4) << " | ";
	o << "DE: " << hex(r.de, 4) << " | ";
	o << "HL: " << hex(r.hl, 4) << "\n";

	o << "Flags: " << r.f << "\n";

	o << "IME: " << r.ime << " | ";
	o << "EI: " << r.ei << "\n";
	
	return o;
}

} // namespace Gameboy
