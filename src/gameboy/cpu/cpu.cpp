#include "cpu.h"

#include "util.h"
#include "hex.h"
#include "disassemble.h"
#include "gameboy/memory.h"
#include "gameboy/system/system.h"

namespace Gameboy {

void Cpu::execute() {
	int cyclesCompleted = cycleCount;

	handleInterrupts();

	if (registers.halted || registers.stopped) {
		addCycles(4);
		lastCycleCount = 4;
		return;
	}

	op = readImmediate8();

	switch (op) {
	case 0x00: 
		nop();
		break;
	case 0x01: 
		ld_n_d16_16(registers.bc);
		break;
	case 0x02: 
		ld_nn_m_8(registers.bc, registers.a);
		break;
	case 0x03: 
		inc_n_16(registers.bc);
		break;
	case 0x04: 
		inc_n_8(registers.b);
		break;
	case 0x05: 
		dec_n_8(registers.b);
		break;
	case 0x06: 
		ld_n_d_8(registers.b);
		break;
	case 0x07: 
		rlca();
		break;
	case 0x08: 
		ld_n_sp_16();
		break;
	case 0x09: 
		add_hl_n_16(registers.bc);
		break;
	case 0x0a: 
		ld_n_mm_8(registers.a, registers.bc);
		break;
	case 0x0b: 
		dec_n_16(registers.bc);
		break;
	case 0x0c: 
		inc_n_8(registers.c);
		break;
	case 0x0d: 
		dec_n_8(registers.c);
		break;
	case 0x0e: 
		ld_n_d_8(registers.c);
		break;
	case 0x0f: 
		rrca();
		break;
	case 0x10: 
		stop();
		break;
	case 0x11: 
		ld_n_d16_16(registers.de);
		break;
	case 0x12: 
		ld_nn_m_8(registers.de, registers.a);
		break;
	case 0x13: 
		inc_n_16(registers.de);
		break;
	case 0x14: 
		inc_n_8(registers.d);
		break;
	case 0x15: 
		dec_n_8(registers.d);
		break;
	case 0x16: 
		ld_n_d_8(registers.d);
		break;
	case 0x17: 
		rla();
		break;
	case 0x18: 
		jr_n(static_cast<int8>(readImmediate8()));
		break;
	case 0x19: 
		add_hl_n_16(registers.de);
		break;
	case 0x1a: 
		ld_n_mm_8(registers.a, registers.de);
		break;
	case 0x1b: 
		dec_n_16(registers.de);
		break;
	case 0x1c: 
		inc_n_8(registers.e);
		break;
	case 0x1d: 
		dec_n_8(registers.e);
		break;
	case 0x1e: 
		ld_n_d_8(registers.e);
		break;
	case 0x1f: 
		rra();
		break;
	case 0x20: 
		jr_cc(registers.f.z == 0);
		break;
	case 0x21: 
		ld_n_d16_16(registers.hl);
		break;
	case 0x22: 
		ld_hli_a_8();
		break;
	case 0x23: 
		inc_n_16(registers.hl);
		break;
	case 0x24: 
		inc_n_8(registers.h);
		break;
	case 0x25: 
		dec_n_8(registers.h);
		break;
	case 0x26:
		ld_n_d_8(registers.h);
		break;
	case 0x27: 
		daa();
		break;
	case 0x28: 
		jr_cc(registers.f.z == 1);
		break;
	case 0x29: 
		add_hl_n_16(registers.hl);
		break;
	case 0x2a: 
		ld_a_hli_8();
		break;
	case 0x2b: 
		dec_n_16(registers.hl);
		break;
	case 0x2c: 
		inc_n_8(registers.l);
		break;
	case 0x2d: 
		dec_n_8(registers.l);
		break;
	case 0x2e: 
		ld_n_d_8(registers.l);
		break;
	case 0x2f: 
		cpl();
		break;
	case 0x30: 
		jr_cc(registers.f.c == 0);
		break;
	case 0x31: 
		ld_n_d16_16(registers.sp);
		break;
	case 0x32: 
		ld_hld_a_8();
		break;
	case 0x33: 
		inc_n_16(registers.sp);
		break;
	case 0x34: 
		inc_hl_8();
		break;
	case 0x35: 
		dec_hl_8();
		break;
	case 0x36: 
		ld_hl_d_8();
		break;
	case 0x37: 
		scf();
		break;
	case 0x38: 
		jr_cc(registers.f.c == 1);
		break;
	case 0x39: 
		add_hl_n_16(registers.sp);
		break;
	case 0x3a: 
		ld_a_hld_8();
		break;
	case 0x3b: 
		dec_n_16(registers.sp);
		break;
	case 0x3c: 
		inc_n_8(registers.a);
		break;
	case 0x3d: 
		dec_n_8(registers.a);
		break;
	case 0x3e: 
		ld_n_d_8(registers.a);
		break;
	case 0x3f: 
		ccf();
		break;
	case 0x40: 
		ld_n_m_8(registers.b, registers.b);
		break;
	case 0x41: 
		ld_n_m_8(registers.b, registers.c);
		break;
	case 0x42: 
		ld_n_m_8(registers.b, registers.d);
		break;
	case 0x43: 
		ld_n_m_8(registers.b, registers.e);
		break;
	case 0x44: 
		ld_n_m_8(registers.b, registers.h);
		break;
	case 0x45: 
		ld_n_m_8(registers.b, registers.l);
		break;
	case 0x46: 
		ld_n_hl_8(registers.b);
		break;
	case 0x47: 
		ld_n_m_8(registers.b, registers.a);
		break;
	case 0x48: 
		ld_n_m_8(registers.c, registers.b);
		break;
	case 0x49: 
		ld_n_m_8(registers.c, registers.c);
		break;
	case 0x4a: 
		ld_n_m_8(registers.c, registers.d);
		break;
	case 0x4b: 
		ld_n_m_8(registers.c, registers.e);
		break;
	case 0x4c: 
		ld_n_m_8(registers.c, registers.h);
		break;
	case 0x4d: 
		ld_n_m_8(registers.c, registers.l);
		break;
	case 0x4e: 
		ld_n_hl_8(registers.c);
		break;
	case 0x4f: 
		ld_n_m_8(registers.c, registers.a);
		break;
	case 0x50: 
		ld_n_m_8(registers.d, registers.b);
		break;
	case 0x51: 
		ld_n_m_8(registers.d, registers.c);
		break;
	case 0x52: 
		ld_n_m_8(registers.d, registers.d);
		break;
	case 0x53: 
		ld_n_m_8(registers.d, registers.e);
		break;
	case 0x54: 
		ld_n_m_8(registers.d, registers.h);
		break;
	case 0x55: 
		ld_n_m_8(registers.d, registers.l);
		break;
	case 0x56: 
		ld_n_hl_8(registers.d);
		break;
	case 0x57: 
		ld_n_m_8(registers.d, registers.a);
		break;
	case 0x58: 
		ld_n_m_8(registers.e, registers.b);
		break;
	case 0x59: 
		ld_n_m_8(registers.e, registers.c);
		break;
	case 0x5a: 
		ld_n_m_8(registers.e, registers.d);
		break;
	case 0x5b: 
		ld_n_m_8(registers.e, registers.e);
		break;
	case 0x5c: 
		ld_n_m_8(registers.e, registers.h);
		break;
	case 0x5d: 
		ld_n_m_8(registers.e, registers.l);
		break;
	case 0x5e: 
		ld_n_hl_8(registers.e);
		break;
	case 0x5f: 
		ld_n_m_8(registers.e, registers.a);
		break;
	case 0x60: 
		ld_n_m_8(registers.h, registers.b);
		break;
	case 0x61: 
		ld_n_m_8(registers.h, registers.c);
		break;
	case 0x62: 
		ld_n_m_8(registers.h, registers.d);
		break;
	case 0x63: 
		ld_n_m_8(registers.h, registers.e);
		break;
	case 0x64: 
		ld_n_m_8(registers.h, registers.h);
		break;
	case 0x65: 
		ld_n_m_8(registers.h, registers.l);
		break;
	case 0x66: 
		ld_n_hl_8(registers.h);
		break;
	case 0x67: 
		ld_n_m_8(registers.h, registers.a);
		break;
	case 0x68: 
		ld_n_m_8(registers.l, registers.b);
		break;
	case 0x69: 
		ld_n_m_8(registers.l, registers.c);
		break;
	case 0x6a: 
		ld_n_m_8(registers.l, registers.d);
		break;
	case 0x6b: 
		ld_n_m_8(registers.l, registers.e);
		break;
	case 0x6c: 
		ld_n_m_8(registers.l, registers.h);
		break;
	case 0x6d: 
		ld_n_m_8(registers.l, registers.l);
		break;
	case 0x6e: 
		ld_n_hl_8(registers.l);
		break;
	case 0x6f: 
		ld_n_m_8(registers.l, registers.a);
		break;
	case 0x70: 
		ld_hl_n_8(registers.b);
		break;
	case 0x71: 
		ld_hl_n_8(registers.c);
		break;
	case 0x72: 
		ld_hl_n_8(registers.d);
		break;
	case 0x73: 
		ld_hl_n_8(registers.e);
		break;
	case 0x74: 
		ld_hl_n_8(registers.h);
		break;
	case 0x75: 
		ld_hl_n_8(registers.l);
		break;
	case 0x76: 
		halt();
		break;
	case 0x77: 
		ld_nn_m_8(registers.hl, registers.a);
		break;
	case 0x78: 
		ld_n_m_8(registers.a, registers.b);
		break;
	case 0x79: 
		ld_n_m_8(registers.a, registers.c);
		break;
	case 0x7a: 
		ld_n_m_8(registers.a, registers.d);
		break;
	case 0x7b: 
		ld_n_m_8(registers.a, registers.e);
		break;
	case 0x7c: 
		ld_n_m_8(registers.a, registers.h);
		break;
	case 0x7d: 
		ld_n_m_8(registers.a, registers.l);
		break;
	case 0x7e: 
		ld_n_hl_8(registers.a);
		break;
	case 0x7f: 
		ld_n_m_8(registers.a, registers.a);
		break;
	case 0x80: 
		add_a_m_8(registers.b);
		break;
	case 0x81: 
		add_a_m_8(registers.c);
		break;
	case 0x82: 
		add_a_m_8(registers.d);
		break;
	case 0x83: 
		add_a_m_8(registers.e);
		break;
	case 0x84: 
		add_a_m_8(registers.h);
		break;
	case 0x85: 
		add_a_m_8(registers.l);
		break;
	case 0x86: 
		add_a_hl_8();
		break;
	case 0x87: 
		add_a_m_8(registers.a);
		break;
	case 0x88: 
		adc_a_m_8(registers.b);
		break;
	case 0x89: 
		adc_a_m_8(registers.c);
		break;
	case 0x8a: 
		adc_a_m_8(registers.d);
		break;
	case 0x8b: 
		adc_a_m_8(registers.e);
		break;
	case 0x8c: 
		adc_a_m_8(registers.h);
		break;
	case 0x8d: 
		adc_a_m_8(registers.l);
		break;
	case 0x8e: 
		adc_a_hl_8();
		break;
	case 0x8f: 
		adc_a_m_8(registers.a);
		break;
	case 0x90: 
		sub_a_m_8(registers.b);
		break;
	case 0x91: 
		sub_a_m_8(registers.c);
		break;
	case 0x92: 
		sub_a_m_8(registers.d);
		break;
	case 0x93: 
		sub_a_m_8(registers.e);
		break;
	case 0x94: 
		sub_a_m_8(registers.h);
		break;
	case 0x95: 
		sub_a_m_8(registers.l);
		break;
	case 0x96: 
		sub_a_hl_8();
		break;
	case 0x97: 
		sub_a_m_8(registers.a);
		break;
	case 0x98: 
		sbc_a_m_8(registers.b);
		break;
	case 0x99: 
		sbc_a_m_8(registers.c);
		break;
	case 0x9a: 
		sbc_a_m_8(registers.d);
		break;
	case 0x9b: 
		sbc_a_m_8(registers.e);
		break;
	case 0x9c: 
		sbc_a_m_8(registers.h);
		break;
	case 0x9d: 
		sbc_a_m_8(registers.l);
		break;
	case 0x9e: 
		sbc_a_hl_8();
		break;
	case 0x9f: 
		sbc_a_m_8(registers.a);
		break;
	case 0xa0: 
		and_a_n_8(registers.b);
		break;
	case 0xa1: 
		and_a_n_8(registers.c);
		break;
	case 0xa2: 
		and_a_n_8(registers.d);
		break;
	case 0xa3: 
		and_a_n_8(registers.e);
		break;
	case 0xa4: 
		and_a_n_8(registers.h);
		break;
	case 0xa5: 
		and_a_n_8(registers.l);
		break;
	case 0xa6: 
		and_a_hl_8();
		break;
	case 0xa7: 
		and_a_n_8(registers.a);
		break;
	case 0xa8: 
		xor_a_n_8(registers.b);
		break;
	case 0xa9: 
		xor_a_n_8(registers.c);
		break;
	case 0xaa: 
		xor_a_n_8(registers.d);
		break;
	case 0xab: 
		xor_a_n_8(registers.e);
		break;
	case 0xac: 
		xor_a_n_8(registers.h);
		break;
	case 0xad: 
		xor_a_n_8(registers.l);
		break;
	case 0xae: 
		xor_a_hl_8();
		break;
	case 0xaf: 
		xor_a_n_8(registers.a);
		break;
	case 0xb0: 
		or_a_n_8(registers.b);
		break;
	case 0xb1: 
		or_a_n_8(registers.c);
		break;
	case 0xb2: 
		or_a_n_8(registers.d);
		break;
	case 0xb3: 
		or_a_n_8(registers.e);
		break;
	case 0xb4: 
		or_a_n_8(registers.h);
		break;
	case 0xb5: 
		or_a_n_8(registers.l);
		break;
	case 0xb6: 
		or_a_hl_8();
		break;
	case 0xb7: 
		or_a_n_8(registers.a);
		break;
	case 0xb8: 
		cp_a_n_8(registers.b);
		break;
	case 0xb9: 
		cp_a_n_8(registers.c);
		break;
	case 0xba: 
		cp_a_n_8(registers.d);
		break;
	case 0xbb: 
		cp_a_n_8(registers.e);
		break;
	case 0xbc: 
		cp_a_n_8(registers.h);
		break;
	case 0xbd: 
		cp_a_n_8(registers.l);
		break;
	case 0xbe: 
		cp_a_hl_8();
		break;
	case 0xbf: 
		cp_a_n_8(registers.a);
		break;
	case 0xc0: 
		ret_cc(registers.f.z == 0);
		break;
	case 0xc1: 
		pop_n_16(registers.bc);
		break;
	case 0xc2: 
		jp_cc(registers.f.z == 0);
		break;
	case 0xc3: 
		jp_n(readImmediate16());
		break;
	case 0xc4: 
		call_cc(registers.f.z == 0);
		break;
	case 0xc5: 
		push_n_16(registers.bc);
		break;
	case 0xc6: 
		add_a_d8();
		break;
	case 0xc7: 
		rst(0x00);
		break;
	case 0xc8: 
		ret_cc(registers.f.z == 1);
		break;
	case 0xc9: 
		ret();
		break;
	case 0xca: 
		jp_cc(registers.f.z == 1);
		break;
	case 0xcb: 
		executeCB();
		break;
	case 0xcc: 
		call_cc(registers.f.z == 1);
		break;
	case 0xcd: 
		call_n(readImmediate16());
		break;
	case 0xce: 
		adc_a_d8();
		break;
	case 0xcf: 
		rst(0x08);
		break;
	case 0xd0: 
		ret_cc(registers.f.c == 0);
		break;
	case 0xd1: 
		pop_n_16(registers.de);
		break;
	case 0xd2: 
		jp_cc(registers.f.c == 0);
		break;
	case 0xd3: 
		unknownOp();
		break;
	case 0xd4: 
		call_cc(registers.f.c == 0);
		break;
	case 0xd5: 
		push_n_16(registers.de);
		break;
	case 0xd6: 
		sub_a_d8();
		break;
	case 0xd7: 
		rst(0x10);
		break;
	case 0xd8: 
		ret_cc(registers.f.c == 1);
		break;
	case 0xd9: 
		reti();
		break;
	case 0xda: 
		jp_cc(registers.f.c == 1);
		break;
	case 0xdb: 
		unknownOp();
		break;
	case 0xdc: 
		call_cc(registers.f.c == 1);
		break;
	case 0xdd: 
		unknownOp();
		break;
	case 0xde: 
		sbc_a_d8();
		break;
	case 0xdf: 
		rst(0x18);
		break;
	case 0xe0: 
		ldh_d8_a_8();
		break;
	case 0xe1: 
		pop_n_16(registers.hl);
		break;
	case 0xe2: 
		ld_ff00plusc_a_8();
		break;
	case 0xe3: 
		unknownOp();
		break;
	case 0xe4: 
		unknownOp();
		break;
	case 0xe5: 
		push_n_16(registers.hl);
		break;
	case 0xe6: 
		and_a_d8();
		break;
	case 0xe7: 
		rst(0x20);
		break;
	case 0xe8: 
		add_sp_d8_16();
		break;
	case 0xe9: 
		jp_hl();
		break;
	case 0xea: 
		ld_d16_m_8(registers.a);
		break;
	case 0xeb: 
		unknownOp();
		break;
	case 0xec: 
		unknownOp();
		break;
	case 0xed: 
		unknownOp();
		break;
	case 0xee: 
		xor_a_d8();
		break;
	case 0xef: 
		rst(0x28);
		break;
	case 0xf0: 
		ldh_a_d8_8();
		break;
	case 0xf1: 
		pop_n_16(registers.af);
		break;
	case 0xf2: 
		ld_a_ff00plusc_8();
		break;
	case 0xf3: 
		di();
		break;
	case 0xf4: 
		unknownOp();
		break;
	case 0xf5: 
		push_n_16(registers.af);
		break;
	case 0xf6: 
		or_a_d8();
		break;
	case 0xf7: 
		rst(0x30);
		break;
	case 0xf8: 
		ld_hl_spplusd8_16();
		break;
	case 0xf9: 
		ld_sp_hl_16();
		break;
	case 0xfa: 
		ld_n_d16_8(registers.a);
		break;
	case 0xfb: 
		ei();
		break;
	case 0xfc: 
		unknownOp();
		break;
	case 0xfd: 
		unknownOp();
		break;
	case 0xfe: 
		cp_a_d8();
		break;
	case 0xff: 
		rst(0x38);
		break;
	}

	++instructionCount;
	lastCycleCount = cycleCount - cyclesCompleted;

	if (paused)
		DEBUG(*this);
}

void Cpu::executeCB() {
 	op = readImmediate8();

 	switch (op) {
	case 0x00:
		rlc(registers.b);
		break;
	case 0x01:
		rlc(registers.c);
		break;
	case 0x02:
		rlc(registers.d);
		break;
	case 0x03:
		rlc(registers.e);
		break;
	case 0x04:
		rlc(registers.h);
		break;
	case 0x05:
		rlc(registers.l);
		break;
	case 0x06:
		rlc_hl();
		break;
	case 0x07:
		rlc(registers.a);
		break;
	case 0x08:
		rrc(registers.b);
		break;
	case 0x09:
		rrc(registers.c);
		break;
	case 0x0a:
		rrc(registers.d);
		break;
	case 0x0b:
		rrc(registers.e);
		break;
	case 0x0c:
		rrc(registers.h);
		break;
	case 0x0d:
		rrc(registers.l);
		break;
	case 0x0e:
		rrc_hl();
		break;
	case 0x0f:
		rrc(registers.a);
		break;
	case 0x10:
		rl(registers.b);
		break;
	case 0x11:
		rl(registers.c);
		break;
	case 0x12:
		rl(registers.d);
		break;
	case 0x13:
		rl(registers.e);
		break;
	case 0x14:
		rl(registers.h);
		break;
	case 0x15:
		rl(registers.l);
		break;
	case 0x16:
		rl_hl();
		break;
	case 0x17:
		rl(registers.a);
		break;
	case 0x18:
		rr(registers.b);
		break;
	case 0x19:
		rr(registers.c);
		break;
	case 0x1a:
		rr(registers.d);
		break;
	case 0x1b:
		rr(registers.e);
		break;
	case 0x1c:
		rr(registers.h);
		break;
	case 0x1d:
		rr(registers.l);
		break;
	case 0x1e:
		rr_hl();
		break;
	case 0x1f:
		rr(registers.a);
		break;
	case 0x20:
		sla(registers.b);
		break;
	case 0x21:
		sla(registers.c);
		break;
	case 0x22:
		sla(registers.d);
		break;
	case 0x23:
		sla(registers.e);
		break;
	case 0x24:
		sla(registers.h);
		break;
	case 0x25:
		sla(registers.l);
		break;
	case 0x26:
		sla_hl();
		break;
	case 0x27:
		sla(registers.a);
		break;
	case 0x28:
		sra(registers.b);
		break;
	case 0x29:
		sra(registers.c);
		break;
	case 0x2a:
		sra(registers.d);
		break;
	case 0x2b:
		sra(registers.e);
		break;
	case 0x2c:
		sra(registers.h);
		break;
	case 0x2d:
		sra(registers.l);
		break;
	case 0x2e:
		sra_hl();
		break;
	case 0x2f:
		sra(registers.a);
		break;
	case 0x30:
		swap_n(registers.b);
		break;
	case 0x31:
		swap_n(registers.c);
		break;
	case 0x32:
		swap_n(registers.d);
		break;
	case 0x33:
		swap_n(registers.e);
		break;
	case 0x34:
		swap_n(registers.h);
		break;
	case 0x35:
		swap_n(registers.l);
		break;
	case 0x36:
		swap_hl();
		break;
	case 0x37:
		swap_n(registers.a);
		break;
	case 0x38:
		srl(registers.b);
		break;
	case 0x39:
		srl(registers.c);
		break;
	case 0x3a:
		srl(registers.d);
		break;
	case 0x3b:
		srl(registers.e);
		break;
	case 0x3c:
		srl(registers.h);
		break;
	case 0x3d:
		srl(registers.l);
		break;
	case 0x3e:
		srl_hl();
		break;
	case 0x3f:
		srl(registers.a);
		break;
	case 0x40:
		bit(registers.b, 0);
		break;
	case 0x41:
		bit(registers.c, 0);
		break;
	case 0x42:
		bit(registers.d, 0);
		break;
	case 0x43:
		bit(registers.e, 0);
		break;
	case 0x44:
		bit(registers.h, 0);
		break;
	case 0x45:
		bit(registers.l, 0);
		break;
	case 0x46:
		bit_hl(0);
		break;
	case 0x47:
		bit(registers.a, 0);
		break;
	case 0x48:
		bit(registers.b, 1);
		break;
	case 0x49:
		bit(registers.c, 1);
		break;
	case 0x4a:
		bit(registers.d, 1);
		break;
	case 0x4b:
		bit(registers.e, 1);
		break;
	case 0x4c:
		bit(registers.h, 1);
		break;
	case 0x4d:
		bit(registers.l, 1);
		break;
	case 0x4e:
		bit_hl(1);
		break;
	case 0x4f:
		bit(registers.a, 1);
		break;
	case 0x50:
		bit(registers.b, 2);
		break;
	case 0x51:
		bit(registers.c, 2);
		break;
	case 0x52:
		bit(registers.d, 2);
		break;
	case 0x53:
		bit(registers.e, 2);
		break;
	case 0x54:
		bit(registers.h, 2);
		break;
	case 0x55:
		bit(registers.l, 2);
		break;
	case 0x56:
		bit_hl(2);
		break;
	case 0x57:
		bit(registers.a, 2);
		break;
	case 0x58:
		bit(registers.b, 3);
		break;
	case 0x59:
		bit(registers.c, 3);
		break;
	case 0x5a:
		bit(registers.d, 3);
		break;
	case 0x5b:
		bit(registers.e, 3);
		break;
	case 0x5c:
		bit(registers.h, 3);
		break;
	case 0x5d:
		bit(registers.l, 3);
		break;
	case 0x5e:
		bit_hl(3);
		break;
	case 0x5f:
		bit(registers.a, 3);
		break;
	case 0x60:
		bit(registers.b, 4);
		break;
	case 0x61:
		bit(registers.c, 4);
		break;
	case 0x62:
		bit(registers.d, 4);
		break;
	case 0x63:
		bit(registers.e, 4);
		break;
	case 0x64:
		bit(registers.h, 4);
		break;
	case 0x65:
		bit(registers.l, 4);
		break;
	case 0x66:
		bit_hl(4);
		break;
	case 0x67:
		bit(registers.a, 4);
		break;
	case 0x68:
		bit(registers.b, 5);
		break;
	case 0x69:
		bit(registers.c, 5);
		break;
	case 0x6a:
		bit(registers.d, 5);
		break;
	case 0x6b:
		bit(registers.e, 5);
		break;
	case 0x6c:
		bit(registers.h, 5);
		break;
	case 0x6d:
		bit(registers.l, 5);
		break;
	case 0x6e:
		bit_hl(5);
		break;
	case 0x6f:
		bit(registers.a, 5);
		break;
	case 0x70:
		bit(registers.b, 6);
		break;
	case 0x71:
		bit(registers.c, 6);
		break;
	case 0x72:
		bit(registers.d, 6);
		break;
	case 0x73:
		bit(registers.e, 6);
		break;
	case 0x74:
		bit(registers.h, 6);
		break;
	case 0x75:
		bit(registers.l, 6);
		break;
	case 0x76:
		bit_hl(6);
		break;
	case 0x77:
		bit(registers.a, 6);
		break;
	case 0x78:
		bit(registers.b, 7);
		break;
	case 0x79:
		bit(registers.c, 7);
		break;
	case 0x7a:
		bit(registers.d, 7);
		break;
	case 0x7b:
		bit(registers.e, 7);
		break;
	case 0x7c:
		bit(registers.h, 7);
		break;
	case 0x7d:
		bit(registers.l, 7);
		break;
	case 0x7e:
		bit_hl(7);
		break;
	case 0x7f:
		bit(registers.a, 7);
		break;
	case 0x80:
		res(registers.b, 0);
		break;
	case 0x81:
		res(registers.c, 0);
		break;
	case 0x82:
		res(registers.d, 0);
		break;
	case 0x83:
		res(registers.e, 0);
		break;
	case 0x84:
		res(registers.h, 0);
		break;
	case 0x85:
		res(registers.l, 0);
		break;
	case 0x86:
		res_hl(0);
		break;
	case 0x87:
		res(registers.a, 0);
		break;
	case 0x88:
		res(registers.b, 1);
		break;
	case 0x89:
		res(registers.c, 1);
		break;
	case 0x8a:
		res(registers.d, 1);
		break;
	case 0x8b:
		res(registers.e, 1);
		break;
	case 0x8c:
		res(registers.h, 1);
		break;
	case 0x8d:
		res(registers.l, 1);
		break;
	case 0x8e:
		res_hl(1);
		break;
	case 0x8f:
		res(registers.a, 1);
		break;
	case 0x90:
		res(registers.b, 2);
		break;
	case 0x91:
		res(registers.c, 2);
		break;
	case 0x92:
		res(registers.d, 2);
		break;
	case 0x93:
		res(registers.e, 2);
		break;
	case 0x94:
		res(registers.h, 2);
		break;
	case 0x95:
		res(registers.l, 2);
		break;
	case 0x96:
		res_hl(2);
		break;
	case 0x97:
		res(registers.a, 2);
		break;
	case 0x98:
		res(registers.b, 3);
		break;
	case 0x99:
		res(registers.c, 3);
		break;
	case 0x9a:
		res(registers.d, 3);
		break;
	case 0x9b:
		res(registers.e, 3);
		break;
	case 0x9c:
		res(registers.h, 3);
		break;
	case 0x9d:
		res(registers.l, 3);
		break;
	case 0x9e:
		res_hl(3);
		break;
	case 0x9f:
		res(registers.a, 3);
		break;
	case 0xa0:
		res(registers.b, 4);
		break;
	case 0xa1:
		res(registers.c, 4);
		break;
	case 0xa2:
		res(registers.d, 4);
		break;
	case 0xa3:
		res(registers.e, 4);
		break;
	case 0xa4:
		res(registers.h, 4);
		break;
	case 0xa5:
		res(registers.l, 4);
		break;
	case 0xa6:
		res_hl(4);
		break;
	case 0xa7:
		res(registers.a, 4);
		break;
	case 0xa8:
		res(registers.b, 5);
		break;
	case 0xa9:
		res(registers.c, 5);
		break;
	case 0xaa:
		res(registers.d, 5);
		break;
	case 0xab:
		res(registers.e, 5);
		break;
	case 0xac:
		res(registers.h, 5);
		break;
	case 0xad:
		res(registers.l, 5);
		break;
	case 0xae:
		res_hl(5);
		break;
	case 0xaf:
		res(registers.a, 5);
		break;
	case 0xb0:
		res(registers.b, 6);
		break;
	case 0xb1:
		res(registers.c, 6);
		break;
	case 0xb2:
		res(registers.d, 6);
		break;
	case 0xb3:
		res(registers.e, 6);
		break;
	case 0xb4:
		res(registers.h, 6);
		break;
	case 0xb5:
		res(registers.l, 6);
		break;
	case 0xb6:
		res_hl(6);
		break;
	case 0xb7:
		res(registers.a, 6);
		break;
	case 0xb8:
		res(registers.b, 7);
		break;
	case 0xb9:
		res(registers.c, 7);
		break;
	case 0xba:
		res(registers.d, 7);
		break;
	case 0xbb:
		res(registers.e, 7);
		break;
	case 0xbc:
		res(registers.h, 7);
		break;
	case 0xbd:
		res(registers.l, 7);
		break;
	case 0xbe:
		res_hl(7);
		break;
	case 0xbf:
		res(registers.a, 7);
		break;
	case 0xc0:
		set(registers.b, 0);
		break;
	case 0xc1:
		set(registers.c, 0);
		break;
	case 0xc2:
		set(registers.d, 0);
		break;
	case 0xc3:
		set(registers.e, 0);
		break;
	case 0xc4:
		set(registers.h, 0);
		break;
	case 0xc5:
		set(registers.l, 0);
		break;
	case 0xc6:
		set_hl(0);
		break;
	case 0xc7:
		set(registers.a, 0);
		break;
	case 0xc8:
		set(registers.b, 1);
		break;
	case 0xc9:
		set(registers.c, 1);
		break;
	case 0xca:
		set(registers.d, 1);
		break;
	case 0xcb:
		set(registers.e, 1);
		break;
	case 0xcc:
		set(registers.h, 1);
		break;
	case 0xcd:
		set(registers.l, 1);
		break;
	case 0xce:
		set_hl(1);
		break;
	case 0xcf:
		set(registers.a, 1);
		break;
	case 0xd0:
		set(registers.b, 2);
		break;
	case 0xd1:
		set(registers.c, 2);
		break;
	case 0xd2:
		set(registers.d, 2);
		break;
	case 0xd3:
		set(registers.e, 2);
		break;
	case 0xd4:
		set(registers.h, 2);
		break;
	case 0xd5:
		set(registers.l, 2);
		break;
	case 0xd6:
		set_hl(2);
		break;
	case 0xd7:
		set(registers.a, 2);
		break;
	case 0xd8:
		set(registers.b, 3);
		break;
	case 0xd9:
		set(registers.c, 3);
		break;
	case 0xda:
		set(registers.d, 3);
		break;
	case 0xdb:
		set(registers.e, 3);
		break;
	case 0xdc:
		set(registers.h, 3);
		break;
	case 0xdd:
		set(registers.l, 3);
		break;
	case 0xde:
		set_hl(3);
		break;
	case 0xdf:
		set(registers.a, 3);
		break;
	case 0xe0:
		set(registers.b, 4);
		break;
	case 0xe1:
		set(registers.c, 4);
		break;
	case 0xe2:
		set(registers.d, 4);
		break;
	case 0xe3:
		set(registers.e, 4);
		break;
	case 0xe4:
		set(registers.h, 4);
		break;
	case 0xe5:
		set(registers.l, 4);
		break;
	case 0xe6:
		set_hl(4);
		break;
	case 0xe7:
		set(registers.a, 4);
		break;
	case 0xe8:
		set(registers.b, 5);
		break;
	case 0xe9:
		set(registers.c, 5);
		break;
	case 0xea:
		set(registers.d, 5);
		break;
	case 0xeb:
		set(registers.e, 5);
		break;
	case 0xec:
		set(registers.h, 5);
		break;
	case 0xed:
		set(registers.l, 5);
		break;
	case 0xee:
		set_hl(5);
		break;
	case 0xef:
		set(registers.a, 5);
		break;
	case 0xf0:
		set(registers.b, 6);
		break;
	case 0xf1:
		set(registers.c, 6);
		break;
	case 0xf2:
		set(registers.d, 6);
		break;
	case 0xf3:
		set(registers.e, 6);
		break;
	case 0xf4:
		set(registers.h, 6);
		break;
	case 0xf5:
		set(registers.l, 6);
		break;
	case 0xf6:
		set_hl(6);
		break;
	case 0xf7:
		set(registers.a, 6);
		break;
	case 0xf8:
		set(registers.b, 7);
		break;
	case 0xf9:
		set(registers.c, 7);
		break;
	case 0xfa:
		set(registers.d, 7);
		break;
	case 0xfb:
		set(registers.e, 7);
		break;
	case 0xfc:
		set(registers.h, 7);
		break;
	case 0xfd:
		set(registers.l, 7);
		break;
	case 0xfe:
		set_hl(7);
		break;
	case 0xff:
		set(registers.a, 7);
		break;
  	}
}

void Cpu::reset() {
	cycleCount = 0;

	registers.af = 0x01B0;
	registers.bc = 0x0013;
	registers.de = 0x00D8;
	registers.hl = 0x014D;
	registers.pc = 0x100;
	registers.sp = 0xFFFE;

	registers.halted = false;
	registers.stopped = false;
	registers.ime = true;
	registers.ei = false;

	for (size_t i = 0xFF00; i < 0xFF80; ++i) {
		write(i, 0xFF);
	}

	write(0xFF00, 0xCF);
	write(0xFF05, 0x00); // TIMA
	write(0xFF06, 0x00); // TMA
	write(0xFF07, 0x00); // TAC
	write(0xFF10, 0x80); // NR10
	write(0xFF11, 0xBF); // NR11
	write(0xFF12, 0xF3); // NR12
	write(0xFF14, 0xBF); // NR14
	write(0xFF16, 0x3F); // NR21
	write(0xFF17, 0x00); // NR22
	write(0xFF19, 0xBF); // NR24
	write(0xFF1A, 0x7F); // NR30
	write(0xFF1B, 0xFF); // NR31
	write(0xFF1C, 0x9F); // NR32
	write(0xFF1E, 0xBF); // NR33
	write(0xFF20, 0xFF); // NR41
	write(0xFF21, 0x00); // NR42
	write(0xFF22, 0x00); // NR43
	write(0xFF23, 0xBF); // NR30
	write(0xFF24, 0x77); // NR50
	write(0xFF25, 0xF3); // NR51
	write(0xFF26, 0xF1); // NR52
	write(0xFFFF, 0x00); // IE
	write(0xFF0F, 0xE0); // IF
	write(Memory::SC, 0x7F);
}

uint8 Cpu::readImmediate8() {
	return readMem(registers.pc++);
}

uint16 Cpu::readImmediate16() {
	uint8 lo = readImmediate8();
	uint8 hi = readImmediate8();
	return dx_util::word(lo, hi);
}

uint8 Cpu::readMem(uint16 address) {
	addCycles(4);
	return system->read(address);
}

void Cpu::writeMem(uint16 address, uint8 val) {
	addCycles(4);
	system->write(address, val);
}

uint8 Cpu::readFromHL() {
	return readMem(registers.hl);
}

void Cpu::writeToHL(uint8 val) {
	writeMem(registers.hl, val);
}

void Cpu::stackPush(uint16 n) {
	writeMem(--registers.sp, n >> 8);
	writeMem(--registers.sp, n);
}

uint16 Cpu::stackPop() {
	uint8 lo = readMem(registers.sp++);
	uint8 hi = readMem(registers.sp++);
	return dx_util::word(lo, hi);
}

std::ostream& operator << (std::ostream& o, Cpu& cpu) {
	o << disassembler(cpu.registers.pc, cpu.system);
	o << cpu.registers;
	o << "cycles completed: " << +(cpu.cycleCount) << "\n";
	o << "DIV: " << hex(cpu.div) << " | ";
	o << "TIMA: " << hex(cpu.tima) << " | ";
	o << "TAC: " << hex(cpu.read(Memory::TAC)) << "\n";
	o << "timer enabled: " << cpu.timerEnabled << " | ";
	int clockFrequency = 0;
	switch (cpu.clockSelect) {
	case 0:
		clockFrequency = 4096;
		break;
	case 1:
		clockFrequency = 262144;
		break;
	case 2:
		clockFrequency = 65536;
		break;
	case 3:
		clockFrequency = 16384;
		break;
	}
	o << "timer frequency: " << clockFrequency <<  "hz" << "\n";
	o << "IE: " << hex(cpu.read(Memory::IE)) << " | ";
	o << "IF: " << hex(cpu.read(Memory::IF)) << "\n";
	o << "enabled: " << "\n";
	o << "V-Blank: " << cpu.interruptEnable.vBlank << " STAT: " << cpu.interruptEnable.STAT << " Timer: " << cpu.interruptEnable.timer << " Serial: " << cpu.interruptEnable.serial << " Joypad: " << cpu.interruptEnable.joypad << "\n";
	o << "requested: " << "\n";
	o << "V-Blank: " << cpu.interruptFlag.vBlank << " STAT: " << cpu.interruptFlag.STAT << " Timer: " << cpu.interruptFlag.timer << " Serial: " << cpu.interruptFlag.serial << " Joypad: " << cpu.interruptFlag.joypad << "\n";
	o << "cycles consumed last op: " << +(cpu.lastCycleCount) << "\n";
	return o;
}

} // namespace Gameboy
