#include "cpu.h"

#include "util.h"
#include "gameboy/system/system.h"
#include "gameboy/memory.h"

namespace Gameboy {

void Cpu::addCycles(int cycles) {
	if (registers.ei) {
		registers.ei = false;
		registers.ime = true;
	}

	// make sure we operate the audio and video at normal speed when in the CGB double speed mode
	auto ioCycles = speedMode ? cycles / 2 : cycles;

	while (cycles--) {
		++cycleCount;
		++rtcTimer;

		if (++divTimer >= 256) {
			++div;
			divTimer -= 256;
		}

		if (rtcTimer >= 4194304) {
			system->cart->incrementRTC();
			rtcTimer -= 4194304;
		}

		if (timerEnabled) {
			++timaTimer;

			int clockSpeed = 0;

			switch (clockSelect) {
			case 0: // 4096 hz
				clockSpeed = 1024;
				break;
			case 1:	// 262144 hz
				clockSpeed = 16;
				break;
			case 2: // 65536 hz
				clockSpeed = 64;
				break;
			case 3: // 16384 hz
				clockSpeed = 256;
				break;
			}

			if (timaTimer >= clockSpeed) {
				if (++tima == 0) {
					tima = tma;
					requestInterrupt(Interrupt::Timer);
				}
				
				timaTimer -= clockSpeed;
			}
		}
		
	}

	while (ioCycles--) {
		system->display.update(1);
		system->apu.update(1);
	}
}

} // namespace Gameboy
