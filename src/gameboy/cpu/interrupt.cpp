#include "cpu.h"
#include "gameboy/memory.h"
#include "util.h"

namespace Gameboy {

void Cpu::interrupt(InterruptVector v) {
	addCycles(12);
	registers.ime = false;
	stackPush(registers.pc);
	registers.pc = dx_util::toUnderlyingType(v);
}

void Cpu::requestInterrupt(Interrupt interrupt) {
	switch (interrupt) {
	case Interrupt::VBlank:
		interruptFlag.vBlank = true;
		if (interruptEnable.vBlank) {
			registers.halted = false;
		}
		break;
	case Interrupt::STAT:
		interruptFlag.STAT = true;
		if (interruptEnable.STAT) {
			registers.halted = false;
		}
		break;
	case Interrupt::Timer:
		interruptFlag.timer = true;
		if (interruptEnable.timer) {
			registers.halted = false;
		}
		break;
	case Interrupt::Serial:
		interruptFlag.serial = true;
		if (interruptEnable.serial) {
			registers.halted = false;
		}
		break;
	case Interrupt::Joypad:
		interruptFlag.joypad = true;
		if (interruptEnable.joypad) {
			registers.halted = false;
			registers.stopped = false;
		}
		break;
	}
}

void Cpu::handleInterrupts() {
	if (!registers.ime)
		return;

	if (interruptFlag.vBlank && interruptEnable.vBlank) {
		interruptFlag.vBlank = false;
		interrupt(InterruptVector::VBlank);
	}
	else if (interruptFlag.STAT && interruptEnable.STAT) {
		interruptFlag.STAT = false;
		interrupt(InterruptVector::STAT);
	}
	else if (interruptFlag.timer && interruptEnable.timer) {
		interruptFlag.timer = false;
		interrupt(InterruptVector::Timer);
	}
	else if (interruptFlag.serial && interruptEnable.serial) {
		interruptFlag.serial = false;
		interrupt(InterruptVector::Serial);
	}
	else if (interruptFlag.joypad && interruptEnable.joypad) {
		interruptFlag.joypad = false;
		interrupt(InterruptVector::Joypad);
	}
}

} // namespace Gameboy
