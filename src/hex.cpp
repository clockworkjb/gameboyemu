#include "hex.h"
#include <iomanip> // std::setw, std::setfill, std::hex

std::ostream& operator << (std::ostream& o, const HexChar& hc)
{
	return (o << std::hex << std::uppercase << std::setw(hc.width) << std::setfill('0') << (int)hc.c << std::dec);
}

HexChar hex(uint16 c) { return HexChar(c); }
HexChar hex(uint16 c, unsigned w) { return HexChar(c, w); }
