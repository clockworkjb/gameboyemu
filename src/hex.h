#pragma once

#include "types.h"

#include <iostream> // std::ostream

struct HexChar
{
	uint16 c;
	unsigned width;
	HexChar(uint16 c, unsigned w = 2) : c(c), width(w) {}

	operator uint16() {
		return c;
	}
};

std::ostream& operator << (std::ostream& o, const HexChar& hc);

HexChar hex(uint16 c);
HexChar hex(uint16 c, unsigned w);
