#pragma once

#include "types.h"
#include <ostream>
#include <cstddef>
#include <cmath>

namespace vmdx {

template <typename T, size_t n> struct vec {
	constexpr static size_t length = n;

	T data[n];

	T& operator [](size_t i) {
		return data[i];
	}

	const T& operator [](size_t i) const {
		return data[i];
	}
};

template <typename T> struct vec<T, 2> {
	union {
		T data[2];
		struct { T x, y; };
	};

	T& operator [](size_t i) {
		return data[i];
	}

	const T& operator [](size_t i) const {
		return data[i];
	}
};

template <typename T> struct vec<T, 3> {
	constexpr static size_t length = 3;
	union {
		T data[3];
		struct { T x, y, z; };
		struct { T r, g, b; };
		vec<T, 2> xy;
	};

	T& operator [](size_t i) {
		return data[i];
	}

	const T& operator [](size_t i) const {
		return data[i];
	}
};

template <typename T> vec<T, 3> cross(const vec<T, 3>& a, const vec<T, 3>& b) {
	vec<T, 3> v;
	v.x = a.y * b.z - a.z * b.y;
	v.y = a.z * b.x - a.x * b.z;
	v.z = a.x * b.y - a.y * b.x;
	return v;
}

template <typename T> struct vec<T, 4> {
	constexpr static size_t length = 4;
	union {
		T data[4];
		struct { T x, y, z, w; };
		struct { T r, g, b, a; };
		vec<T, 2> xy;
		vec<T, 3> xyz;
		vec<T, 3> rgb;
	};

	T& operator [](size_t i) {
		return data[i];
	}

	const T& operator [](size_t i) const {
		return data[i];
	}
};

// 32-bit RGBA color specialization
template <> struct vec<uint8, 4> {
	constexpr static size_t length = 4;
	union {
		uint8 data[4];
		struct { uint8 x, y, z, w; };
		struct { uint8 r, g, b, a; };
		vec<uint8, 2> xy;
		vec<uint8, 3> xyz;
		vec<uint8, 3> rgb;
		uint32 u32;
	};

	constexpr vec(uint8 r = 0, uint8 g = 0, uint8 b = 0, uint8 a = 255) : r(r), g(g), b(b), a(a) {}
	constexpr vec(uint32 u32) : u32(u32) {}

	uint8& operator [](size_t i) {
		return data[i];
	}

	const uint8& operator [](size_t i) const {
		return data[i];
	}

	operator uint32() {
		return u32;
	}
};

using vec2f = vec<float, 2>;
using vec3f = vec<float, 3>;
using vec4f = vec<float, 4>;

using color3f = vec3f;
using color4f = vec4f;

using rgb8 = vec<uint8, 3>;
using rgba8 = vec<uint8, 4>;

constexpr color4f convertRGBA8ToColor4f(rgba8 c) {
	float s = 1.0f / 255.0f;
	return { c.r * s, c.g * s, c.b * s, c.a * s };
}

template <typename T, size_t n> bool operator ==(const vec<T, n>& a, const vec<T, n>& b);
template <typename T, size_t n> bool operator !=(const vec<T, n>& a, const vec<T, n>& b);
template <typename T, size_t n> vec<T, n> operator -(const vec<T, n>& v);
template <typename T, size_t n> vec<T, n>& operator +=(vec<T, n>& a, const vec<T, n>& b);
template <typename T, size_t n> vec<T, n> operator +(const vec<T, n>& a, const vec<T, n>& b);
template <typename T, size_t n> vec<T, n>& operator -=(vec<T, n>& a, const vec<T, n>& b);
template <typename T, size_t n> vec<T, n> operator -(const vec<T, n>& a, const vec<T, n>& b);
template <typename T, size_t n, typename S> vec<T, n>& operator *=(vec<T, n>& a, const S& s);
template <typename T, size_t n, typename S> vec<T, n> operator *(const vec<T, n>& a, const S& s);
template <typename T, size_t n, typename S> vec<T, n> operator *(const S& s, const vec<T, n>& a);
template <typename T, size_t n, typename S> vec<T, n>& operator /=(vec<T, n>& a, const S& s);
template <typename T, size_t n, typename S> vec<T, n> operator /(const vec<T, n>& a, const S& s);
template <typename T, size_t n> T dot(const vec<T, n>& a, const vec<T, n>& b);
template <typename T, size_t n> T lengthSquared(const vec<T, n>& v);
template <typename T, size_t n> T length(const vec<T, n>& v);
template <typename T, size_t n> T distance(const vec<T, n>& a, const vec<T, n>& b);
template <typename T, size_t n> vec<T, n> normalize(const vec<T, n>& v);
template <typename T, size_t n> vec<T, n> reflect(const vec<T, n>& incident, const vec<T, n>& normal);
template <typename T, size_t n, typename S> vec<T, n> refract(const vec<T, n>& incident, const vec<T, n>& normal, const S& r);

template <typename T, size_t n> std::ostream& operator <<(std::ostream& o, const vec<T, n>& v);

template <typename T, size_t n> bool operator ==(const vec<T, n>& a, const vec<T, n>& b) {
	for (size_t i = 0; i < n; ++i) {
		if (a.data[i] != b.data[i])
			return false;
	}
	return true;
}

template <typename T, size_t n> bool operator !=(const vec<T, n>& a, const vec<T, n>& b) {
	return !(a == b);
}

template <typename T, size_t n> vec<T, n> operator -(const vec<T, n>& v) {
	auto result = v;
	for (size_t i = 0; i < n; ++i) {
		result[i] = -result[i];
	}
	return result;
}

template <typename T, size_t n> vec<T, n>& operator +=(vec<T, n>& a, const vec<T, n>& b) {
	for (size_t i = 0; i < n; ++i) {
		a[i] += b[i];
	}
	return a;
}

template <typename T, size_t n> vec<T, n> operator +(const vec<T, n>& a, const vec<T, n>& b) {
	auto v = a;
	v += b;
	return v;
}

template <typename T, size_t n> vec<T, n>& operator -=(vec<T, n>& a, const vec<T, n>& b) {
	for (size_t i = 0; i < n; ++i) {
		a[i] -= b[i];
	}
	return a;
}

template <typename T, size_t n> vec<T, n> operator -(const vec<T, n>& a, const vec<T, n>& b) {
	auto v = a;
	v -= b;
	return v;
}

template <typename T, size_t n, typename S> vec<T, n>& operator *=(vec<T, n>& a, const S& s) {
	for (size_t i = 0; i < n; ++i) {
		a[i] *= s;
	}
	return a;
}

template <typename T, size_t n, typename S> vec<T, n> operator *(const vec<T, n>& a, const S& s) {
	auto v = a;
	v *= s;
	return v;
}

template <typename T, size_t n, typename S> vec<T, n> operator *(const S& s, const vec<T, n>& a) {
	return a * s;
}

template <typename T, size_t n, typename S> vec<T, n>& operator /=(vec<T, n>& a, const S& s) {
	for (size_t i = 0; i < n; ++i) {
		a[i] /= s;
	}
	return a;
}

template <typename T, size_t n, typename S> vec<T, n> operator /(const vec<T, n>& a, const S& s) {
	auto v = a;
	v /= s;
	return v;
}

template <typename T, size_t n> T dot(const vec<T, n>& a, const vec<T, n>& b) {
	T result = {};
	for (size_t i = 0; i < n; ++i) {
		result += a[i] * b[i];
	}
	return result;
}

template <typename T, size_t n> T lengthSquared(const vec<T, n>& v) {
	return dot(v, v);
}

template <typename T, size_t n> T length(const vec<T, n>& v) {
	return std::sqrt(lengthSquared(v));
}

template <typename T, size_t n> T distance(const vec<T, n>& a, const vec<T, n>& b) {
	return length(a - b);
}

template <typename T, size_t n> vec<T, n> normalize(const vec<T, n>& v) {
	auto lenSq = lengthSquared(v);
	if (lenSq == 0)
		return v;
	return v / std::sqrt(lenSq);
}

template <typename T, size_t n> vec<T, n> reflect(const vec<T, n>& incident, const vec<T, n>& normal) {
	return incident - 2 * dot(incident, normal) * normal;
}

template <typename T, size_t n, typename S> vec<T, n> refract(const vec<T, n>& incident, const vec<T, n>& normal, const S& r) {
	auto nDotI = dot(normal, incident);
	auto d = 1 - r * r * (1 - nDotI * nDotI);
	if (d < 0)
		return {};
	return r * incident - (r * nDotI + std::sqrt(d)) * n;
}

template <typename T, size_t n> std::ostream& operator <<(std::ostream& o, const vec<T, n>& v) {
	o << "<";
	for (size_t i = 0; i < n - 1; ++i) {
		o << v[i] << ", ";
	}
	o << v[n - 1] << ">";
	return o;
}

}

