#pragma once

#include "types.h"

#include <string>
#include <algorithm>
#include <type_traits>

namespace dx_util {

	template <typename First, typename ... Args>
	bool compareTo(First&& first, Args&& ... args) {
		return ((first == args) || ...);
	}

	constexpr bool testBit(uint8 byte, unsigned pos) {
		return byte & (1 << pos);
	}

	template<typename T> constexpr T getBit(T byte, unsigned pos) {
		return (byte & (1 << pos)) >> pos;
	}

	constexpr uint8 setBit(uint8 byte, unsigned pos) {
		return byte | (1 << pos);
	}

	constexpr uint8 resetBit(uint8 byte, unsigned pos) {
		return byte & ~(1 << pos);
	}

	constexpr uint8 toggleBit(uint8 byte, unsigned pos) {
		return byte ^ (1 << pos);
	}

	constexpr uint16 word(uint8 lo, uint8 hi) {
		return (hi << 8) | lo;
	}

	template <typename T, typename... Args>
	constexpr T createMask(T mask, T first, Args... rest) {
		mask |= (1 << first);
		if constexpr (sizeof...(rest) > 0)
			return createMask(mask, rest...);
		return mask;
	}

	template <typename T, typename... Args>
	constexpr bool testBits(T value, Args... positions) {
		return value & createMask(0, positions...);
	}

	template <typename T, typename... Args>
	constexpr T setBits(T value, Args... positions) {
		return value | createMask(0, positions...);
	}

	template <typename T, typename... Args>
	constexpr T unsetBits(T value, Args... positions) {
		return value & ~createMask(0, positions...);
	}

	template <typename T, typename... Args>
	constexpr T toggleBits(T value, Args... positions) {
		return value ^ createMask(0, positions...);
	}

	inline std::string ltrim(std::string s) {
		s.erase(begin(s), find_if_not(begin(s), end(s), [](char c) { return isspace(c); }));
		return s;
	}

	inline std::string rtrim(std::string s) {
		s.erase(find_if_not(rbegin(s), rend(s), [](char c) { return isspace(c); }).base(), end(s));
		return s;
	}

	inline std::string trimboth(std::string s) {
		return ltrim(rtrim(s));
	}

	constexpr uint8 convert5Bit(uint8 x) {
		return (x * 255) / 31;
	}

	template <typename T>
	constexpr auto toUnderlyingType(T t) {
		return static_cast<std::underlying_type_t<T>>(t);
	}

}
