#pragma once

struct Texture {
	int width, height;

	// id/pointer to data to be consumed by rendering API
	void* content;
	intptr_t getContent() { return (intptr_t)content; }
};
