#include "nesdebugger.h"

#include "debug.h"
#include "types.h"
#include "nes/cpu.h"

#include "sdl/sdl.h"
#include "renderer.h"
#include "io.h"
#include "texture.h"
#include "vec.h"
#include "nes/ppu/palette.h"

#include "../imgui/imgui.h"
#include "../imgui/imgui_tabs.h"
#include "../imgui/imgui_memory_editor.h"
#include "../imgui/imgui_impl_sdl_gl3.h"

#include <chrono>
#include <unordered_map>
#include <memory>

#include <iostream>
#include <iomanip>

namespace Debugger {

	using namespace std::literals::chrono_literals;

	constexpr int systemHeight = 240, systemWidth = 256;

	enum struct TimingMode { Run, Halt, Stop };

	static struct {
		float scaleX = 4.0, scaleY = 4.0;
		float speedMultiplier = 1.0f;
		bool lockAspectRatio = false;
		bool requireFocus = false;

		std::unordered_map<SDL_Keycode, Nes::Joypad::Button> keys;
		std::unordered_map<SDL_GameControllerButton, Nes::Joypad::Button> buttons;
	} config;

	static struct {
		Nes::Cpu* nes;
		bool initialised = false;
		int height = systemHeight * config.scaleY, width = systemWidth * config.scaleX;

		int frameCount = 0;
		float frameRate = 0;

		std::unique_ptr<SDL::Window> mainWindow;

		bool wantRedisplay = true;
		TimingMode tMode = TimingMode::Run;

		int cyclesLeftover = 0;

		Texture framebuffer, nametable;

		int MAX_CONTROLLERS = 1;
		SDL_GameController *controllers[1];
		int controllerIndex = 0;
	} state;

	static void handleKeys(SDL_KeyboardEvent event) {
		if (!state.mainWindow->focus)
			return;

		auto key = event.keysym;
		bool setting = event.type == SDL_KEYDOWN;

		// TODO: allow inputs for player 2 in a second map
		if (auto it = config.keys.find(key.sym); it != std::end(config.keys))
			state.nes->joypad.toggleButton<0>(it->second, setting);
			
		switch (key.sym) {
		case SDLK_ESCAPE:
			state.tMode = TimingMode::Stop;
			break;
		case SDLK_p:
			if (state.tMode == TimingMode::Run)
				state.tMode = TimingMode::Halt;
			else
				state.tMode = TimingMode::Run;
			break;
		default:
			break;
		}
	}

	static void handleControllerButton(SDL_ControllerButtonEvent &event)
	{
		bool setting = event.state == SDL_PRESSED;

		if (auto it = config.buttons.find(static_cast<SDL_GameControllerButton>(event.button)); it != std::end(config.buttons))
			state.nes->joypad.toggleButton<0>(it->second, setting);
	}

	static void addController(int id)
	{
		if (SDL_IsGameController(id)) {
			if (auto pad = SDL_GameControllerOpen(id); pad) {
				if (state.controllerIndex < state.MAX_CONTROLLERS) {
					state.controllers[state.controllerIndex++] = pad;

					const char *name = SDL_GameControllerName(pad);
					if (!name) name = "Unknown Controller";
					std::cout << "Plugged in controller: " << name << "\n";
				}
			}
		}
	}

	static void removeController(int id)
	{
		--state.controllerIndex;
		auto pad = SDL_GameControllerFromInstanceID(id);
		const char *name = SDL_GameControllerName(pad);
		if (!name) name = "Unknown Controller";
		std::cout << "Removed controller: " << name << "\n";
		SDL_GameControllerClose(pad);
	}


	static void handleEvents() {
		SDL_Event event;
		auto& io = ImGui::GetIO();

		while (SDL_PollEvent(&event)) {
			if (io.WantCaptureMouse || io.WantCaptureKeyboard) {
				ImGui_ImplSdlGL3_ProcessEvent(&event);
			}

			switch (event.type) {
			case SDL_QUIT:
				state.tMode = TimingMode::Stop;
				break;

			case SDL_MOUSEMOTION:
				break;

			case SDL_MOUSEBUTTONDOWN:
				break;

			case SDL_MOUSEBUTTONUP:
				break;

			case SDL_KEYDOWN:
			case SDL_KEYUP:
				handleKeys(event.key);
				break;

			case SDL_CONTROLLERBUTTONDOWN:
			case SDL_CONTROLLERBUTTONUP:
				handleControllerButton(event.cbutton);
				break;

			case SDL_CONTROLLERDEVICEADDED:
				addController(event.cdevice.which);
				break;

			case SDL_CONTROLLERDEVICEREMOVED:
				removeController(event.cdevice.which);
				break;

			case SDL_WINDOWEVENT:
				state.wantRedisplay = true;
				switch (event.window.event) {
				case SDL_WINDOWEVENT_RESIZED:
					if (event.window.windowID == SDL_GetWindowID(state.mainWindow->window)) {
						state.mainWindow->handleResize(event);
					}
					break;

				case SDL_WINDOWEVENT_CLOSE:
					state.tMode = TimingMode::Stop;
					break;
				default:
					break;

				case SDL_WINDOWEVENT_FOCUS_GAINED:
					if (event.window.windowID == SDL_GetWindowID(state.mainWindow->window)) {
						state.mainWindow->gainFocus();
					}
					break;
				case SDL_WINDOWEVENT_FOCUS_LOST:
					if (event.window.windowID == SDL_GetWindowID(state.mainWindow->window)) {
						state.mainWindow->loseFocus();
					}
					break;
				}
				break;

			default:
				break;
			}
		}
	}
	static void updateGUI() {
		ImGui_ImplSdlGL3_NewFrame(state.mainWindow->window);
		//ImGui::SetNextWindowPos(ImVec2(0, 0));
		//ImGui::SetNextWindowSize(ImVec2(state.debugWindow->width, state.debugWindow->height));

		ImGui::Begin("Debugger", nullptr, 0);
		const auto dimensions = ImGui::GetContentRegionAvail();
		if (ImGui::CollapsingHeader("PPU")) {
			auto& paletteRam = state.nes->ppu.paletteRam;
			ImGui::Text("Palette");
			for (int i = 0; i < 32; ++i) {
				if ((i % 4) == 0) {
					ImGui::Spacing();
					ImGui::Text("Palette %d", i / 4 + 1);
				}
				auto floatCol = vmdx::convertRGBA8ToColor4f(palette[paletteRam[i]]);
				ImGui::Text("Address: %04x Value: %02x", 0x3f00 + i, paletteRam[i]);
				ImGui::ColorEdit3("Color##i", &floatCol.r);
			}
		}
		if (ImGui::CollapsingHeader("Tile Map")) {
			const auto[width, height] = Image::resizeMaintainAspect(256, 256, dimensions.x, dimensions.y);
			ImGui::Image((ImTextureID)state.nametable.getContent(), ImVec2(width, height));
		}
		ImGui::End();
	}

	using FramePeriod = std::chrono::duration<long long int, std::ratio<1, 60>>;
	constexpr auto singleFrame = FramePeriod(1);

	static void idle() {
		static auto startTime = std::chrono::steady_clock::now();
		static auto last = startTime;
		static auto accumulator = 0ns;

		auto now = std::chrono::steady_clock::now();
		auto frameTime = now - last;
		last = now;

		accumulator += frameTime;

		if (accumulator > 100ms)
			accumulator = 100ms;

		while (accumulator >= singleFrame) {
			if (state.tMode == TimingMode::Run) {
				state.cyclesLeftover = state.nes->run((Nes::Cpu::cyclesPerFrame + state.cyclesLeftover) * config.speedMultiplier);
			}

			accumulator -= std::chrono::duration_cast<std::chrono::nanoseconds>(singleFrame);
			++state.frameCount;
		}
	}

	void mainLoop() {
		if (!state.initialised) {
			return;
		}

		while (state.tMode != TimingMode::Stop) {
			handleEvents();

			if (state.wantRedisplay) {
				Renderer::updateTexture(state.framebuffer, state.nes->ppu.frontbuffer);
				Renderer::updateTexture(state.nametable, state.nes->ppu.nametableImage[0]);

				Renderer::viewport(*state.mainWindow);
				Renderer::clear();
				Renderer::renderTexture(state.framebuffer);
				updateGUI();
				ImGui::Render();
				ImGui_ImplSdlGL3_RenderDrawData(ImGui::GetDrawData());
				Renderer::swap(*state.mainWindow);
			}

			idle();
		}

		ImGui_ImplSdlGL3_Shutdown();
		ImGui::DestroyContext();


		Renderer::cleanupTexture(state.framebuffer);
		Renderer::cleanupTexture(state.nametable);
		Renderer::cleanup();
		SDL_Quit();
	}

	bool init(Nes::Cpu* nes) {
		state.nes = nes;

		if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER | SDL_INIT_AUDIO) < 0) {
			SDL::sdlError("Unable to init SDL: ");
			return false;
		}

		state.mainWindow = std::make_unique<SDL::Window>("GB Emulator", state.width, state.height);

		if (!state.mainWindow->window) {
			SDL::sdlError("Failed to create a window: ");
			return false;
		}

		state.mainWindow->raise();

		std::cout << "using OpenGL renderer" << "\n";
		if (!Renderer::init(*state.mainWindow)) {
			std::cerr << "Failed to initialize OpenGL renderer!" << "\n";
			return false;
		}

		Renderer::generateTexture(state.framebuffer, systemWidth, systemHeight);
		Renderer::generateTexture(state.nametable, systemWidth, systemHeight);

		ImGui::CreateContext();

		auto& io = ImGui::GetIO();
		io.DisplaySize.x = state.mainWindow->width;
		io.DisplaySize.y = state.mainWindow->height;

		auto& style = ImGui::GetStyle();
		style.WindowRounding = 0;
		style.WindowBorderSize = 0;

		ImGui_ImplSdlGL3_Init(state.mainWindow->window);

		config.keys[SDLK_z] = Nes::Joypad::Button::A;
		config.keys[SDLK_x] = Nes::Joypad::Button::B;
		config.keys[SDLK_RETURN] = Nes::Joypad::Button::Start;
		config.keys[SDLK_RSHIFT] = Nes::Joypad::Button::Select;
		config.keys[SDLK_UP] = Nes::Joypad::Button::Up;
		config.keys[SDLK_DOWN] = Nes::Joypad::Button::Down;
		config.keys[SDLK_LEFT] = Nes::Joypad::Button::Left;
		config.keys[SDLK_RIGHT] = Nes::Joypad::Button::Right;

		config.buttons[SDL_CONTROLLER_BUTTON_A] = Nes::Joypad::Button::A;
		config.buttons[SDL_CONTROLLER_BUTTON_B] = Nes::Joypad::Button::B;
		config.buttons[SDL_CONTROLLER_BUTTON_X] = Nes::Joypad::Button::B;
		config.buttons[SDL_CONTROLLER_BUTTON_START] = Nes::Joypad::Button::Start;
		config.buttons[SDL_CONTROLLER_BUTTON_BACK] = Nes::Joypad::Button::Select;
		config.buttons[SDL_CONTROLLER_BUTTON_DPAD_UP] = Nes::Joypad::Button::Up;
		config.buttons[SDL_CONTROLLER_BUTTON_DPAD_DOWN] = Nes::Joypad::Button::Down;
		config.buttons[SDL_CONTROLLER_BUTTON_DPAD_LEFT] = Nes::Joypad::Button::Left;
		config.buttons[SDL_CONTROLLER_BUTTON_DPAD_RIGHT] = Nes::Joypad::Button::Right;

		state.initialised = true;

		return true;
	}

} // namespace UI
