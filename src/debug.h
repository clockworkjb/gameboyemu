#pragma once

#define DEBUG_BUILD
#define DEBUG_MEM
// #define DEBUG_TIMING
// #define DEBUG_CPU
// #define DEBUG_VIDEO

#ifdef DEBUG_BUILD
#	include <iostream>
#	define DEBUG(x) do { std::cerr << x << "\n"; } while (0)
#	ifdef DEBUG_MEM
#		define DEBUG_M(x) do { std::cerr << x << "\n"; } while (0)
#	else
#		define DEBUG_M(x) do { } while (0)
#	endif
#	ifdef DEBUG_TIMING
#		define DEBUG_T(x) do { std::cerr << x << "\n"; } while (0)
#	else
#		define DEBUG_T(x) do { } while (0)
#	endif
#	ifdef DEBUG_CPU
#		define DEBUG_C(x) do { std::cerr << x << "\n"; } while (0)
#	else
#		define DEBUG_C(x) do { } while (0)
#	endif
#	ifdef DEBUG_VIDEO
#		define DEBUG_V(x) do { std::cerr << x << "\n"; } while (0)
#	else
#		define DEBUG_V(x) do { } while (0)
#	endif
#else
#	define DEBUG(x) do { } while (0)
#endif
