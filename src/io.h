#pragma once

#include "types.h"

#include <string>
#include <string_view>
#include <vector>
#include <filesystem>

namespace IO {

namespace fs = std::filesystem;

std::vector<uint8> loadFile(const fs::path& path) noexcept;
std::string loadFileSigned(const fs::path& path) noexcept;
void saveFile(const fs::path& path, const std::vector<uint8>& data) noexcept;
void saveFile(const fs::path& path, std::string_view data) noexcept;
bool createDir(const fs::path& path) noexcept;

}
