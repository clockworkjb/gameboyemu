#include "cpu.h"

#include "disassemble.h"

namespace Nes {

int Cpu::execute() {
	int cyclesCompleted = cycleCount;

	op = readImmediate8();
	
	switch (op) {
	case 0x00:
		brk();
		break;
	case 0x01:
		ora<&Cpu::indirectX>();
		break;
	case 0x05:
		ora<&Cpu::zeroPage>();
		break;
	case 0x06:
		asl<&Cpu::zeroPage>();
		break;
	case 0x08:
		push(registers.p);
		break;
	case 0x09:
		ora<&Cpu::immediate>();
		break;
	case 0x0a:
		asla();
		break;
	case 0x0d:
		ora<&Cpu::absolute>();
		break;
	case 0x0e:
		asl<&Cpu::absolute>();
		break;
	case 0x10:
		branch(!registers.p.n);
		break;
	case 0x11:
		ora<&Cpu::indirectY>();
		break;
	case 0x15:
		ora<&Cpu::zeroPageX>();
		break;
	case 0x16:
		asl<&Cpu::zeroPageX>();
		break;
	case 0x18:
		setFlag<false>(registers.p.c);
		break;
	case 0x19:
		ora<&Cpu::absoluteY>();
		break;
	case 0x1d:
		ora<&Cpu::absoluteX>();
		break;
	case 0x1e:
		asl<&Cpu::absoluteX>();
		break;
	case 0x20:
		jsr();
		break;
	case 0x21:
		AND<&Cpu::indirectX>();
		break;
	case 0x24:
		bit<&Cpu::zeroPage>();
		break;
	case 0x25:
		AND<&Cpu::zeroPage>();
		break;
	case 0x26:
		rol<&Cpu::zeroPage>();
		break;
	case 0x28:
		pull(registers.p);
		break;
	case 0x29:
		AND<&Cpu::immediate>();
		break;
	case 0x2a:
		rola();
		break;
	case 0x2c:
		bit<&Cpu::absolute>();
		break;
	case 0x2d:
		AND<&Cpu::absolute>();
		break;
	case 0x2e:
		rol<&Cpu::absolute>();
		break;
	case 0x30:
		branch(registers.p.n);
		break;
	case 0x31:
		AND<&Cpu::indirectY>();
		break;
	case 0x35:
		AND<&Cpu::zeroPageX>();
		break;
	case 0x36:
		rol<&Cpu::zeroPageX>();
		break;
	case 0x38:
		setFlag<true>(registers.p.c);
		break;
	case 0x3d:
		AND<&Cpu::absoluteX>();
		break;
	case 0x39:
		AND<&Cpu::absoluteY>();
		break;
	case 0x3e:
		rol<&Cpu::absoluteX>();
		break;
	case 0x40:
		rti();
		break;
	case 0x41:
		eor<&Cpu::indirectX>();
		break;
	case 0x45:
		eor<&Cpu::zeroPage>();
		break;
	case 0x46:
		lsr<&Cpu::zeroPage>();
		break;
	case 0x48:
		push(registers.a);
		break;
	case 0x49:
		eor<&Cpu::immediate>();
		break;
	case 0x4a:
		lsra();
		break;
	case 0x4c:
		jmp();
		break;
	case 0x4d:
		eor<&Cpu::absolute>();
		break;
	case 0x4e:
		lsr<&Cpu::absolute>();
		break;
	case 0x50:
		branch(!registers.p.v);
		break;
	case 0x51:
		eor<&Cpu::indirectY>();
		break;
	case 0x55:
		eor<&Cpu::zeroPageX>();
		break;
	case 0x56:
		lsr<&Cpu::zeroPageX>();
		break;
	case 0x58:
		setFlag<false>(registers.p.i);
		break;
	case 0x59:
		eor<&Cpu::absoluteY>();
		break;
	case 0x5d:
		eor<&Cpu::absoluteX>();
		break;
	case 0x5e:
		lsr<&Cpu::absoluteX>();
		break;
	case 0x60:
		rts();
		break;
	case 0x61:
		adc<&Cpu::indirectX>();
		break;
	case 0x65:
		adc<&Cpu::zeroPage>();
		break;
	case 0x66:
		ror<&Cpu::zeroPage>();
		break;
	case 0x68:
		pull(registers.a);
		setNZ(registers.a);
		break;
	case 0x69:
		adc<&Cpu::immediate>();
		break;
	case 0x6a:
		rora();
		break;
	case 0x6c:
		jmpIndirect();
		break;
	case 0x6d:
		adc<&Cpu::absolute>();
		break;
	case 0x6e:
		ror<&Cpu::absolute>();
		break;
	case 0x70:
		branch(registers.p.v);
		break;
	case 0x71:
		adc<&Cpu::indirectY>();
		break;
	case 0x75:
		adc<&Cpu::zeroPageX>();
		break;
	case 0x76:
		ror<&Cpu::zeroPageX>();
		break;
	case 0x78:
		setFlag<true>(registers.p.i);
		break;
	case 0x79:
		adc<&Cpu::absoluteY>();
		break;
	case 0x7d:
		adc<&Cpu::absoluteX>();
		break;
	case 0x7e:
		ror<&Cpu::absoluteX>();
		break;
	case 0x81:
		st<&Cpu::indirectX>(registers.a);
		break;
	case 0x84:
		st<&Cpu::zeroPage>(registers.y);
		break;
	case 0x85:
		st<&Cpu::zeroPage>(registers.a);
		break;
	case 0x86:
		st<&Cpu::zeroPage>(registers.x);
		break;
	case 0x88:
		dec(registers.y);
		break;
	case 0x8a:
		tr(registers.x, registers.a);
		break;
	case 0x8c:
		st<&Cpu::absolute>(registers.y);
		break;
	case 0x8d:
		st<&Cpu::absolute>(registers.a);
		break;
	case 0x8e:
		st<&Cpu::absolute>(registers.x);
		break;
	case 0x90:
		branch(!registers.p.c);
		break;
	case 0x91:
		st<&Cpu::indirectY>(registers.a);
		break;
	case 0x94:
		st<&Cpu::zeroPageX>(registers.y);
		break;
	case 0x95:
		st<&Cpu::zeroPageX>(registers.a);
		break;
	case 0x96:
		st<&Cpu::zeroPageY>(registers.x);
		break;
	case 0x98:
		tr(registers.y, registers.a);
		break;
	case 0x99:
		st<&Cpu::absoluteY>(registers.a);
		break;
	case 0x9a:
		tr(registers.x, registers.sp, false);
		break;
	case 0x9d:
		st<&Cpu::absoluteX>(registers.a);
		break;
	case 0xa0:
		ld<&Cpu::immediate>(registers.y);
		break;
	case 0xa1:
		ld<&Cpu::indirectX>(registers.a);
		break;
	case 0xa2:
		ld<&Cpu::immediate>(registers.x);
		break;
	case 0xa4:
		ld<&Cpu::zeroPage>(registers.y);
		break;
	case 0xa5:
		ld<&Cpu::zeroPage>(registers.a);
		break;
	case 0xa6:
		ld<&Cpu::zeroPage>(registers.x);
		break;
	case 0xa8:
		tr(registers.a, registers.y);
		break;
	case 0xa9:
		ld<&Cpu::immediate>(registers.a);
		break;
	case 0xaa:
		tr(registers.a, registers.x);
		break;
	case 0xac:
		ld<&Cpu::absolute>(registers.y);
		break;
	case 0xad:
		ld<&Cpu::absolute>(registers.a);
		break;
	case 0xae:
		ld<&Cpu::absolute>(registers.x);
		break;
	case 0xb0:
		branch(registers.p.c);
		break;
	case 0xb1:
		ld<&Cpu::indirectY>(registers.a);
		break;
	case 0xb4:
		ld<&Cpu::zeroPageX>(registers.y);
		break;
	case 0xb5:
		ld<&Cpu::zeroPageX>(registers.a);
		break;
	case 0xb6:
		ld<&Cpu::zeroPageY>(registers.x);
		break;
	case 0xb8:
		setFlag<false>(registers.p.v);
		break;
	case 0xb9:
		ld<&Cpu::absoluteY>(registers.a);
		break;
	case 0xba:
		tr(registers.sp, registers.x);
		break;
	case 0xbc:
		ld<&Cpu::absoluteX>(registers.y);
		break;
	case 0xbd:
		ld<&Cpu::absoluteX>(registers.a);
		break;
	case 0xbe:
		ld<&Cpu::absoluteY>(registers.x);
		break;
	case 0xc0:
		cmp<&Cpu::immediate>(registers.y);
		break;
	case 0xc1:
		cmp<&Cpu::indirectX>(registers.a);
		break;
	case 0xc4:
		cmp<&Cpu::zeroPage>(registers.y);
		break;
	case 0xc5:
		cmp<&Cpu::zeroPage>(registers.a);
		break;
	case 0xc6:
		dec<&Cpu::zeroPage>();
		break;
	case 0xc8:
		inc(registers.y);
		break;
	case 0xc9:
		cmp<&Cpu::immediate>(registers.a);
		break;
	case 0xca:
		dec(registers.x);
		break;
	case 0xcc:
		cmp<&Cpu::absolute>(registers.y);
		break;
	case 0xcd:
		cmp<&Cpu::absolute>(registers.a);
		break;
	case 0xce:
		dec<&Cpu::absolute>();
		break;
	case 0xd0:
		branch(!registers.p.z);
		break;
	case 0xd1:
		cmp<&Cpu::indirectY>(registers.a);
		break;
	case 0xd5:
		cmp<&Cpu::zeroPageX>(registers.a);
		break;
	case 0xd6:
		dec<&Cpu::zeroPageX>();
		break;
	case 0xd8:
		setFlag<false>(registers.p.d);
		break;
	case 0xd9:
		cmp<&Cpu::absoluteY>(registers.a);
		break;
	case 0xdd:
		cmp<&Cpu::absoluteX>(registers.a);
		break;
	case 0xde:
		dec<&Cpu::absoluteX>();
		break;
	case 0xe0:
		cmp<&Cpu::immediate>(registers.x);
		break;
	case 0xe1:
		sbc<&Cpu::indirectX>();
		break;
	case 0xe4:
		cmp<&Cpu::zeroPage>(registers.x);
		break;
	case 0xe5:
		sbc<&Cpu::zeroPage>();
		break;
	case 0xe6:
		inc<&Cpu::zeroPage>();
		break;
	case 0xe8:
		inc(registers.x);
		break;
	case 0xe9:
		sbc<&Cpu::immediate>();
		break;
	case 0xea:
		nop();
		break;
	case 0xec:
		cmp<&Cpu::absolute>(registers.x);
		break;
	case 0xed:
		sbc<&Cpu::absolute>();
		break;
	case 0xee:
		inc<&Cpu::absolute>();
		break;
	case 0xf0:
		branch(registers.p.z);
		break;
	case 0xf1:
		sbc<&Cpu::indirectY>();
		break;
	case 0xf5:
		sbc<&Cpu::zeroPageX>();
		break;
	case 0xf6:
		inc<&Cpu::zeroPageX>();
		break;
	case 0xf8:
		setFlag<true>(registers.p.d);
		break;
	case 0xf9:
		sbc<&Cpu::absoluteY>();
		break;
	case 0xfd:
		sbc<&Cpu::absoluteX>();
		break;
	case 0xfe:
		inc<&Cpu::absoluteX>();
		break;
	default:
		unknownOp();
		break;
	}

	++instructionCount;
	lastCycleCount = cycleCount - cyclesCompleted;
	return lastCycleCount;
}

int Cpu::run(int cyclesNeeded) {
	while (cyclesNeeded > 0) {
		nmi = ppu.checkNMI();
		if (nmi) {
			doInterrupt(Interrupt::NMI);
		}
		else if (irq && !registers.p.i) {
			doInterrupt(Interrupt::IRQ);
		}
		cyclesNeeded -= execute();
	}

	return cyclesNeeded;
}

void Cpu::doInterrupt(Interrupt i) {
	if (i != Interrupt::BRK) {
		tick();
	}

	if (i == Interrupt::RST) {
		tick(3);
		registers.sp -= 3;
	}
	else {
		stackPush((registers.pc >> 8) & 0xff);
		stackPush(registers.pc & 0xff);
		if (i == Interrupt::BRK) registers.p.b = true;
		stackPush(registers.p);
	}

	registers.p.i = true;

	uint16 intVec = 0;
	switch(i) {
		case Interrupt::BRK:
		case Interrupt::IRQ:
			intVec = 0xfffe;
			break;
		case Interrupt::NMI:
			intVec = 0xfffa;
			break;
		case Interrupt::RST:
			intVec = 0xfffc;
			break;
	}
	uint8 lo = readMem(intVec);
	uint8 hi = readMem(++intVec);
	registers.pc = dx_util::word(lo, hi);
}

uint16 Cpu::immediate() {
	return registers.pc++;
}

uint8 Cpu::readImmediate8() {
	return readMem(immediate());
}

uint16 Cpu::readImmediate16() {
	uint8 lo = readImmediate8();
	uint8 hi = readImmediate8();
	return dx_util::word(lo, hi);
}

uint8 Cpu::readMem(uint16 addr) {
	tick();
	return read(addr);
}

void Cpu::writeMem(uint16 addr, uint8 val) {
	tick();
	write(addr, val);
}

uint16 Cpu::zeroPage() {
	return readImmediate8();
}

uint16 Cpu::absolute() {
	return readImmediate16();
}

uint16 Cpu::absoluteX() {
	return registers.x + readImmediate16();
}

uint16 Cpu::absoluteY() {
	return registers.y + readImmediate16();
}

uint16 Cpu::zeroPageX() {
	tick();
	return (registers.x + readImmediate8()) % 256;
}

uint16 Cpu::zeroPageY() {
	tick();
	return (registers.y + readImmediate8()) % 256;
}

uint16 Cpu::indirectX() {
	tick();
	uint8 v = readImmediate8() + registers.x;
	uint8 lo = readMem(v);
	uint8 hi = readMem(++v);
	return dx_util::word(lo, hi);
}

uint16 Cpu::indirectY() {
	uint8 v = readImmediate8();
	uint8 lo = readMem(v);
	uint8 hi = readMem(++v);
	uint16 addr = dx_util::word(lo, hi);
	pageCheck(addr, addr + registers.y);
	return addr + registers.y;
}

void Cpu::pageCheck(uint16 a, uint16 b) {
	if ((a & 0xff00) != (b & 0x00ff)) tick();
}

void Cpu::tick(int cycles) {
	while (cycles--) {
		ppu.tick(3);
		apu.tick();

		++cycleCount;
	}
}

} // namespace Nes
