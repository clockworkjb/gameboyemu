#include "cpu.h"

namespace Nes {

uint8 Cpu::read(uint16 address) {
	if (address <= 0x1fff) {
		return ram[address % 0x0800];
	}
	else if (address >= 0x2000 && address <= 0x3fff) {
		return ppu.read(address);
	}
	if (address == 0x4016) {
		return joypad.read<0>();
	}
	if (address == 0x4017) {
		return joypad.read<1>();
	}
	else if (address >= 0x4018) {
		return cart.read(address);
	}
	else {
		return 0xff;
	}
}

void Cpu::write(uint16 address, uint8 v) {
	if (address <= 0x1fff) {
		ram[address % 0x0800] = v;
	}
	else if (address >= 0x2000 && address <= 0x3fff) {
		ppu.write(address, v);
	}
	if (address == 0x4014) {
		oamDMA(v);
	}
	if (address == 0x4016) {
		joypad.write(v);
	}
	else if (address >= 0x4018) {
		cart.write(address, v);
	}
}

void Cpu::oamDMA(uint8 v) {
	tick();
	for (uint16 i = 0; i < 256; ++i) {
		writeMem(0x2014, readMem(v * 0x100 + i));
	}
}

} // namepace Nes
