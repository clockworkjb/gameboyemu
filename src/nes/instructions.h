void unknownOp() {
	//DEBUG("Unknown opcode: " << hex(op, 4));
}

void setNZ(uint8 r) {
	registers.p.z = (r == 0);
	registers.p.n = dx_util::testBit(r, 7);
}

void brk() {
	readImmediate8();
	/*stackPush((registers.pc >> 8) & 0xff);
	stackPush(registers.pc & 0xff);
	registers.p.b = true;
	stackPush(registers.p | 0x30);
	registers.p.i = true;
	uint8 lo = readMem(0xfffe);
	uint8 hi = readMem(0xffff);
	registers.pc = word(lo, hi);*/
	doInterrupt(Interrupt::BRK);
}

template <auto mode> void AND() {
	uint8 v = fetch<mode>();
	registers.a &= v;
	setNZ(registers.a);
}

template <auto mode> void adc() {
	uint8 v = fetch<mode>();
	int16 result = registers.a + v + registers.p.c;
	registers.p.v = ~(registers.a ^ v) & (registers.a ^ result) & 0x80;
	registers.p.c = (result > 0xff);
	setNZ(result);
	registers.a = result;
}

template <auto mode> void asl() {
	uint16 addr = getAddr<mode>();
	uint8 v = readMem(addr);
	uint8 result = v << 1;
	registers.p.c = dx_util::testBit(v, 7);
	setNZ(result);
	tick();
	writeMem(addr, result);
}

void asla() {
	uint8 v = registers.a;
	uint8 result = v << 1;
	registers.p.c = dx_util::testBit(v, 7);
	setNZ(result);
	tick();
	registers.a = result;
}

void branch(bool c) {
	int8 v = readImmediate8();

	if (c) {
		tick();
		uint16 dest = registers.pc + v;
		registers.pc = dest;
	}
}

template <auto mode> void bit() {
	uint8 v = fetch<mode>();
	registers.p.z = !(registers.a & v);
	registers.p.n = dx_util::testBit(v, 7);
	registers.p.v = dx_util::testBit(v, 6);
}

template <bool v> void setFlag(bool& f) {
	tick();
	f = v;
}

template <auto mode> void cmp(uint8 x) {
	uint8 v = fetch<mode>();
	int16 result = x - v;
	registers.p.c = (result >= 0);
	setNZ(result);
}

template <auto mode> void dec() {
	uint16 addr = getAddr<mode>();
	uint8 v = readMem(addr);
	setNZ(--v);
	tick();
	writeMem(addr, v);
}

void dec(uint8& x) {
	tick();
	setNZ(--x);
}

template <auto mode> void eor() {
	uint8 v = fetch<mode>();
	registers.a ^= v;
	setNZ(registers.a);
}

template <auto mode> void inc() {
	uint16 addr = getAddr<mode>();
	uint8 v = readMem(addr);
	setNZ(++v);
	tick();
	writeMem(addr, v);
}

void inc(uint8& x) {
	tick();
	setNZ(++x);
}

void jmp() {
	registers.pc = readImmediate16();
}

void jmpIndirect() {
	uint8 lo = readImmediate8();
	uint8 hi = readImmediate8();
	uint8 lo2 = readMem(dx_util::word(lo, hi));
	uint8 hi2 = readMem(dx_util::word(++lo, hi));
	registers.pc = dx_util::word(lo2, hi2);
}

void stackPush(uint8 data) {
	writeMem(registers.sp-- | 256, data);
}

uint8 stackPop() {
	return readMem(++registers.sp | 256);
}

void jsr() {
	uint16 l = registers.pc + 1;
	tick();
	stackPush(l >> 8);
	stackPush(l);
	jmp();
}

template <auto mode> void ld(uint8& r) {
	r = fetch<mode>();
	setNZ(r);
}

template <auto mode> void lsr() {
	uint16 addr = getAddr<mode>();
	uint8 v = readMem(addr);
	v = rightShift(v);
	writeMem(addr, v);
}

void lsra() {
	registers.a = rightShift(registers.a);
}

uint8 rightShift(uint8 x) {
	registers.p.c = dx_util::testBit(x, 0);
	x >>= 1;
	setNZ(x);
	tick();
	return x;
}

void nop() {
	tick();
}

template <auto mode> void ora() {
	uint8 v = fetch<mode>();
	registers.a |= v;
	setNZ(registers.a);
}

void push(uint8 r) {
	tick();
	stackPush(r);
}

template<typename T> void pull(T& r) {
	tick(2);
	r = stackPop();
}

uint8 rotateLeft(uint8 x) {
	uint8 c = registers.p.c;
	registers.p.c = x & 0x80;
	x = (x << 1) | c;
	setNZ(x);
	tick();
	return x;
}

template <auto mode> void rol() {
	uint16 addr = getAddr<mode>();
	uint8 v = readMem(addr);
	v = rotateLeft(v);
	writeMem(addr, v);
}

void rola() {
	registers.a = rotateLeft(registers.a);
}

uint8 rotateRight(uint8 x) {
	uint8 c = registers.p.c << 7;
	registers.p.c = x & 0x01;
	x = (x >> 1) | c;
	setNZ(x);
	tick();
	return x;
}

template <auto mode> void ror() {
	uint16 addr = getAddr<mode>();
	uint8 v = readMem(addr);
	v = rotateRight(v);
	writeMem(addr, v);
}

void rora() {
	registers.a = rotateRight(registers.a);
}

void rti() {
	pull(registers.p);
	uint8 lo = stackPop();
	uint8 hi = stackPop();
	registers.pc = dx_util::word(lo, hi);
}

void rts() {
	tick(2);
	uint8 lo = stackPop();
	uint8 hi = stackPop();
	registers.pc = dx_util::word(lo, hi) + 1;
}

template <auto mode> void sbc() {
	uint8 v = fetch<mode>();
	v ^= 0xff;
	int16 result = registers.a + v + registers.p.c;
	registers.p.v = ~(v ^ registers.a) & (registers.a ^ result) & 0x80;
	registers.p.c = result > 0xff;
	setNZ(result);
	registers.a = result;
}

template <auto mode> void st(uint8 r) {
	uint16 addr = getAddr<mode>();
	writeMem(addr, r);
}

void tr(uint8 s, uint8& d, bool flag = true) {
	tick();
	d = s;
	if (flag)
		setNZ(d);
}
