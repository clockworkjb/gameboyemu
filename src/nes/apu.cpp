#include "apu.h"

namespace Nes {

void Apu::tick(int cycles) {
	while (cycles > 0) {
		--cycles;
	}
}

} // namespace Nes
