#include "ppu.h"

#include "nes/cartridge/cartridge.h"
#include "util.h"
#include "hex.h"
#include "debug.h"

namespace Nes {

	uint8 Ppu::read(uint16 address) {
		uint8 value = 0;

		switch (address & 7) {
		case 2: // PPUSTATUS
			value |= inVblank << 7;
			value |= spriteZeroHit << 6;
			value |= spriteOverflow << 5;
			value |= lastWrite & 0x1f;

			inVblank = false;
			registers.w = false;
			break;
		case 4: // OAMDATA
			value = oamMem[oamAddr];
			break;
		case 7: // PPUDATA
			if (enabled() && (scanline <= 240 || scanline == 261))
				break;
			value = busData;
			busData = vRamRead(registers.v.address);
			registers.v.address += vramIncrement ? 32 : 1;
			break;
		}
		return value;
	}

	uint16 Ppu::getNametableAddress(uint16 address) {
		switch (mirroring) {
		case Mirroring::horizontal:
			return ((address >> 1) & 0x400) + (address % 0x400);
		case Mirroring::vertical:
			return address % 0x800;
		case Mirroring::singleScreen:
			return address % 0x400;
		default:
			return address % 0x800;
		}
	}

	void Ppu::write(uint16 address, uint8 v) {
		lastWrite = v;

		switch (address & 7) {
		case 0: // PPUCTRL
			enableNMI = dx_util::testBit(v, 7);
			masterSlaveSelect = dx_util::testBit(v, 6);
			spriteSize = dx_util::testBit(v, 5);
			bgTableAddr = dx_util::testBit(v, 4);
			spriteTableAddr = dx_util::testBit(v, 3);
			vramIncrement = dx_util::testBit(v, 2);
			registers.t.nametableSelect = v & 3;
			break;
		case 1: // PPUMASK
			emphasis.b = dx_util::testBit(v, 7);
			emphasis.g = dx_util::testBit(v, 6);
			emphasis.r = dx_util::testBit(v, 5);
			enableSprites = dx_util::testBit(v, 4);
			enableBG = dx_util::testBit(v, 3);
			enableLeftmostSprites = dx_util::testBit(v, 2);
			enableLeftmostBG = dx_util::testBit(v, 1);
			greyscale = dx_util::testBit(v, 0);
			break;
		case 3: // OAMADDR
			oamAddr = v;
			break;
		case 4: // OAMDATA
			oamMem[oamAddr++] = v;
			break;
		case 5: // PPUSCROLL
			if (!registers.w) {
				registers.x = v & 7;
				registers.t.coarseX = v >> 3;
			}
			else {
				registers.t.fineY = v & 7;
				registers.t.coarseY = v >> 3;
			}
			registers.w = !registers.w;
			break;
		case 6: // PPUADDR
			if (!registers.w) {
				registers.t.hi = v & 0x3f;
			}
			else {
				registers.t.lo = v;
				registers.v.address = registers.t.address;
			}
			registers.w = !registers.w;
			break;
		case 7: // PPUDATA
			if (enabled() && (scanline <= 240 || scanline == 261))
				break;
			vRamWrite(registers.v.address, v);
			registers.v.address += vramIncrement ? 32 : 1;
			break;
		}
	}

	uint8 Ppu::readPaletteRam(uint16 address) {
		if ((address & 0x13) == 0x10)
			address &= ~0x10;
		auto ret = paletteRam[address];
		if (greyscale)
			ret &= 0x30;
		return ret;
	}

	void Ppu::writePaletteRam(uint16 address, uint8 v) {
		if ((address & 0x13) == 0x10)
			address &= ~0x10;
		paletteRam[address] = v;
	}
	
	uint8 Ppu::vRamRead(uint16 address) {
		if (address <= 0x1fff) { // CHR
			return cart->chrRead(address);
		}
		else if (address <= 0x3eff) { // Nametables
			return nametables[getNametableAddress(address)];
		}
		else if (address <= 0x3fff) { // palette
			return readPaletteRam(address & 0x1f);
		}
		else {
			return 0;
		}
	}

	void Ppu::vRamWrite(uint16 address, uint8 v) {
		if (address <= 0x1fff) { // CHR
			cart->chrWrite(address, v);
		}
		else if (address <= 0x3eff) { // Nametables
			nametables[getNametableAddress(address)] = v;
		}
		else if (address <= 0x3fff) { // palette
			writePaletteRam(address & 0x1f, v);
		}
	}
}
