#include "ppu.h"

#include "nes/cartridge/cartridge.h"
#include "palette.h"
#include "util.h"
#include "debug.h"
#include "hex.h"

#include <cassert>

namespace Nes {

static constexpr vmdx::rgba8 debugPalette[4] {
	{ 232, 232, 232, 255 },
	{ 160, 160, 160, 255 },
	{ 88, 88, 88, 255 },
	{ 16, 16, 16, 255 },
};

Ppu::Ppu(Cartridge* cart) : cart(cart) {
	assert(cart);
	assert(cart->mapper);
	mirroring = cart->mapper->mirroring;
}

static constexpr int cyclesPerScanline = 341;
static constexpr int scanlinesPerFrame = 262;
static constexpr int cyclesPerFrame = scanlinesPerFrame * cyclesPerScanline;

bool Ppu::enabled() {
	return enableBG || enableSprites;
}

void Ppu::fetchNametableAddress() {
	nametableAddress = (registers.v.address & 0xfff) | 0x2000;
}

void Ppu::fetchAttributeAddress() {
	attributeAddress = 0x23c0 | (registers.v.nametableSelect << 10) | (registers.v.coarseY >> 2) << 3 | (registers.v.coarseX >> 2);
}

void Ppu::fetchTileAddress() {
	tileAddress = (bgTableAddr * 0x1000) | (nametable << 4) | registers.v.fineY;
}

enum struct ScanlineType {
	Visible, PostRender, VBlank, PreRender
};

void Ppu::tick(int cycles) {
	while (cycles--) {

		if (scanline < 240)
			doScanline<ScanlineType::Visible>();
		else if (scanline == 240)
			postRenderScanline();
		else if (scanline > 240 && scanline < 261)
			vBlankScanline();
		else if (scanline == 261)
			doScanline<ScanlineType::PreRender>();

		if (++scanlineCycle > 340) {
			scanlineCycle = 0;
			if (++scanline > 261) {
				scanline = 0;
				oddFrame = !oddFrame;
				if (oddFrame)
					++scanlineCycle;
				updatePatternTable();
				updateNametableImage();
			}
		}
	}
}

template <Ppu::ScanlineType type> void Ppu::doScanline() {
	static_assert(type == ScanlineType::Visible || type == ScanlineType::PreRender);

	// --- Backgrounds ---
	if (scanlineCycle == 1) {
		if constexpr (type == ScanlineType::PreRender) {
			inVblank = false;
			spriteZeroHit = false;
			spriteOverflow = false;
		}
		else {
			fetchNametableAddress();
		}
	}
	else if (scanlineCycle > 1 && scanlineCycle < 256) {
		/*
		fetch the data for each tile
		mem access takes 2 PPU cycles
		1 - nametable
		2 - attribute table
		3 - pattern table low
		4 - pattern table high

		data is latched, then fed into shift registers every 8 cycles
		shifters are reloaded during cycles 9, 17, 25, ..., 257

		no actual pixel will be output until frame 4, we might just ignore this though.

		data for first 2 tiles should already be ready at the start of the scanline, so first data to be loaded will be for tile 3
		*/

		if constexpr (type == ScanlineType::Visible) {
			renderPixel();
		}
		registerShift();
		
		renderLoop();
	}
	else if (scanlineCycle == 256) {
		// increment vertical postition in v, effective y scroll coord should be incremented
		// should this happen before or after the other stuff in the loop?
		if constexpr (type == ScanlineType::Visible) {
			renderPixel();
		}
		registerShift();
		tileHigh = vRamRead(tileAddress);
		verticalScroll();
	}
	else if (scanlineCycle == 257) {
		// all horizontal related bits should be copied from t to v:
		// v: ....F.. ...EDCBA = t: ....F.. ...EDCBA
		if constexpr (type == ScanlineType::Visible) {
			renderPixel();
		}
		registerShift();
		reloadShift();
		horizontalUpdate();
	}
	else if (scanlineCycle >= 280 && scanlineCycle <= 304) {
		if constexpr (type == ScanlineType::PreRender) {
			//If rendering is enabled, at the end of vblank, shortly after the horizontal bits are copied from t to v at dot 257, the PPU will repeatedly copy the vertical bits from t to v from dots 280 to 304, completing the full initialization of v from t :
			// v: IHGF.ED CBA..... = t: IHGF.ED CBA.....
			verticalUpdate();
		}
	}
	else if (scanlineCycle >= 322 && scanlineCycle <= 337) {
		registerShift();
		renderLoop();
	}
	else if (scanlineCycle == 321 || scanlineCycle == 339) {
		fetchNametableAddress();
	}
	else if (scanlineCycle == 338 || scanlineCycle == 340) {
		nametable = vRamRead(nametableAddress);
	}

	// --- Sprites ---
	if (scanlineCycle == 1) {
		// clear secondary OAM
		secondaryOAM = {};
	}
	else if (scanlineCycle == 257) {
		// fill secondary OAM with sprites for next scanline
		size_t spriteIndex = 0;
		for (size_t i = 0; i < 64; ++i) {
			int line = (scanline == 261 ? -1 : scanline) - oamMem[i * 4];

			if (line >= 0 && line < (spriteSize ? 16 : 8)) {
				auto& sprite = secondaryOAM[spriteIndex++];
				sprite.index = i;
				sprite.yPos = oamMem[i * 4];
				sprite.tile = oamMem[i * 4 + 1];
				auto attribute = oamMem[i * 4 + 2];
				sprite.palette = attribute & 3;
				sprite.priority = dx_util::testBit(attribute, 5);
				sprite.hFlip = dx_util::testBit(attribute, 6);
				sprite.vFlip = dx_util::testBit(attribute, 7);
				sprite.xPos = oamMem[i * 4 + 3];

				if (spriteIndex > 7) {
					spriteOverflow = true;
					break;
				}
			}
		}
	}
	else if (scanlineCycle == 321) {
		// load sprite info from secondary to main OAM and get the tile data
		oam = secondaryOAM;

		for (auto& sprite : oam) {
			uint16 address = 0;
			if (spriteSize)
				address = ((sprite.tile & 1) * 0x1000) + ((sprite.tile & ~1) * 16);
			else
				address = (spriteTableAddr * 0x1000) + (sprite.tile * 16);

			unsigned y = (scanline - sprite.yPos) % (spriteSize ? 16 : 8);
			if (sprite.vFlip)
				y ^= (spriteSize ? 16 : 8) - 1;

			address += y + (y & 8);
			assert(address < 0x1ff8);
			sprite.tileDataLow = vRamRead(address);
			sprite.tileDataHigh = vRamRead(address + 8);
		}
	}
}

void Ppu::postRenderScanline() {
	// do nothing, AFAIK
	// we'll use this time to copy the backbuffer to the frontbuffer
	if (scanlineCycle == 1) {
		std::copy(std::begin(backbuffer), std::begin(backbuffer) + (240 * 256), std::begin(frontbuffer));
		std::fill(std::begin(backbuffer), std::end(backbuffer), vmdx::rgba8{ 0, 0, 0, 255 });
	}
}

void Ppu::vBlankScanline() {
	if (scanline == 241 && scanlineCycle == 1) {
		inVblank = true;
		postNMI = postNMI ? postNMI : enableNMI;
	}
}

void Ppu::renderLoop() {
	switch (scanlineCycle % 8) {
	case 1:
		// nametable
		fetchNametableAddress();
		reloadShift();
		break;
	case 2:
		nametable = vRamRead(nametableAddress);
		break;
	case 3:
		// attribute
		fetchAttributeAddress();
		break;
	case 4:
		attribute = vRamRead(attributeAddress);
		if (registers.v.coarseY & 2)
			attribute >>= 4;
		if (registers.v.coarseX & 2)
			attribute >>= 2;
		break;
	case 5:
		// tile low
		fetchTileAddress();
		break;
	case 6:
		tileLow = vRamRead(tileAddress);
		break;
	case 7:
		// tile high
		tileAddress += 8;
		break;
	case 0:
		tileHigh = vRamRead(tileAddress);
		horizontalScroll();
		break;
	}
}

void Ppu::renderPixel() {
	uint8 paletteAddr = 0;
	uint8 spritePaletteAddr = 0;
	bool priority = false;
	size_t x = scanlineCycle - 2;

	if (enableBG && !(!enableLeftmostBG && x < 8)) {
		paletteAddr = (dx_util::getBit(latches.tileShiftHigh, 15 - registers.x) << 1) | dx_util::getBit(latches.tileShiftLow, 15 - registers.x);
		
		if (paletteAddr)
			paletteAddr |= ((dx_util::getBit(attributeShiftHigh, 7 - registers.x) << 1) | dx_util::getBit(attributeShiftLow, 7 - registers.x)) << 2;
	}


	if (enableSprites && !(!enableLeftmostSprites && x < 8)) {
		for (int i = 7; i >= 0; --i) {
			const auto& sprite = oam[i];
			if (sprite.index == 64)
				continue;

			auto spriteX = x - sprite.xPos;
			if (spriteX >= 8)
				continue;
			if (sprite.hFlip)
				spriteX ^= 7;

			uint8 spritePalette = (dx_util::getBit(sprite.tileDataHigh, 7 - spriteX) << 1) | dx_util::getBit(sprite.tileDataLow, 7 - spriteX);
			if (spritePalette == 0)
				continue;

			if (sprite.index == 0 && paletteAddr && x != 255)
				spriteZeroHit = true;

			spritePalette |= sprite.palette << 2;
			spritePaletteAddr = spritePalette + 16;
			priority = sprite.priority;
		}
	}

	if (spritePaletteAddr && (paletteAddr == 0 || !priority))
		paletteAddr = spritePaletteAddr;

	if (!enabled())
		paletteAddr = 0;

	// not too sure about the emphasis here, we'll revisit this later
	//backbuffer[scanline * 256 + x] = palette[(emphasis.b << 8) | (emphasis.g << 7) | (emphasis.r << 6) | readPaletteRam(paletteAddr & 0x1f)];
	backbuffer[scanline * 256 + x] = palette[readPaletteRam(paletteAddr)];
}

bool Ppu::checkNMI() {
	if (postNMI) {
		postNMI = false;
		return true;
	}
	return false;
}

static int shiftReloadCount = 0;
void Ppu::reloadShift() {
	assert(shiftReloadCount == 8);
	latches.tileShiftLow = (latches.tileShiftLow & 0xff00) | tileLow;
	latches.tileShiftHigh = (latches.tileShiftHigh & 0xff00) | tileHigh;
	latches.attributeLow = (attribute & 1);
	latches.attributeHigh = (attribute & 2);
	shiftReloadCount = 0;

}

void Ppu::registerShift() {
	latches.tileShiftLow <<= 1;
	latches.tileShiftHigh <<= 1;
	attributeShiftLow = latches.attributeLow | (attributeShiftLow << 1);
	attributeShiftHigh = latches.attributeHigh | (attributeShiftHigh << 1);
	++shiftReloadCount;
}

void Ppu::horizontalUpdate() {
	if (enabled())
		registers.v.address = (registers.v.address & ~0x041f) | (registers.t.address & 0x041f);
}
void Ppu::verticalUpdate() {
	if (enabled())
		registers.v.address = (registers.v.address & ~0x7BE0) | (registers.t.address & 0x7BE0);
}

void Ppu::horizontalScroll() {
	if (!enabled())
		return;
	if ((registers.v.address & 0x001F) == 31) {
		//When the value is 31, wrap around to 0 and switch nametable
		registers.v.address = (registers.v.address & ~0x001F) ^ 0x0400;
	}
	else {
		++registers.v.address;
	}
}

void Ppu::verticalScroll() {
	if (!enabled())
		return;
	if ((registers.v.address & 0x7000) != 0x7000) {
		// if fine Y < 7
		registers.v.address += 0x1000; // increment fine Y
	}
	else {
		// fine Y = 0
		registers.v.address &= ~0x7000;
		int y = (registers.v.address & 0x03e0) >> 5; // let y = coarse Y
		if (y == 29) {
			y = 0; // coarse Y = 0
			registers.v.address ^= 0x0800; // switch vertical nametable
		}
		else if (y == 31) {
			y = 0; // coarse Y = 0, nametable not switched
		}
		else {
			y++; // increment coarse Y
		}
		registers.v.address = (registers.v.address & ~0x03e0) | (y << 5); // put coarse Y back into v
	}
}

void Ppu::updateNametableImage() {
	for (size_t row = 0, px = 0; row < 30; ++row) {
		size_t baseOffset = row * 32;
		for (size_t pxY = 0; pxY < 8; ++pxY) {
			for (size_t col = 0; col < 32; ++col) {
				auto index = vRamRead(0x2000 + baseOffset + col);
				auto tile = patternTable[1][index];
				auto pal = palette[vRamRead(0x23c0 + baseOffset + col)];
				for (auto pxX = 0; pxX < 8; ++pxX) {
					nametableImage[0][px++] = pal[tile[pxY][pxX]];
				}
			}
		}
	}
}

void Ppu::updatePatternTable() {
	for (size_t i = 0; i < 256; ++i) {
		auto& tile = patternTable[0][i];
		auto& tile2 = patternTable[1][i];
		auto baseIndex = i * 16;
		for (size_t pxY = 0; pxY < 8; ++pxY) {
			auto lowIndex = baseIndex + pxY;
			auto highIndex = lowIndex + 8;
			auto lowByte = cart->mapper->chr[lowIndex];
			auto highByte = cart->mapper->chr[highIndex];
			auto lowByte2 = cart->mapper->chr[lowIndex + 0x1000];
			auto highByte2 = cart->mapper->chr[highIndex + 0x1000];

			for (int pxX = 0; pxX < 8; ++pxX) {
				int x = pxX - 7;
				x *= -1;
				tile[pxY][pxX] = (dx_util::getBit(highByte, x) << 1) | dx_util::getBit(lowByte, x);
				tile2[pxY][pxX] = (dx_util::getBit(highByte2, x) << 1) | dx_util::getBit(lowByte2, x);
			}
		}
	}
}

#if 0
void Ppu::updatePatternTableImage() {
	for (size_t tileY = 0, px = 0; tileY < 16; ++tileY) {
		for (size_t pxY = 0; pxY < 8; ++pxY) {
			for (size_t tileX = 0; tileX < 16; ++tileX) {
				auto index = tileY * 16 + tileX;
				const auto tile1 = patternTable[0][index];
				const auto tile2 = patternTable[1][index];
				for (int pxX = 0; pxX < 8; ++pxX) {
					patternTableImage[0][px] = debugPalette[tile1[pxY][pxX]];
					patternTableImage[1][px] = debugPalette[tile2[pxY][pxX]];
					++px;
				}
			}
		}
	}
	// OR
	for (size_t tileY = 0, px = 0; tileY < 16; ++tileY) {
		auto baseOffset = tileY * 256;

		for (size_t pxY = 0; pxY < 8; ++pxY) {
			for (size_t tileX = 0; tileX < 16; ++tileX) {
				auto tileOffset = baseOffset + (16 * tileX);
				auto lowIndex = tileOffset + pxY;
				auto highIndex = lowIndex + 8;
				auto lowByte = cart->mapper->chr[lowIndex];
				auto highByte = cart->mapper->chr[highIndex];
				auto lowByte2 = cart->mapper->chr[lowIndex + 0x1000];
				auto highByte2 = cart->mapper->chr[highIndex + 0x1000];

				for (int pxX = 7; pxX >= 0; --pxX) {
					patternTableImage[0][px] = ::palette[(getBit(highByte, pxX) << 1) | getBit(lowByte, pxX)];
					patternTableImage[1][px] = ::palette[(getBit(highByte2, pxX) << 1) | getBit(lowByte2, pxX)];
					++px;
				}
			}
		}
	}
}
#endif


} // namespace Nes
