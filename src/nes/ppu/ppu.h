#pragma once

#include "types.h"
#include "image.h"
#include "vec.h"

#include <array>

namespace Nes {

struct Cartridge;
enum struct Mirroring : unsigned;

struct Sprite {
	unsigned index = 64;
	uint8 yPos = 0xff; // byte 0
	uint8 tile = 0xff; // byte 1
	// byte 2 attributes
	unsigned palette = 3; // bits 0 -1
	bool priority = true; // bit 5
	bool hFlip = true; // bit 6
	bool vFlip = true; // bit 7
	uint8 xPos = 0xff; // byte 3

	uint8 tileDataLow = 0;
	uint8 tileDataHigh = 0;
};

struct Ppu {
	uint8 lastWrite;
	bool postNMI = false;

	// 0x2000 PPUCTRL
	bool enableNMI = 0;
	bool masterSlaveSelect = 0;
	bool spriteSize = 0; // 16 if set, otherwise 8
	bool bgTableAddr = 0;
	bool spriteTableAddr = 0;
	bool vramIncrement = true;
	unsigned nametableBase = 0;

	// 0x2001 PPUMASK
	struct {
		bool b, g, r;
	} emphasis = {};
	bool enableSprites = false;
	bool enableBG = false;
	bool enableLeftmostSprites = false;
	bool enableLeftmostBG = false;
	bool greyscale = false;

	// 0x2002 PPUSTATUS
	bool inVblank = false;
	bool spriteZeroHit = false;
	bool spriteOverflow = false;

	// 0x2003 OAMADDR
	uint8 oamAddr = 0;

	uint8 busData = 0;

	
	// loopy v and t
	struct {
		union {
			struct {
				unsigned coarseX : 5;
				unsigned coarseY : 5;
				unsigned nametableSelect : 2;
				unsigned fineY : 3;
			};
			struct {
				unsigned lo : 8;
				unsigned hi : 7;
			};
			unsigned address : 15;
		} v, t = {};
		uint8 x = 0; // 3 bits
		bool w = 0; // px2005 and 0x2006 share this, reading 0x2002 will clear it
	} registers = {};
	
	struct {
		uint16 tileShiftLow;
		uint16 tileShiftHigh;
		uint8 attributeLow;
		uint8 attributeHigh;
		uint16 nametable;
	} latches = {};

	uint8 attributeShiftLow = 0;
	uint8 attributeShiftHigh = 0;

	uint16 nametableAddress = 0;
	uint8 nametable = 0;
	uint16 attributeAddress = 0;
	uint8 attribute = 0;
	uint16 tileAddress = 0;
	uint8 tileLow = 0, tileHigh = 0;

	int scanlineCycle = 0;
	int scanline = 0;
	int currentPixel = 0;
	bool oddFrame = false;

	using Tile = std::array<std::array<uint8, 8>, 8>;

	std::array<Tile, 256> patternTable[2];
	std::array<uint8, 2048> nametables = {};
	std::array<uint8, 32> paletteRam = {};
	std::array<uint8, 256> oamMem = {};
	std::array<Sprite, 8> oam = {};
	std::array<Sprite, 8> secondaryOAM = {};
	Image::PixelData<vmdx::rgba8, 256, 262> backbuffer = {};
	Image::PixelData<vmdx::rgba8, 256, 240> frontbuffer = {};
	Image::PixelData<vmdx::rgba8, 256, 240> nametableImage[4];

	Cartridge* cart;
	Mirroring mirroring;

	Ppu(Cartridge* cart);
	~Ppu() {
	}

	uint8 read(uint16 address);
	void write(uint16 address, uint8 v);

	uint16 getNametableAddress(uint16 address);

	uint8 vRamRead(uint16 address);
	void vRamWrite(uint16 address, uint8 v);
	uint8 readPaletteRam(uint16 address);
	void writePaletteRam(uint16 address, uint8 v);

	void tick(int cycles = 3);

	enum struct ScanlineType {
		Visible, PostRender, VBlank, PreRender
	};

	template <ScanlineType type> void doScanline();
	void postRenderScanline();
	void vBlankScanline();

	void renderLoop(); // cycles 0 - 256 of visible and pre-render scanlines

	void renderPixel();
	bool checkNMI();
	bool enabled();
	void reloadShift();
	void registerShift();
	void verticalUpdate();
	void horizontalUpdate();
	void horizontalScroll();
	void verticalScroll();

	void fetchNametableAddress();
	void fetchAttributeAddress();
	void fetchTileAddress();

	void updateNametableImage();
	void updatePatternTable();
};

} // namespace Nes
