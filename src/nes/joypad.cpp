#include "joypad.h"

namespace Nes {

	void Joypad::write(uint8 v) {
		if (!v && strobe) {
			for (size_t i = 0; i < 2; ++ i)
				buttons[i] = latchButtons[i];
		}

		strobe = v;
	}

}
