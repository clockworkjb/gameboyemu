#pragma once
#include "types.h"

#include <array>

namespace Nes {
	struct Joypad {
		enum Button {
			A = 0x01,
			B = 0x02,
			Select = 0x04,
			Start = 0x08,
			Up = 0x10,
			Down = 0x20,
			Left = 0x40,
			Right = 0x80,
		};

		std::array<uint8, 2> buttons = {};
		std::array<uint8, 2> latchButtons = {};
		bool strobe = 0;

		void write(uint8 v);

		template <bool player> uint8 read() {
			uint8 ret = 0x40;

			if (strobe) {
				ret |= (latchButtons[player] & Button::A);
			}
			else {
				ret |= (buttons[player] & 1);
				buttons[player] = (buttons[player] >> 1) | 0x80;
			}

			return ret;
		}

		template <bool player> void toggleButton(Button button, bool setting) {
			latchButtons[player] &= ~button;
			if (setting)
				latchButtons[player] |= button;
		}
	};

}
