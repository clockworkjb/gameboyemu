#pragma once

#include "types.h"

namespace Nes {

struct Register {
	virtual operator unsigned() const = 0;
	virtual unsigned operator =(unsigned b) = 0;

	Register& operator =(Register& b) {
		*this = (unsigned)b;
		return *this;
	}

	unsigned operator +=(unsigned b) {
		*this = *this + b;
		return *this;
	}

	unsigned operator -=(unsigned b) {
		*this = *this - b;
		return *this;
	}

	unsigned operator *=(unsigned b) {
		*this = *this * b;
		return *this;
	}

	unsigned operator /=(unsigned b) {
		*this = *this / b;
		return *this;
	}

	unsigned operator <<=(unsigned b) {
		*this = *this << b;
		return *this;
	}

	unsigned operator >>=(unsigned b) {
		*this = *this >> b;
		return *this;
	}

	unsigned operator |=(unsigned b) {
		*this = *this | b;
		return *this;
	}

	unsigned operator &=(unsigned b) {
		*this = *this & b;
		return *this;
	}

	unsigned operator ^=(unsigned b) {
		*this = *this ^ b;
		return *this;
	}

	unsigned operator ++() {
		*this = *this + 1;
		return *this;
	}

	unsigned operator --() {
		*this = *this - 1;
		return *this;
	}

	unsigned operator ++(int) {
		unsigned temp = *this;
		operator++();
		return temp;
	}

	unsigned operator --(int) {
		unsigned temp = *this;
		operator--();
		return temp;
	}
};

struct RegisterFlags : Register {
	bool n, v, b, d, i, z, c;

	//RegisterFlags(bool n = 0, bool v = 0, bool b = 0, bool d = 0, bool i = 0, bool z = 0, bool c = 0) : n(n), v(v), b(b), d(d), i(i), z(z), c(c) {}
	RegisterFlags(uint16 p) {
		*this = p;
	}

	operator unsigned() const override { return (n << 7) | (v << 6) | (1 << 5) | (b << 4) | (d << 3) | (i << 2) | (z << 1) | c | 0x30; }

	unsigned operator =(unsigned byte) override {
		n = byte & 0x80;
		v = byte & 0x40;
		b = byte & 0x10;
		d = byte & 0x08;
		i = byte & 0x04;
		z = byte & 0x02;
		c = byte & 0x01;

		return *this; 
	}
};

struct Registers {
	uint8 a = 0, x = 0, y = 0, sp = 0xfd;
	RegisterFlags p = 0x4;
	uint16 pc; // this should be set to the rst vector

	Registers() {}
};

} // namespace Nes
