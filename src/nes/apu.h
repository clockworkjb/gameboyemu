#pragma once

#include "types.h"

namespace Nes {

class Apu {
public:
	Apu() {
	}

	~Apu() {
	}

	void tick(int cycles = 1);
};

} // namespace Nes
