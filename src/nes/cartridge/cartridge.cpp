#include "cartridge.h"

#include "mmc3.h"
#include "hex.h"

namespace Nes {

static constexpr uint8 INES_DESC[] = { 0x4e, 0x45, 0x53, 0x1a };

Cartridge::Cartridge(const std::vector<uint8>& buffer) {
	loadRom(buffer);
}

Cartridge::~Cartridge() {
	unloadRom();
}

void Cartridge::loadRom(const std::vector<uint8>& buffer) {
	unloadRom();

	// TODO verify that we actually have enough bytes for the header

	unsigned mapperNum = (buffer[6] & 0xf0) | (buffer[7] >> 4);

	switch (mapperNum) {
		case 4:
			mapper = std::make_unique<Mmc3>(buffer);
			break;
		default:
			mapper = std::make_unique<Mapper>(buffer);
	}
}

void Cartridge::unloadRom() {
}

uint8 Cartridge::read(uint16 address) {
	return mapper->read(address);
}

void Cartridge::write(uint16 address, uint8 v) {
	return mapper->write(address, v);
}

uint8 Cartridge::chrRead(uint16 address) {
	return mapper->chrRead(address);
}

void Cartridge::chrWrite(uint16 address, uint8 v) {
	return mapper->chrWrite(address, v);
}

void Cartridge::saveSRAM(const std::string& filename) {
}

void Cartridge::loadSRAM(const std::string& filename) {
}

} // namespace Nes
