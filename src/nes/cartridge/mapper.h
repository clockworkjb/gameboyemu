#pragma once

#include "types.h"

#include "util.h"

#include <vector>
#include <string>
#include <string_view>

namespace Nes {

enum struct Mirroring : unsigned {
	horizontal,
	vertical,
	singleScreen,
	fourScreen,
};

struct Mapper {
	static constexpr size_t PRG_PAGE_SIZE = 0x4000;
	static constexpr size_t PRG_RAM_PAGE_SIZE = 0x2000;
	static constexpr size_t CHR_PAGE_SIZE = 0x2000;
	static constexpr size_t HEADER_SIZE = 0x10;

	Mapper(const std::vector<uint8>& buffer, std::string_view name = "None");
	virtual ~Mapper() {
	}

	virtual uint8 read(uint16 address);
	virtual void write(uint16 address, uint8 v);

	virtual uint8 chrRead(uint16 address);
	virtual void chrWrite(uint16 address, uint8 v);

	std::vector<uint8> prgRom;
	std::vector<uint8> prgRam;
	std::vector<uint8> chr;

	unsigned prgRomPages, chrRomPages;
	bool chrRam;
	Mirroring mirroring;
	bool battery;

	std::string name;
};

std::ostream& operator << (std::ostream& o, Mapper& mapper);

} // namespace NES

