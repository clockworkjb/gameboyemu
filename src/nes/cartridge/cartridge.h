#pragma once

#include "types.h"
#include "util.h"
#include "mapper.h"

#include <string>
#include <vector>
#include <memory>

namespace Nes {

struct Cartridge {
	Cartridge(const std::vector<uint8>& buffer);
	~Cartridge();

	void loadRom(const std::vector<uint8>& buffer);
	void unloadRom();

	uint8 read(uint16 address);
	void write(uint16 address, uint8 v);

	uint8 chrRead(uint16 address);
	void chrWrite(uint16 address, uint8 v);

	void saveSRAM(const std::string& filename);
	void loadSRAM(const std::string& filename);

	std::unique_ptr<Mapper> mapper;
};

} // namespace Nes
