#pragma once
#include "mapper.h"

namespace Nes {

struct Mmc3 : public Mapper {
	Mmc3(const std::vector<uint8>& buffer) : Mapper(buffer, "MMC3") {
	}

	~Mmc3() {
	}

	uint8 read(uint16 address) override {
		if (address & 0x8000) {
		}
		return prgRom[address % 0x8000];
	}

	uint8 chrRead(uint16 address) override {
		return chr[address % 0x2000];
	}

	void write(uint16 address, uint8 v) override {
	}

	void chrWrite(uint16 address, uint8 v) override {
	}

	size_t bankSelect;
};

} // namespace Nes
