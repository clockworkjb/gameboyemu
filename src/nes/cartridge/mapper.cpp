#include "mapper.h"

#include <iostream>
#include <cassert>

namespace Nes {

Mapper::Mapper(const std::vector<uint8>& buffer, std::string_view name) : name(name) {
	prgRomPages = buffer[4];
	chrRomPages = buffer[5];

	mirroring = dx_util::testBit(buffer[6], 0) ? Mirroring::vertical : Mirroring::horizontal;
	battery = dx_util::testBit(buffer[6], 1);

	auto prgRamSize = buffer[8];
	if (prgRamSize)
		prgRam.resize(prgRamSize * PRG_RAM_PAGE_SIZE);

	assert(buffer.size() == (HEADER_SIZE + prgRomPages * PRG_PAGE_SIZE + chrRomPages * CHR_PAGE_SIZE));

	auto prgIt = std::begin(buffer) + HEADER_SIZE;
	auto prgEnd = prgIt + prgRomPages * PRG_PAGE_SIZE;
	prgRom.assign(prgIt, prgEnd);

	if (chrRomPages > 0) {
		chrRam = false;
		chr.assign(prgEnd, prgEnd + chrRomPages * CHR_PAGE_SIZE);
	}
	else {
		chrRam = true;
		chr.resize(CHR_PAGE_SIZE);
	}
}

uint8 Mapper::read(uint16 address) {
	if (prgRam.size() && address < 0x8000) {
		// PRG ram, mirrored to fill 8kb
		return prgRam[(address - 0x6000) % prgRam.size()];
	}
	// 1-2 pages of PRG rom
	return prgRom[(address - 0x8000) % prgRom.size()];
}

uint8 Mapper::chrRead(uint16 address) {
	return chr[address % 0x2000];
}

void Mapper::write(uint16 address, uint8 v) {
	if (prgRam.size() && address < 0x8000) {
		// PRG ram, mirrored to fill 8kb
		prgRam[(address - 0x6000) % prgRam.size()] = v;
	}
}

void Mapper::chrWrite(uint16 address, uint8 v) {
}

std::ostream& operator << (std::ostream& o, Mapper& mapper) {
	o << "Type: " << mapper.name << "\n";
	o << "PRG pages: " << +(mapper.prgRomPages) << " size: " << +(mapper.prgRomPages * Mapper::PRG_PAGE_SIZE) << "b\n";
	o << "PRG ram size: " << +(mapper.prgRam.size()) << "b\n";
	o << "CHR pages: " << +(mapper.chrRomPages) << " size: " << +(mapper.chrRomPages * Mapper::CHR_PAGE_SIZE) << "b\n";
	o << "CHR ram: " << (mapper.chrRam ? "yes" : "no") << "\n";

	return o;
}


} // namespace Nes
