#pragma once

#include "types.h"
#include "registers.h"
#include "cartridge/cartridge.h"
#include "ppu/ppu.h"
#include "apu.h"
#include "joypad.h"
#include "util.h"
#include "debug.h"
#include "hex.h"

#include <array>

namespace Nes {

enum struct Interrupt {
	BRK = 0,
	IRQ,
	NMI,
	RST,
};

struct Cpu {
	static constexpr int cyclesPerFrame = 29780; // ppu cycles/3, this probably isn't exact, but it seems to be good enough

	Cpu(const std::vector<uint8>& buffer) : cart(buffer), ppu(&cart) {
		doInterrupt(Interrupt::RST);
	}

	~Cpu() {}

	int execute();
	void reset();

	int run(int cyclesNeeded);

	void doInterrupt(Interrupt i);

	uint8 op;
	Registers registers;
	bool nmi = false, irq = false;

#include "memory.h"

	Ppu ppu;
	Apu apu;
	Joypad joypad;

	int instructionCount = 0;
	int cycleCount = 0;
	int lastCycleCount;

	uint16 immediate();
	uint8 readImmediate8();
	uint16 readImmediate16();

	uint8 readMem(uint16 addr);
	void writeMem(uint16 addr, uint8 val);	

	uint16 zeroPage();
	uint16 absolute();
	uint16 absoluteX();
	uint16 absoluteY();
	uint16 zeroPageX();
	uint16 zeroPageY();
	uint16 indirectX();
	uint16 indirectY();
	
	void pageCheck(uint16 a, uint16 b);

	void tick(int cycles = 1);

	template <auto mode> uint16 getAddr() {
		return (this->*mode)();
	}

	template <auto mode> uint8 fetch() {
		return readMem(getAddr<mode>());
	}

#include "instructions.h"
};

} // namespace Nes
