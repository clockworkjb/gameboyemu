static constexpr uint16 WRAM_OFFSET = 0x0000;
static constexpr uint16 WRAM_SIZE = 0x0800;
static constexpr uint16 WRAM_MIRROR = WRAM_OFFSET + WRAM_SIZE;
static constexpr uint16 PPU_REG_OFFSET = 0x2000;
static constexpr uint16 PPU_REG_SIZE = 0x0008;
static constexpr uint16 PPU_REG_MIRROR = PPU_REG_OFFSET + PPU_REG_SIZE;
static constexpr uint16 APU_REG_OFFSET = 0x4000;
static constexpr uint16 APU_REG_SIZE = 0x0020;
static constexpr uint16 CART_EXP_OFFSET = 0x4020;
static constexpr uint16 CART_EXP_SIZE = 0x1fdf;
static constexpr uint16 CART_RAM_OFFSET = 0x6000;
static constexpr uint16 CART_RAM_SIZE = 0x2000;
static constexpr uint16 CART_ROM_OFFSET = 0x8000;
static constexpr uint16 CART_ROM_SIZE = 0x8000;

enum {
	PPUCTRL = 0x2000,
	PPUMASK = 0x2001,
	PPUSTATUS = 0x2002,
	OAMADDR = 0x2003,
	OAMDATA = 0x2004,
	PPUSCROLL = 0x2005,
	PPUADDR = 0x2006,
	PPUDATA = 0x2007,

	SQ1_VOL = 0x4000,
	SQ1_SWEEP = 0x4001,
	SQ1_LO = 0x4002,
	SQ1_HI = 0x4003,
	SQ2_VOL = 0x4004,
	SQ2_SWEEP = 0x4005,
	SQ2_LO = 0x4006,
	SQ2_HI = 0x4007,
	TRI_LINEAR = 0x4008,
	TRI_LO = 0x400a,
	TRI_HI = 0x400b,
	NOISE_VOL = 0x400c,
	NOISE_LO = 0x400e,
	NOISE_HI = 0x400f,
	DMC_FREQ = 0x4010,
	DMC_RAW = 0x4011,
	DMC_START = 0x4012,
	DMC_LEN = 0x4013,
	OAMDMA = 0x4014,
	SND_CHN = 0x4015,
	JOY1 = 0x4016,
	JOY2 = 0x4017,

	IRQ1 = 0xfffe,
	IRQ2 = 0xffff,
};

uint8 read(uint16 address);
void write(uint16 address, uint8 v);

void oamDMA(uint8 v);

std::array<uint8, WRAM_SIZE> ram = {}; //0x0000 - 0x07ff
std::array<uint8, APU_REG_SIZE> ioRegisters = {}; // 0x4000 - 0x4017

Cartridge cart;
