#pragma once

#include "types.h"

#include <iostream>

namespace Nes {

class Cpu;

class Disassembler {
public:
	Disassembler() {}
	~Disassembler() {}

	const Disassembler& operator()(uint16 pc, Cpu* mem) {
		this->pc = pc;
		this->mem = mem;
		return *this;
	}

	friend std::ostream& operator << (std::ostream& o, const Disassembler& d);
private:
	Cpu* mem;
	uint16 pc;
};

extern Disassembler disassembler;

}
