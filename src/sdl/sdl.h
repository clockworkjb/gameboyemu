#pragma once

#define SDL_MAIN_HANDLED
#include <SDL.h>

#include "types.h"

#include <iostream>

namespace SDL {

inline void sdlError(const char* s) {
	const char* error = SDL_GetError();

	if (*error)
		std::cerr << s << error << "\n";
}

struct Window {
	SDL_Window* window;
	int width, height;
	bool visible = true, focus = false;

	Window(const char* title, int width, int height, uint32 flags = SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL) : width(width), height(height) {
		window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, flags);
		visible = true;
	}

	~Window() {
		SDL_DestroyWindow(window);
	}

	void resize(int w, int h) {
		width = w;
		height = h;
		SDL_SetWindowSize(window, width, height);
	}

	void handleResize(SDL_Event& event) {
		width = event.window.data1;
		height = event.window.data2;
		SDL_SetWindowSize(window, width, height);
	}

	void hide() {
		SDL_HideWindow(window);
		visible = false;
	}

	void show() {
		SDL_ShowWindow(window);
		visible = true;
	}

	void raise() {
		SDL_RaiseWindow(window);
	}

	void gainFocus() {
		focus = true;
	}

	void loseFocus() {
		focus = false;
	}
};

}

