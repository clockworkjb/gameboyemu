#include "debug.h"
#include "io.h"
#include "ui.h"
#include "nesdebugger.h"

#include "gameboy/system/system.h"
#include "nes/cpu.h"

#include <iostream>

int main (int argc, char** argv)
{
	if (argc < 2) {
		std::cerr << "No ROM filename provided, exiting\n";
		return EXIT_SUCCESS;
	}

	IO::fs::path path{ argv[1] };

	if (!IO::fs::exists(path)) {
		std::cerr << "File " << argv[1] << " could not be found, exiting\n";
		return EXIT_SUCCESS;
	}

	auto buffer = IO::loadFile(path);

	if (buffer.empty()) {
		std::cerr << "Error opening rom file\n";
		return EXIT_SUCCESS;
	}

	if (path.has_extension() && path.extension() == ".nes") {
		auto nes = new Nes::Cpu(buffer);
		DEBUG(*nes->cart.mapper);
		Debugger::init(nes);
		Debugger::mainLoop();
		delete nes;
	}
	else {
		auto gb = new Gameboy::System(buffer);
		buffer.resize(0);

		UI::init(gb);
		UI::mainLoop();

		delete gb;
	}

	return EXIT_SUCCESS;
}
