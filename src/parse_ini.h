#pragma once

#include "debug.h"
#include "io.h"
#include "util.h"

#include <string>
#include <string_view>
#include <charconv>
#include <map>
#include <sstream>
#include <optional>

namespace INI {

namespace detail {
	template<typename T> T convert(std::string_view value) {
		return T{value};
	}

	template <> bool convert(std::string_view value) {
		return dx_util::compareTo(value, "true", "1", "yes");
	}

	template <> float convert(std::string_view value) {
		// until charconv properly supports floats on gcc and clang, we'll just allocate a string here
		return std::stof(std::string{value});
	}

	template <> int convert(std::string_view value) {
		int result = 0;
		std::from_chars(value.data(), value.data() + value.size(), result);
		return result;
	}
}

struct Section {
	std::map<std::string, std::string, std::less<>> values;

	auto find(std::string_view name) {
		return values.find(name);
	}

	template <typename T> void setValueOr(std::string_view key, T& param, T def) {
		if (auto value = getValue(key); value)
			param = detail::convert<T>(value.value());
		else
			param = def;
	}

	template <typename T> void setValue(std::string_view key, T& param) {
		if (auto value = getValue(key); value)
			param = detail::convert<T>(value.value());
	}

	std::optional<std::string_view> getValue(std::string_view name) {
		auto result = values.find(name);
		if (result != values.end())
			return result->second;
		return {};
	}

	auto begin() {
		return values.begin();
	}

	auto end() {
		return values.end();
	}
};

class Parser {
public:
	IO::fs::path filename = {};

	Parser(std::string_view content) {
		parseContent(content);
	}

	Parser(std::string_view defaultContent, std::string_view filename) : filename(filename) {
		parseContent(defaultContent);
		if (!IO::fs::exists(filename)) {
			IO::saveFile(filename, defaultContent);
		}
		else {
			auto content = IO::loadFileSigned(filename);
			parseContent(content);
		}
	}

	std::optional<Section> getSection(std::string_view name) {
		auto result = sections.find(name);
		if (result != sections.end())
			return result->second;
		return {};
	}

	auto begin() const {
		return sections.begin();
	}

	auto end() const {
		return sections.end();
	}

	auto& getSections() const {
		return sections;
	}

private:
	std::map<std::string, Section, std::less<>> sections;

	void parseContent(std::string_view content) {
		std::string currentSection {};

		std::istringstream stream(content.data());
		for (std::string line; std::getline(stream, line);) {
			line = dx_util::trimboth(line);
			if (line[0] == '[') {
				auto end = line.find(']', 1);
				if (end != std::string::npos) {
					line = line.substr(1, end - 1);
					//DEBUG("section: " << line);
					currentSection = line;
				}
			}
			else {
				auto end = line.find('=', 1);
				if (end != std::string::npos) {
					auto key = dx_util::rtrim(line.substr(0, end));
					auto value = dx_util::ltrim(line.substr(end + 1, line.size()));
					sections[currentSection].values[key] = value;
					//DEBUG("section: " << currentSection << " key: " << key << " value: " << (*this)[currentSection][key]);
				}
			}
		}
	}
};

}
