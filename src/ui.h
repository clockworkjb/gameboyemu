#pragma once

namespace Gameboy {
	class System;
}

namespace UI {

void mainLoop();
bool init(Gameboy::System* gb);

} // namespace UI

