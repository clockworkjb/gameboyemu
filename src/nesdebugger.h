#pragma once

#include "image.h"

namespace Nes {
	struct Cpu;
}

namespace Debugger {
	bool init(Nes::Cpu* nes);
	void mainLoop();
}
