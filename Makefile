
BIN := gameboy_emu

CC :=
LD :=

SRC_EXT := cpp

SRC_DIR := src
OBJ_DIR := obj
IM_OBJ_DIR := im_obj

IM_DIR := imgui

CFLAGS := -std=c++17 -g -Ilib -I. -I$(IM_DIR) -I$(SRC_DIR) -Og
WARN_FLAGS := -Wall -Wextra
LDFLAGS = 

SHADER_CC := 

# OS specific flags for dependecies
ifeq ($(OS),Windows_NT)
	# Windows
	# should probably try and get this working using mingw or wsl I guess...
	CFLAGS += 
	LDFLAGS +=
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		# Linux
		LDFLAGS += -lGL -lGLEW -lm -lSDL2 -D_LINUX `sdl2-config --libs`
		CFLAGS += `sdl2-config --cflags`
		CC = g++
		LD = g++
	endif
	ifeq ($(UNAME_S),Darwin)
		# macOS
		LDFLAGS += -L/usr/local/lib/ -lglew -F/Library/Frameworks -framework SDL2 -framework OpenGL -lstdc++fs
		CFLAGS += -D__APPLE__ -I/usr/local/include -I/Library/Frameworks/SDL2.framework/Headers/
		# use latest gcc installed via Homebrew until the version of Clang that ships with macOS supports C++17 
		CC = g++-8
		LD = g++-8
	endif
endif

SOURCES := $(shell find $(SRC_DIR) -name '*.$(SRC_EXT)')
OBJECTS := $(SOURCES:$(SRC_DIR)/%.$(SRC_EXT)=%.o)
OBJECTS := $(OBJECTS:%.o=$(OBJ_DIR)/%.o)

IM_SOURCES := $(shell find $(IM_DIR) -name '*.$(SRC_EXT)')
IM_OBJECTS := $(IM_SOURCES:$(IM_DIR)/%.$(SRC_EXT)=%.o)
IM_OBJECTS := $(IM_OBJECTS:%.o=$(IM_OBJ_DIR)/%.o)

DEPS := $(OBJECTS:.o=.d)
DEPS += $(IM_OBJECTS:.o=.d)

.PHONY: all
all: $(BIN)

$(BIN): $(OBJECTS) $(IM_OBJECTS)
	@echo "linking: $(OBJECTS) $(IM_OBJECTS) > $(BIN)"
	@$(LD) -o $(BIN) $(OBJECTS) $(IM_OBJECTS) $(LDFLAGS)

$(OBJECTS): $(OBJ_DIR)/%.o: $(SRC_DIR)/%.$(SRC_EXT)
	@mkdir -p $(@D)
	@echo "compiling: $< > $@"
	@$(CC) $(CFLAGS) $(WARN_FLAGS) -c -MP -MMD $< -o $@

$(IM_OBJECTS): $(IM_OBJ_DIR)/%.o: $(IM_DIR)/%.$(SRC_EXT)
	@mkdir -p $(@D)
	@echo "compiling: $< > $@"
	@$(CC) $(CFLAGS) -c -MP -MMD $< -o $@

-include $(DEPS)

.PHONY: clean
clean:
	rm -f $(OBJECTS) $(IM_OBJECTS) $(DEPS)
